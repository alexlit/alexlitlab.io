# FAQ

[![pipeline status](https://gitlab.com/alexlit/alexlit.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/alexlit/alexlit.gitlab.io/commits/master)
[![tested with jest](https://img.shields.io/badge/tested_with-jest-99424f.svg)](https://github.com/facebook/jest)

## Структура проекта

Проект разработан с использованием BEM рекомендаций по архитектуре, все файлы
компонента (такие как шаблоны, скрипты, стили, изображения и т.д.) находятся в
папке с именем компонента.

```text
[src] // исходный код проекта
  [layouts] // шаблоны для страниц
  [pages] // страницы
  [components] // компоненты
```

## Технологии

* **HTML**

  * **[Nunjucks](https://mozilla.github.io/nunjucks/)** - html-шаблонизатор

* **CSS**

  * **[BEM](https://ru.bem.info/)** - методология Yandex для построения
    масштабируемых интерфейсов
  * **[PostCSS](http://postcss.org/)** - CSS препроцессор
    * **[CSSNext](http://cssnext.io/)** - набор PostCSS плагинов для
      транспайлинга CSS

* **JS**

  * **[Babel](http://babeljs.io/)** - транспайлер javascript
  * **[Vue.js](https://vuejs.org/)** - реактивный front-end фреймворк
    * **[Element UI](http://element.eleme.io/)** - библиотека UI элементов
    * **[Vuex.js](https://vuex.vuejs.org/)** - паттерн управления состоянием
      приложения

* **Tools**

  * **[Git](https://git-scm.com/)** - система контроля версий
  * **[Gitbook](https://toolchain.gitbook.com/)** - генератор документации
  * **[Gulp](http://gulpjs.com/)** - таск-раннер
  * **[Jest](https://facebook.github.io/jest/)** - юнит-тестирование
  * **[Webpack](https://webpack.js.org/)** - сборщик js модулей

## Развертывание

### Установка приложений

Установите [Git](https://git-scm.com/book/ru/v1/Введение-Установка-Git) для
своей платформы.

Установите [node.js + npm](https://nodejs.org/en/).

### Установка утилит для сборки и зависимостей проекта

После установки зависимостей проект будет готов для развертывания

```sh
npm i gulp webpack -g && npm i
```

## Разработка

### Development сборка

Отличается от production сборки отсутствием минификации
скриптов/стилей/изображений, использованием source map и консольных сообщений
для отладки кода

```sh
npm run dev
```

### Production сборка

Скрипты и стили минифицированы, изображения оптимизированы, интерфейсные
изображения (такие как иконки) кодированы в base64 и хранятся в файле стилей,
консольные сообщения вырезаны

```sh
npm run build
```

### Создать новый компонент

Создать в папке `components` папку с именем компонента и базовыми файлами:

```sh
npm run new имя_нового_компонента
```

### Генерация style guide

```sh
npm run styleguide
```

### Юнит-тестирование

Запустить тест отдельного компонента:

```sh
npm run test имя_компонента
```

Сгенерировать отчет о покрытии тестами:

```sh
npm run test:coverage
```

### Deploy на удаленный сервер

В файле `package.json` в секции `scripts` прописать конфигурацию для доступа на
сервер по FTP и запустить команду:

```sh
npm run deploy
```
