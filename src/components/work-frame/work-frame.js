import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|------------------------------------------------------------------------------
| Регистрация состояния компонента
|------------------------------------------------------------------------------
*/
store.registerModule('WorkFrame', {
  namespaced: true,

  state() {
    return {
      isActive: false
    };
  },

  getters: {},

  mutations: {
    /**
     * Установка состояния состояния фрейма
     * @param {{}} state
     * @param {Boolean} isVisible видимость фрейма
     */
    setFrameVisibility(state, isVisible) {
      state.isActive = isVisible;
    }
  }
});

/*
|------------------------------------------------------------------------------
| Объявление объекта опций компонента
|------------------------------------------------------------------------------
*/
const WorkFrame = {
  template: require('./work-frame.htm'),

  props: ['href', 'type', 'name'],

  computed: {
    ...mapState('WorkFrame', ['isActive']),
    ...mapState('PortfolioItem', ['currentItem']),

    ...mapGetters('WorkFrame', [])
  },

  methods: {
    ...mapMutations('WorkFrame', ['setFrameVisibility'])
  },

  filters: {
    /**
     * Формирование названия работы из алиаса
     * @param {String} value - значение
     */
    formatName(value) {
      return value.replace(/-/g, ' ');
    }
  }
};

Vue.component('WorkFrame', WorkFrame);

export default WorkFrame;
