import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import particles from 'exports-loader?particlesJS=window.particlesJS,window.pJSDom!particles.js';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|------------------------------------------------------------------------------
| Регистрация состояния компонента
|------------------------------------------------------------------------------
*/
store.registerModule('SiteFooter', {
  namespaced: true,

  state() {
    return {};
  },

  getters: {},

  mutations: {}
});

/*
|------------------------------------------------------------------------------
| Объявление объекта опций компонента
|------------------------------------------------------------------------------
*/
const SiteFooter = {
  template: require('./site-footer.htm'),

  data() {
    return {
      contactsItems: [
        {
          label: '+7 (978) 745-30-18',
          href: 'tel:+79787453018',
          type: 'phone'
        },
        {
          label: 'alex.lit@outlook.com',
          href: 'mailto:alex.lit@outlook.com',
          type: 'email'
        },
        {
          label: 'alekseyjazz',
          href: 'skype:alekseyjazz?chat',
          type: 'skype'
        },
        {
          label: 'alexlit_webdev',
          href: 'https://t.me/alexlit_webdev',
          type: 'telegram'
        }
      ],
      openSourceItems: [
        {
          label: 'Плагин Zooming Images для OctoberCMS',
          href: 'https://github.com/alex-lit/OctoberCMS-Zooming-Images-Plugin'
        },
        {
          label: 'Плагин Browser Detector для OctoberCMS',
          href: 'https://github.com/alex-lit/OctoberCMS-Browser-Detector-Plugin'
        },
        {
          label: 'Плагин Yandex Share для OctoberCMS',
          href: 'https://github.com/alex-lit/OctoberCMS-Yandex-Share-Plugin'
        },
        {
          label: 'Плагин Earth NullSchool Maps для OctoberCMS',
          href:
            'https://github.com/alex-lit/OctoberCMS-Earth-NullSchool-Maps-Plugin'
        },
        {
          label: 'Плагин VK Widgets для OctoberCMS',
          href: 'https://github.com/alex-lit/OctoberCMS-VK-Widgets-Plugin'
        },
        {
          label: 'Плагин HyperComments для OctoberCMS',
          href: 'https://github.com/alex-lit/OctoberCMS-HyperComments-Plugin'
        },
        {
          label: 'Тема Juicy для OctoberCMS',
          href: 'https://github.com/alex-lit/OctoberCMS-Juicy-Theme'
        }
      ],
      plugins: {
        particles: {
          enable: true,
          options: {
            particles: {
              number: {
                value: 160,
                density: {
                  enable: true,
                  value_area: 800
                }
              },
              color: {
                value: '#ffffff'
              },
              shape: {
                type: 'circle',
                stroke: {
                  width: 0,
                  color: '#000000'
                },
                polygon: {
                  nb_sides: 5
                },
                image: {
                  src: 'img/github.svg',
                  width: 0,
                  height: 0
                }
              },
              opacity: {
                value: 0,
                random: false,
                anim: {
                  enable: false,
                  speed: 1,
                  opacity_min: 0.1,
                  sync: false
                }
              },
              size: {
                value: 0,
                random: false,
                anim: {
                  enable: false,
                  speed: 40,
                  size_min: 0.1,
                  sync: false
                }
              },
              line_linked: {
                enable: true,
                distance: 60,
                color: '#ffffff',
                opacity: 0.3,
                width: 1
              },
              move: {
                enable: true,
                speed: 3,
                direction: 'none',
                random: false,
                straight: false,
                out_mode: 'out',
                bounce: false,
                attract: {
                  enable: false,
                  rotateX: 600,
                  rotateY: 1200
                }
              }
            },
            interactivity: {
              detect_on: 'canvas',
              events: {
                onhover: {
                  enable: false,
                  mode: 'repulse'
                },
                onclick: {
                  enable: false,
                  mode: 'push'
                },
                resize: true
              },
              modes: {
                grab: {
                  distance: 400,
                  line_linked: {
                    opacity: 1
                  }
                },
                bubble: {
                  distance: 400,
                  size: 40,
                  duration: 2,
                  opacity: 8,
                  speed: 3
                },
                repulse: {
                  distance: 200,
                  duration: 0.4
                },
                push: {
                  particles_nb: 4
                },
                remove: {
                  particles_nb: 2
                }
              }
            },
            retina_detect: true
          }
        }
      }
    };
  },

  computed: {
    ...mapState('SiteFooter', []),
    ...mapState('PortfolioItem', ['currentItem']),

    ...mapGetters('SiteFooter', [])
  },

  methods: {
    ...mapMutations('SiteFooter', [])
  },

  mounted() {
    if (this.plugins.particles.enable)
      particles.particlesJS('site-footer', this.plugins.particles.options);
  }
};

Vue.component('SiteFooter', SiteFooter);

export default SiteFooter;
