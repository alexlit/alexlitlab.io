import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|------------------------------------------------------------------------------
| Регистрация состояния компонента
|------------------------------------------------------------------------------
*/
store.registerModule('SectionHeading', {
  namespaced: true,

  state() {
    return {};
  },

  getters: {},

  mutations: {}
});

/*
|------------------------------------------------------------------------------
| Объявление объекта опций компонента
|------------------------------------------------------------------------------
*/
const SectionHeading = {
  template: require('./section-heading.htm'),

  props: {
    // модификатор
    mod: {
      type: String,
      default: '',
      validator(value) {
        return ['', 'light'].indexOf(value) !== -1;
      }
    },
    // уроверь заголовка <H?>
    level: {
      type: Number,
      default: 2,
      validator(value) {
        return [1, 2, 3, 4, 5, 6].indexOf(value) !== -1;
      }
    },
    // заголовок
    title: {
      type: String,
      default: ''
    },
    // подзаголовок
    subtitle: {
      type: String,
      default: ''
    }
  },

  computed: {
    ...mapState('SectionHeading', []),

    ...mapGetters('SectionHeading', [])
  },

  methods: {
    ...mapMutations('SectionHeading', [])
  }
};

Vue.component('SectionHeading', SectionHeading);

export default SectionHeading;
