import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import LazyLoad from 'vanilla-lazyload';
import _ from 'lodash';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|------------------------------------------------------------------------------
| Регистрация состояния компонента
|------------------------------------------------------------------------------
*/
store.registerModule('SimilarProjects', {
  namespaced: true,

  state() {
    return {};
  },

  getters: {},

  mutations: {}
});

/*
|------------------------------------------------------------------------------
| Объявление объекта опций компонента
|------------------------------------------------------------------------------
*/
const SimilarProjects = {
  template: require('./similar-projects.htm'),

  computed: {
    ...mapState('SimilarProjects', []),
    ...mapState('PortfolioItem', ['currentItem', 'db']),

    ...mapGetters('SimilarProjects', []),
    ...mapGetters('PortfolioItem', ['worksCount']),

    // Выборка из портфолио работ этого же типа
    items() {
      return _.shuffle(
        this.db.filter(
          item =>
            item.type === this.currentItem.type &&
            item.slug !== this.currentItem.slug
        )
      ).slice(0, 6);
    }
  },

  filters: {
    /**
     * Формирование названия работы из алиаса
     * @param {String} alias - алиас
     */
    formatName(alias) {
      return alias.replace(/-/g, ' ');
    }
  },

  methods: {
    ...mapMutations('SimilarProjects', []),

    /**
     * Ленивая загрузка изображений
     * @param {Srting} selector селектор элемента
     */
    lazyLoadImages(selector) {
      const similarProjectsLazyLoad = new LazyLoad({
        elements_selector: selector,
        threshold: 2000
      });
      similarProjectsLazyLoad.update();
    }
  },

  updated() {
    this.lazyLoadImages('.similar-projects [data-src]');
  }
};

Vue.component('SimilarProjects', SimilarProjects);

export default SimilarProjects;
