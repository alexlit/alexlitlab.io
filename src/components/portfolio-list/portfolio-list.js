import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import LazyLoad from 'vanilla-lazyload';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|------------------------------------------------------------------------------
| Регистрация состояния компонента
|------------------------------------------------------------------------------
*/
store.registerModule('PortfolioList', {
  namespaced: true,

  state() {
    return {};
  },

  getters: {},

  mutations: {}
});

/*
|------------------------------------------------------------------------------
| Объявление объекта опций компонента
|------------------------------------------------------------------------------
*/
const PortfolioList = {
  template: require('./portfolio-list.htm'),

  computed: {
    ...mapState('PortfolioList', []),
    ...mapState('PortfolioItem', ['db']),

    ...mapGetters('PortfolioList', [])
  },

  filters: {
    /**
     * Формирование названия работы из алиаса
     * @param {String} value значение
     */
    formatName(value) {
      return value.replace(/-/g, ' ');
    }
  },

  methods: {
    ...mapMutations('PortfolioList', []),

    /**
     * Ленивая загрузка изображений
     * @param {Srting} selector селектор элемента
     */
    lazyLoadImages(selector) {
      const portfolioListLazyLoad = new LazyLoad({
        elements_selector: selector,
        threshold: 2000
      });
      portfolioListLazyLoad.update();
    }
  },

  mounted() {
    this.lazyLoadImages('.portfolio-list [data-src]');
  }
};

Vue.component('PortfolioList', PortfolioList);

export default PortfolioList;
