### Адаптивность

Сайт работает на устройствах с любым размером дисплея.

![](/assets/images/portfolio-item/vintehnika/content/1.gif)

### Общение с клиентом

Реализована возможность связаться с продовцом и уточнить информацию в 1 клик или
заполнить полную форму.

![](/assets/images/portfolio-item/vintehnika/content/2.gif)

### Скорость

Благодаря оптимизации кода и изображений переход между страницами осуществляется
мгновенно.

![](/assets/images/portfolio-item/vintehnika/content/3.gif)
