## Установка

```shell
$ npm i css-material-color-palette-variables -S
```

## Подключение

Через HTML:

```html
<style src="/node_modules/css-material-color-palette-variables/index.css"></style>
```

... или CSS:

```css
@import url('/node_modules/css-material-color-palette-variables/index.css');
```

... или Javascript (Webpack):

```js
import('css-material-color-palette-variables');
```

## Использование

Используется в стилях как нативные CSS переменные:

```css
.red-text {
  color: var(--red--500); /* то же что и color: #f44336 */
}
```

Библиотека весьма простенькая, но полезная, я использую ее в проектах где отсутствует фирменный стайлгайд.

<iframe src="https://codesandbox.io/embed/nqpjm180p?autoresize=1&view=editor" style="width:100%; height:500px; border:0; border-radius: 4px; overflow:hidden;" sandbox="allow-modals allow-forms allow-popups allow-scripts allow-same-origin"></iframe>
