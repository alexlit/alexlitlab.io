### Интерфейсная анимация

Все действия пользователя анимированы, все происходит плавно и приложением
приятно пользоваться.

![](/assets/images/portfolio-item/jetruby-test-task/content/1.gif)

### Адаптивный интерфейс

Внешний вид приложения разработан с учетом использования на различных
устройствах.

![](/assets/images/portfolio-item/jetruby-test-task/content/2.gif)
