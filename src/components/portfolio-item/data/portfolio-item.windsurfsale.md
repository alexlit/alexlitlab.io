### Удобная навигация

Шапка сайта и рекламные материалы всегда остаются на виду.

![](/assets/images/portfolio-item/windsurfsale/content/1.gif)

### Общение с клиентом

Реализована возможность связаться с продовцом и уточнить информацию по товару в
1 клик.

![](/assets/images/portfolio-item/windsurfsale/content/2.gif)

### Простое администрирование

Дружелюбная панель администратора позволяет обновлять ассортимент за счинанные
секунды.

![](/assets/images/portfolio-item/windsurfsale/content/3.gif)
