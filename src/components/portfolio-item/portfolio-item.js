/* eslint import/no-dynamic-require:0 */
import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import * as Vibrant from 'node-vibrant';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|------------------------------------------------------------------------------
| Регистрация состояния компонента
|------------------------------------------------------------------------------
*/
store.registerModule('PortfolioItem', {
  namespaced: true,

  state() {
    return {
      db: require('./data/portfolio-item.data.json').reverse(),
      currentItem: {}
    };
  },

  getters: {
    /**
     * Количество работ в портфолио
     * @param {Object} state
     */
    worksCount(state) {
      return state.db.length;
    }
  },

  mutations: {
    /**
     * Определение данных текущей работы из портфолио
     * @param {Object} state состояние
     * @param {String} payload алиас
     */
    setCurrentItemData(state, payload) {
      // Выборка данных по работе
      state.currentItem = {
        ...state.db.filter(item => item.slug === payload)[0]
      };

      // Если есть описание то ренерируем разметку md->html и заменяем ей
      // значение 'description' на полученный html
      if (state.currentItem.description) {
        state.currentItem.description = require(`./data/portfolio-item.${
          state.currentItem.slug
        }.md`);
      }

      // Формирование массивов из мокапов и скриншотов
      state.currentItem = {
        ...state.currentItem,
        mockups: [],
        screenshots: []
      };
      for (let i = 1; i <= state.currentItem.mockupsCount; i += 1) {
        state.currentItem.mockups.push(
          `/assets/images/portfolio-item/${state.currentItem.slug}/${i}.jpg`
        );
      }
      for (let i = 1; i <= state.currentItem.screenshotsCount; i += 1) {
        state.currentItem.screenshots.push(
          `/assets/images/portfolio-item/${
            state.currentItem.slug
          }/screenshots/${i.toString().padStart(3, '0')}.jpg`
        );
      }

      // Получение значений цветовой палитры
      Vibrant.from(
        state.currentItem.screenshots[0] || state.currentItem.mockups[0],
        {
          colorCount: 2
        }
      )
        .getPalette()
        .then(palette => {
          state.currentItem = {
            ...state.currentItem,
            colors: palette,
            gradient: `linear-gradient(147deg, rgb(${parseInt(
              palette.DarkMuted ? palette.DarkMuted.r : palette.DarkVibrant.r,
              10
            )}, ${parseInt(
              palette.DarkMuted ? palette.DarkMuted.g : palette.DarkVibrant.g,
              10
            )}, ${parseInt(
              palette.DarkMuted ? palette.DarkMuted.b : palette.DarkVibrant.b,
              10
            )}) 0%, rgb(${parseInt(
              palette.LightMuted
                ? palette.LightMuted.r
                : palette.LightVibrant.r,
              10
            )}, ${parseInt(
              palette.LightMuted
                ? palette.LightMuted.g
                : palette.LightVibrant.g,
              10
            )}, ${parseInt(
              palette.LightMuted
                ? palette.LightMuted.b
                : palette.LightVibrant.b,
              10
            )}) 74%)`
          };
        });
    }
  }
});

/*
|------------------------------------------------------------------------------
| Объявление объекта опций компонента
|------------------------------------------------------------------------------
*/
const PortfolioItem = {
  template: require('./portfolio-item.htm'),

  computed: {
    ...mapState('PortfolioItem', ['db', 'currentItem']),

    ...mapGetters('PortfolioItem', ['worksCount'])
  },

  methods: {
    ...mapMutations('PortfolioItem', ['setCurrentItemData']),

    /**
     * Получене значения параметра из урла
     * @param {String} name имя параметра
     * @param {String} url урл
     */
    getParameterByName(name, url) {
      if (!url) url = window.location.href;
      name = name.replace(/[[\]]/g, '\\$&');
      const regex = new RegExp(`[?&]${name}(=([^&#]*)|&|#|$)`);
      const results = regex.exec(url);
      if (!results) return null;
      if (!results[2]) return '';
      return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }
  },

  created() {
    // Извлекаем данные по текущему элементу портфолио
    this.setCurrentItemData(this.getParameterByName('slug'));
  }
};

Vue.component('PortfolioItem', PortfolioItem);

export default PortfolioItem;
