import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import axios from 'axios';
import qs from 'qs';
import Inputmask from 'inputmask/dist/inputmask/inputmask';
import VeeValidate, { Validator } from 'vee-validate';
import ru from 'vee-validate/dist/locale/ru';
import 'animate.css/animate.css';
import './form-question.php';
import store from '../app/app.store';

Vue.config.productionTip = false;

Vue.use(VeeValidate, {
  locale: 'ru',
  inject: false
});

/*
|------------------------------------------------------------------------------
| Регистрация состояния компонента
|------------------------------------------------------------------------------
*/
store.registerModule('FormQuestion', {
  namespaced: true,

  state() {
    return {};
  },

  getters: {},

  mutations: {}
});

/*
|------------------------------------------------------------------------------
| Объявление объекта опций компонента
|------------------------------------------------------------------------------
*/
const FormQuestion = {
  data() {
    return {
      useUrlEncode: true,
      success: false,
      shake: false,
      tries: 0,
      confirmPolicy: false,
      formData: {}
    };
  },

  props: {
    action: {
      type: String,
      default: '/assets/forms/form-question.php'
    }
  },

  computed: {
    ...mapState('FormQuestion', []),

    ...mapGetters('FormQuestion', [])
  },

  inject: {
    $validator: '$validator'
  },

  methods: {
    ...mapMutations('FormQuestion', []),
    ...mapMutations('App', ['showModal']),

    /**
     * Счетчик попыток отправить форму
     * @param {*} state
     */
    incrementTries() {
      this.tries += 1;
      this.shake = true;
      setTimeout(() => {
        this.shake = false;
      }, 1000);
    },

    /**
     * Валидация формы и отправка данных
     * @param {Object} scope
     */
    validateBeforeSubmit(scope) {
      const vm = this;

      vm.$validator
        .validateAll(scope)
        .then(response => {
          if (response) {
            const post = vm.useUrlEncode
              ? qs.stringify(vm.formData)
              : vm.formData;
            axios
              .post(vm.action, post)
              .then(serverResponse => {
                vm.success = true;
                console.info('Отправлены данные:');
                console.info(post);
                console.info('Ответ сервера:');
                console.info(serverResponse);
              })
              .catch(error => {
                console.error(error);
              });
          } else {
            vm.incrementTries();
          }
        })
        .catch(error => {
          console.log(error);
        });
    },

    /**
     * Подключение маски
     * @param {String} selector
     */
    inputmask(selector) {
      Inputmask({ showMaskOnHover: false }).mask(
        document.querySelectorAll(selector)
      );
    }
  },

  mounted() {
    this.inputmask('.form-question [data-inputmask]');
  }
};

Vue.component('FormQuestion', FormQuestion);

export default FormQuestion;
