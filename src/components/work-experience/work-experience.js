import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import 'jarallax';
import moment from 'moment';
import 'moment/locale/ru';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|------------------------------------------------------------------------------
| Регистрация состояния компонента
|------------------------------------------------------------------------------
*/
store.registerModule('WorkExperience', {
  namespaced: true,

  state() {
    return {
      db: require('./work-experience.json').reverse()
    };
  },

  getters: {},

  mutations: {
    calculatePeriods(state) {
      /**
       * Склонение даты в зависимости от количества месяцев/лет
       * @param {Number} count количество
       * @param {'год'|'месяц'} word склоняемое слово
       */
      function humaniseDate(count, word) {
        if (word === 'год') {
          if (count === 1) {
            return 'год';
          } else if (count > 1 && count < 5) {
            return 'года';
          }
          return 'лет';
        } else if (word === 'месяц') {
          if (count === 1) {
            return 'месяц';
          } else if (count > 1 && count < 5) {
            return 'месяца';
          }
          return 'месяцев';
        }
        throw new Error('Неверно указано склоняемое слово');
      }

      // вычисление периода работы и добавление его к элементам
      state.db.forEach(item => {
        const endDate = item.time_end ? moment(item.time_end) : moment();
        const startDate = moment(item.time_start);
        const years = endDate.diff(startDate, 'year');
        startDate.add(years, 'years');
        const months = endDate.diff(startDate, 'months');
        startDate.add(months, 'months');
        item.period = `${
          years ? `${years} ${humaniseDate(years, 'год')}` : ''
        }${months ? ` ${months} ${humaniseDate(months, 'месяц')}` : ''}`;
      });
    }
  }
});

/*
|------------------------------------------------------------------------------
| Объявление объекта опций компонента
|------------------------------------------------------------------------------
*/
const WorkExperience = {
  template: require('./work-experience.htm'),

  data() {
    return {
      plugins: {
        jarallax: {
          enable: true,
          options: {
            speed: 0.2
          }
        }
      }
    };
  },

  computed: {
    ...mapState('WorkExperience', ['db']),

    ...mapGetters('WorkExperience', [])
  },

  methods: {
    ...mapMutations('WorkExperience', ['calculatePeriods']),

    initParallax() {
      jarallax(this.$el, this.plugins.jarallax.options);
    }
  },

  filters: {
    // формирование даты
    date(value) {
      if (!value) {
        return 'по настоящее время';
      }
      return moment(value).format('MMMM YYYY');
    }
  },

  created() {
    this.calculatePeriods();
  },

  mounted() {
    if (this.plugins.jarallax.enable) this.initParallax();
  }
};

Vue.component('WorkExperience', WorkExperience);

export default WorkExperience;
