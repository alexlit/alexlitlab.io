import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|-------------------------------------------------------------------------------
| Регистрация состояния компонента
|-------------------------------------------------------------------------------
*/
store.registerModule('SocialLinks', {
  namespaced: true,

  state() {
    return {};
  },

  getters: {},

  mutations: {}
});

/*
|-------------------------------------------------------------------------------
| Объявление объекта опций компонента
|-------------------------------------------------------------------------------
*/
const SocialLinks = {
  template: require('./social-links.htm'),

  data() {
    return {
      items: [
        {
          type: 'gh',
          href: 'https://github.com/alex-lit'
        },
        {
          type: 'gl',
          href: 'https://gitlab.com/alexlit'
        },
        {
          type: 'vk',
          href: 'https://vk.com/alexlit_webdev'
        },
        {
          type: 'hh',
          href: 'https://sevastopol.hh.ru/resume/ec5eb6e9ff023f2e180039ed1f674a724f6254'
        }
      ]
    };
  },

  computed: {
    ...mapState('SocialLinks', []),
    ...mapGetters('SocialLinks', [])
  },

  methods: {
    ...mapMutations('SocialLinks', [])
  }
};

Vue.component('SocialLinks', SocialLinks);

export default SocialLinks;
