import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import LazyLoad from 'vanilla-lazyload';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|------------------------------------------------------------------------------
| Регистрация состояния компонента
|------------------------------------------------------------------------------
*/
store.registerModule('RecentProjects', {
  namespaced: true,

  state() {
    return {};
  },

  getters: {},

  mutations: {}
});

/*
|------------------------------------------------------------------------------
| Объявление объекта опций компонента
|------------------------------------------------------------------------------
*/
const RecentProjects = {
  template: require('./recent-projects.htm'),

  data() {
    return {
      isAllItemsDisplayed: false
    };
  },

  computed: {
    ...mapState('RecentProjects', []),
    ...mapState('PortfolioItem', ['db']),

    ...mapGetters('RecentProjects', []),

    /**
     * Выборка последних работ из портфолио
     */
    items() {
      return this.db.slice(0, 9);
    }
  },

  filters: {
    /**
     * Формирование названия работы из алиаса
     * @param {String} value - значение
     */
    formatName(value) {
      return value.replace(/-/g, ' ');
    }
  },

  methods: {
    ...mapMutations('RecentProjects', []),

    /**
     * Ленивая загрузка изображений
     * @param {Srting} selector селектор элемента
     */
    lazyLoadImages(selector) {
      const recentProjectsLazyLoad = new LazyLoad({
        elements_selector: selector,
        threshold: 2000
      });
      recentProjectsLazyLoad.update();
    },

    /**
     * Отобразить все работы
     */
    showAllItems() {
      setTimeout(() => {
        this.isAllItemsDisplayed = true;
      }, 1000);
    }
  },

  mounted() {
    this.lazyLoadImages('.recent-projects [data-src]');
  }
};

Vue.component('RecentProjects', RecentProjects);

export default RecentProjects;
