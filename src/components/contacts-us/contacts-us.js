import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import particles from 'exports-loader?particlesJS=window.particlesJS,window.pJSDom!particles.js';
import 'jarallax';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|------------------------------------------------------------------------------
| Регистрация состояния компонента
|------------------------------------------------------------------------------
*/
store.registerModule('ContactsUs', {
  namespaced: true,

  state() {
    return {};
  },

  getters: {},

  mutations: {}
});

/*
|------------------------------------------------------------------------------
| Объявление объекта опций компонента
|------------------------------------------------------------------------------
*/
const ContactsUs = {
  data() {
    return {
      plugins: {
        particles: {
          enable: true,
          options: {
            particles: {
              number: {
                value: 160,
                density: {
                  enable: true,
                  value_area: 800
                }
              },
              color: {
                value: '#ffffff'
              },
              shape: {
                type: 'circle',
                stroke: {
                  width: 0,
                  color: '#000000'
                },
                polygon: {
                  nb_sides: 5
                },
                image: {
                  src: 'img/github.svg',
                  width: 0,
                  height: 0
                }
              },
              opacity: {
                value: 0,
                random: false,
                anim: {
                  enable: false,
                  speed: 1,
                  opacity_min: 0.1,
                  sync: false
                }
              },
              size: {
                value: 0,
                random: false,
                anim: {
                  enable: false,
                  speed: 40,
                  size_min: 0.1,
                  sync: false
                }
              },
              line_linked: {
                enable: true,
                distance: 60,
                color: '#ffffff',
                opacity: 0.3,
                width: 1
              },
              move: {
                enable: true,
                speed: 3,
                direction: 'none',
                random: false,
                straight: false,
                out_mode: 'out',
                bounce: false,
                attract: {
                  enable: false,
                  rotateX: 600,
                  rotateY: 1200
                }
              }
            },
            interactivity: {
              detect_on: 'canvas',
              events: {
                onhover: {
                  enable: false,
                  mode: 'repulse'
                },
                onclick: {
                  enable: false,
                  mode: 'push'
                },
                resize: true
              },
              modes: {
                grab: {
                  distance: 400,
                  line_linked: {
                    opacity: 1
                  }
                },
                bubble: {
                  distance: 400,
                  size: 40,
                  duration: 2,
                  opacity: 8,
                  speed: 3
                },
                repulse: {
                  distance: 200,
                  duration: 0.4
                },
                push: {
                  particles_nb: 4
                },
                remove: {
                  particles_nb: 2
                }
              }
            },
            retina_detect: true
          }
        },
        jarallax: {
          enable: true,
          options: {
            speed: 0.2
          }
        }
      }
    };
  },
  computed: {
    ...mapState('ContactsUs', []),

    ...mapGetters('ContactsUs', [])
  },

  methods: {
    ...mapMutations('ContactsUs', []),

    initParallax() {
      jarallax(
        this.$el.querySelectorAll('[data-parallax]'),
        this.plugins.jarallax.options
      );
    }
  },

  mounted() {
    if (this.plugins.jarallax.enable) {
      this.initParallax();
    }
    if (this.plugins.particles.enable) {
      particles.particlesJS(
        'contact-us-content-wrap',
        this.plugins.particles.options
      );
    }
  }
};

Vue.component('ContactsUs', ContactsUs);

export default ContactsUs;
