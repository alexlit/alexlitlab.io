/* eslint  no-unused-vars: 0 */

import Vue from 'vue/dist/vue';
import ElementUI from 'element-ui';
import locale from 'element-ui/lib/locale/lang/ru-RU';
import 'element-ui/lib/theme-chalk/index.css';
import PortalVue from 'portal-vue';
import VueScrollTo from 'vue-scrollto';
import VueMq from 'vue-mq';
import VueObserveVisibility from 'vue-observe-visibility';
// import 'vue-clicky';
import bowser from 'bowser';
import objectFitImages from 'object-fit-images';
import 'normalize.css/normalize.css';
import store from './app.store';

Vue.use(ElementUI, { locale });
Vue.use(PortalVue);
Vue.use(VueObserveVisibility);
Vue.use(VueScrollTo, {
  container: 'body',
  duration: 1000,
  easing: 'easeInOutCubic',
  offset: -48, // зависит от высоты меню
  cancelable: true,
  onDone: false,
  onCancel: false,
  x: false,
  y: true
});
Vue.use(VueMq, {
  breakpoints: {
    mobile: 480,
    tablet: 1240,
    laptop: Infinity
  }
});

Vue.config.productionTip = false;
Vue.config.performance = process.env.NODE_ENV === 'dev';
Vue.config.devtools = process.env.NODE_ENV === 'dev';
Vue.config.ignoredElements = [
  'vue-ignore',
  'noindex',
  'code',
  'pre',
  'no-typography'
];

const App = new Vue({
  el: '#app',

  name: 'App',

  delimiters: ['[[', ']]'],

  store,

  data: {
    device: {},
    options: {
      cqProlyfill: {
        enable: false,
        options: { preprocess: true }
      },
      serviceWorkers: {
        enable: process.env.NODE_ENV !== 'dev'
      },
      disableUserSelect: {
        enable: false
      }
    },
    plugins: {
      bowser: {
        enable: true
      },
      objectFitImages: {
        enable: true,
        options: {
          watchMQ: true
        }
      }
    },
    noScroll: false
  },

  watch: {
    /**
     * Блокировка скролла
     * @param {Boolean} newValue
     */
    noScroll(newValue) {
      if (newValue) {
        document.body.style.paddingRight =
          this.$mq === 'laptop' ? '17px' : 'initial';
        document.body.style.overflow = 'hidden';
      } else {
        document.body.style.paddingRight = 'initial';
        document.body.style.overflow = 'initial';
      }
    },

    /**
     * Отслеживания состояния фрейма. При закрытии фрейма разблокируем скролл
     * только после его скрытия
     */
    '$store.state.WorkFrame.isActive': function(newValue) {
      if (newValue) {
        this.noScroll = newValue;
      } else {
        setTimeout(() => {
          this.noScroll = newValue;
        }, 400);
      }
    },

    /**
     * Отслеживания состояния мобильного меню. При закрытии меню разблокируем
     * скролл только после его скрытия
     */
    '$store.state.MainMenu.data.isActive': function(newValue) {
      if (newValue) {
        this.noScroll = newValue;
      } else {
        setTimeout(() => {
          this.noScroll = newValue;
        }, 400);
      }
    }
  },

  methods: {
    /**
     * Регистрация сервис-воркера
     */
    registerServiceWorkers() {
      if ('serviceWorker' in navigator) {
        navigator.serviceWorker.register('service-worker.js');
      }
    },

    /**
     * Запрет пользователю копировать содержимое
     */
    disableUserSelect() {
      document.ondragstart = false;
      document.onselectstart = false;
      document.oncontextmenu = false;
    },

    /**
     * Получение сведений о браузере и системе пользователя
     */
    bowser() {
      function detectBrowserEngine() {
        let browser;
        if (bowser.blink) {
          browser = 'blink';
        } else if (bowser.webkit) {
          browser = 'webkit';
        } else if (bowser.gecko) {
          browser = 'gecko';
        } else if (bowser.msie) {
          browser = 'msie';
        } else if (bowser.msedge) {
          browser = 'msedge';
        }
        return browser;
      }

      function detectDeviceType() {
        let deviceType;
        if (bowser.mobile) {
          deviceType = 'mobile';
        } else if (bowser.tablet) {
          deviceType = 'tablet';
        } else {
          deviceType = 'laptop';
        }
        return deviceType;
      }

      function detectOs() {
        let os;
        if (bowser.mac) {
          os = 'mac';
        } else if (bowser.windows) {
          os = 'windows';
        } else if (bowser.windowsphone) {
          os = 'windowsphone';
        } else if (bowser.linux) {
          os = 'linux';
        } else if (bowser.chromeos) {
          os = 'chromeos';
        } else if (bowser.android) {
          os = 'android';
        } else if (bowser.ios) {
          os = 'ios';
        } else if (bowser.blackberry) {
          os = 'blackberry';
        } else if (bowser.firefoxos) {
          os = 'firefoxos';
        } else if (bowser.webos) {
          os = 'webos';
        } else if (bowser.bada) {
          os = 'bada';
        } else if (bowser.tizen) {
          os = 'tizen';
        } else if (bowser.sailfish) {
          os = 'sailfish';
        }
        return os;
      }

      this.device.browser = bowser.name;
      this.device.browserVersion = bowser.version;
      this.device.browserEngine = detectBrowserEngine();
      this.device.type = detectDeviceType();
      this.device.os = detectOs();
      this.device.osVersion = bowser.osversion;
    },

    /**
     * Инициализация полифила для object-fit
     */
    objectFitImages() {
      objectFitImages(null, this.plugins.objectFitImages.options);
    },

    /**
     * Инициализация полифила container query
     */
    initContainerQueryPolyphill() {
      const cq = require('cq-prolyfill')(this.cqProlyfill.options);
    },

    /**
     * Установка метатегов
     */
    setMetaTags() {
      // Страница работы из портфолио
      if (Object.keys(this.$store.state.PortfolioItem.currentItem).length > 0) {
        // title
        document.querySelector(
          'title'
        ).text = `Портфолио Front-end разработчика: ${
          this.$store.state.PortfolioItem.currentItem.text
        }`;
        // description
        document
          .querySelector('meta[name="description"]')
          .setAttribute(
            'content',
            `Тип работы: ${
              this.$store.state.PortfolioItem.currentItem.type
            }. Год разработки: ${
              this.$store.state.PortfolioItem.currentItem.date
            }.`
          );
        // keywords
        document.querySelector('meta[name="keywords"]').setAttribute(
          'content',
          this.$store.state.PortfolioItem.currentItem.text
            .replace(/"/g, '')
            .toLowerCase()
            .split(' ')
        );
      }
    }
  },

  beforeMount() {
    if (this.plugins.bowser.enable) this.bowser();
  },

  mounted() {
    this.setMetaTags();

    if (this.options.serviceWorkers.enable) this.registerServiceWorkers();
    if (this.options.disableUserSelect.enable) this.disableUserSelect();
    if (this.plugins.bowser.enable) this.bowser();
    if (this.plugins.objectFitImages.enable) this.objectFitImages();
    if (this.options.cqProlyfill.enable) this.initContainerQueryPolyphill();
  }
});
