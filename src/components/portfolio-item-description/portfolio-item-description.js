import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import Zooming from 'zooming';
import _ from 'lodash';
import hljs from 'highlight.js/lib/highlight';
import javascript from 'highlight.js/lib/languages/javascript';
import twig from 'highlight.js/lib/languages/twig';
import css from 'highlight.js/lib/languages/css';
import xml from 'highlight.js/lib/languages/xml';
import shell from 'highlight.js/lib/languages/shell';
import 'highlight.js/styles/atom-one-light.css';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|------------------------------------------------------------------------------
| Регистрация состояния компонента
|------------------------------------------------------------------------------
*/
store.registerModule('PortfolioItemDescription', {
  namespaced: true,

  state() {
    return {
      db: require('./data/portfolio-item-description.json')
    };
  },

  getters: {},

  mutations: {}
});

/*
|------------------------------------------------------------------------------
| Объявление объекта опций компонента
|------------------------------------------------------------------------------
*/
const PortfolioItemDescription = {
  template: require('./portfolio-item-description.htm'),

  data() {
    return {
      item: null
    };
  },

  computed: {
    ...mapState('PortfolioItemDescription', ['db']),
    ...mapState('PortfolioItem', ['currentItem']),
    ...mapState('TechnologiesSlideshow', {
      currentItemTechnologies(state) {
        return _.orderBy(state.currentItemTechnologies, 'name', 'asc');
      }
    }),

    ...mapGetters('PortfolioItemDescription', [])
  },

  methods: {
    ...mapMutations('PortfolioItemDescription', []),

    /**
     * Инициализация подсветки синтаксиса
     */
    initHighlightJs() {
      hljs.registerLanguage('javascript', javascript);
      hljs.registerLanguage('twig', twig);
      hljs.registerLanguage('css', css);
      hljs.registerLanguage('xml', xml);
      hljs.registerLanguage('shell', shell);

      hljs.initHighlightingOnLoad();
    },

    /**
     * Инициализация масштабирования изображений
     * @param {Object} options опции
     */
    initZooming(
      options = {
        bgColor: '#212121',
        bgOpacity: 0.9
      }
    ) {
      const zooming = new Zooming(options);
      zooming.listen('.portfolio-item-description .typography img');
    }
  },

  created() {
    // получение информации о типе работы
    this.item = this.db.filter(item => item.type === this.currentItem.type)[0];
  },

  mounted() {
    this.initHighlightJs();
    this.initZooming();
  }
};

Vue.component('PortfolioItemDescription', PortfolioItemDescription);

export default PortfolioItemDescription;
