/* eslint no-undef: 0 */

import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import 'lightgallery.js/dist/css/lightgallery.css';
import 'lightgallery.js';
import 'lg-zoom.js';
import 'lg-thumbnail.js';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|------------------------------------------------------------------------------
| Регистрация состояния компонента
|------------------------------------------------------------------------------
*/
store.registerModule('ReceivedSertificates', {
  namespaced: true,

  state() {
    return {
      db: require('./received-sertificates.json').reverse()
    };
  },

  getters: {},

  mutations: {}
});

/*
|------------------------------------------------------------------------------
| Объявление объекта опций компонента
|------------------------------------------------------------------------------
*/
const ReceivedSertificates = {
  template: require('./received-sertificates.htm'),

  data() {
    return {
      id: `received-sertificates-${Math.random() // id для инициализации галерей
        .toString(36)
        .substring(2)}`,
      isAnimateItems: false // декоративная анимация при наведении на кнопку
    };
  },

  computed: {
    ...mapState('ReceivedSertificates', ['db']),

    ...mapGetters('ReceivedSertificates', [])
  },

  methods: {
    ...mapMutations('ReceivedSertificates', []),

    /**
     * Инициализация галереи
     * @param {String} id
     * @param {Object} options
     */
    initLightGallery(id, options) {
      lightGallery(document.getElementById(id), options);
    }
  }
};

Vue.component('ReceivedSertificates', ReceivedSertificates);

export default ReceivedSertificates;
