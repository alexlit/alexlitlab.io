import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|------------------------------------------------------------------------------
| Регистрация состояния компонента
|------------------------------------------------------------------------------
*/
store.registerModule('PrivacyPolicy', {
  namespaced: true,

  state() {
    return {};
  },

  getters: {},

  mutations: {}
});

/*
|------------------------------------------------------------------------------
| Объявление объекта опций компонента
|------------------------------------------------------------------------------
*/
const PrivacyPolicy = {
  computed: {
    ...mapState('PrivacyPolicy', []),

    ...mapGetters('PrivacyPolicy', [])
  },

  methods: {
    ...mapMutations('PrivacyPolicy', [])
  }
};

Vue.component('PrivacyPolicy', PrivacyPolicy);

export default PrivacyPolicy;