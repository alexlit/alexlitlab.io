import Vue from 'vue/dist/vue';

Vue.config.productionTip = false;

/*
|------------------------------------------------------------------------------
| Объявление объекта опций компонента
|------------------------------------------------------------------------------
*/
const UiSpinner = {
  delimiters: ['[[', ']]'],

  template: require('./ui-spinner.htm'),

  data() {
    return {
      // возможные шаблоны
      layouts: [
        'chasing-dots',
        'circle',
        'cube-grid',
        'double-bounce',
        'fading-circle',
        'folding-cube',
        'pulse',
        'rectangle-bounce',
        'rotating-plane',
        'three-bounce',
        'wandering-cubes'
      ],
      activeLayout: null
    };
  },

  computed: {
    /**
     * Случайный шаблон
     */
    randomLayout() {
      return this.layouts[Math.floor(Math.random() * this.layouts.length)];
    }
  },

  props: {
    // использовать случайный шаблон?
    random: {
      type: Boolean,
      default: false
    },

    // имя шаблона
    layout: {
      type: String,
      default: 'rotating-plane',
      validator(value) {
        return (
          [
            'chasing-dots',
            'circle',
            'cube-grid',
            'double-bounce',
            'fading-circle',
            'folding-cube',
            'pulse',
            'rectangle-bounce',
            'rotating-plane',
            'three-bounce',
            'wandering-cubes'
          ].indexOf(value) !== -1
        );
      }
    },

    // цвет
    color: {
      type: String,
      default: '#fff'
    },

    // прозрачность
    opacity: {
      type: Number,
      default: 1,
      validator(value) {
        return value >= 0 && value <= 1;
      }
    },

    // размер в px
    size: {
      type: Number,
      default: 40
    }
  },

  mounted() {
    if (this.random) {
      this.activeLayout = this.randomLayout;
    } else {
      this.activeLayout = this.layout;
    }
  }
};

Vue.component('UiSpinner', UiSpinner);

export default UiSpinner;
