/* eslint no-unused-vars: 0 */
import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import Flickity from 'flickity';
import 'flickity-imagesloaded';
import 'flickity/css/flickity.css';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|-------------------------------------------------------------------------------
| Регистрация состояния компонента
|-------------------------------------------------------------------------------
*/
store.registerModule('TestimonialsCarousel', {
  namespaced: true,

  state() {
    return {};
  },

  getters: {},

  mutations: {}
});

/*
|-------------------------------------------------------------------------------
| Объявление объекта опций компонента
|-------------------------------------------------------------------------------
*/
const TestimonialsCarousel = {
  template: require('./testimonials-carousel.htm'),

  data() {
    return {
      rate: 5,
      plugins: {
        flickity: {
          enable: true,
          options: [
            {
              cellAlign: 'center',
              contain: true,
              pageDots: false,
              prevNextButtons: false,
              lazyLoad: true,
              wrapAround: true,
              autoPlay: 5000,
              pauseAutoPlayOnHover: false
            },
            {
              contain: true,
              cellAlign: 'center',
              prevNextButtons: false,
              pageDots: false,
              wrapAround: true,
              autoPlay: 5000,
              lazyLoad: 3,
              asNavFor: '.testimonials-carousel [data-slideshow]'
            }
          ]
        }
      }
    };
  },

  computed: {
    ...mapState('TestimonialsCarousel', []),

    ...mapGetters('TestimonialsCarousel', [])
  },

  methods: {
    ...mapMutations('TestimonialsCarousel', []),

    /**
     * Инициализация карусели
     */
    initSlideshow() {
      const vm = this;
      const technologiesCarousel = new Flickity(
        '.testimonials-carousel [data-slideshow]',
        vm.plugins.flickity.options[0]
      );
      const technologiesCarouselNav = new Flickity(
        '.testimonials-carousel [data-slideshow-thumbs]',
        vm.plugins.flickity.options[1]
      );
    }
  },

  mounted() {
    this.initSlideshow();
  }
};

Vue.component('TestimonialsCarousel', TestimonialsCarousel);

export default TestimonialsCarousel;
