document.addEventListener('DOMContentLoaded', () => {
  // forEach fix for IE

  if (typeof NodeList.prototype.forEach !== 'function')
    NodeList.prototype.forEach = Array.prototype.forEach;

  // srcollable tables
  function srcollableTables() {
    const tables = document.querySelectorAll('.typography table');

    if (tables) {
      tables.forEach(table => {
        if (table.clientWidth >= table.closest('.typography').clientWidth) {
          table.style.display = 'block';
        } else {
          table.style.display = 'table';
        }
      });
    }
  }
  srcollableTables();
  window.addEventListener('resize', () => {
    srcollableTables();
  });
});
