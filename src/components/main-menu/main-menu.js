import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|------------------------------------------------------------------------------
| Регистрация состояния компонента
|------------------------------------------------------------------------------
*/
store.registerModule('MainMenu', {
  namespaced: true,

  state() {
    return {
      data: {
        isFixed: false, // меню зафиксировано?
        isActive: false // видимость мобильного меню
      }
    };
  },

  getters: {},

  mutations: {
    /**
     * Фиксация меню при скролле
     * @param {Object} state - состояние компонента
     */
    fixMenu(state) {
      window.addEventListener('scroll', () => {
        state.data.isFixed = window.pageYOffset > 0;
      });
    },

    /**
     * Изменение состояния мобильного меню
     * @param {Object} state состояние компонента
     * @param {Boolean} isActive состояние видимости меню
     */
    changeActiveState(state, isActive) {
      if (isActive === undefined) {
        state.data.isActive = !state.data.isActive;
      } else if (typeof isActive === 'boolean') {
        state.data.isActive = isActive;
      } else {
        throw new Error('Передан неверный параметр');
      }
    }
  }
});

/*
|------------------------------------------------------------------------------
| Объявление объекта опций компонента
|------------------------------------------------------------------------------
*/
const MainMenu = {
  template: require('./main-menu.htm'),

  props: {
    page: {
      type: String,
      default: 'main'
    }
  },

  computed: {
    ...mapState('MainMenu', ['data']),
    ...mapGetters('MainMenu', [])
  },

  methods: {
    ...mapMutations('MainMenu', ['fixMenu', 'changeActiveState'])
  },

  mounted() {
    this.fixMenu();

    window.addEventListener('resize', () => {
      this.changeActiveState(false);
    })
  }
};

Vue.component('MainMenu', MainMenu);

export default MainMenu;
