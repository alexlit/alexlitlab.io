import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import 'jarallax';
import 'jarallax/dist/jarallax-video.min';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|------------------------------------------------------------------------------
| Регистрация состояния компонента
|------------------------------------------------------------------------------
*/
store.registerModule('MainPromo', {
  namespaced: true,

  state() {
    return {};
  },

  getters: {
    /**
     * Вычисление текущего стажа
     * @return {Number} - стаж в годах
     */
    currentStage() {
      const currentYear = new Date();
      return currentYear.getFullYear() - 2012;
    }
  },

  mutations: {}
});

/*
|------------------------------------------------------------------------------
| Объявление объекта опций компонента
|------------------------------------------------------------------------------
*/
const MainPromo = {
  template: require('./main-promo.htm'),

  data() {
    return {
      plugins: {
        jarallax: {
          enable: true,
          options: {
            speed: 0.2
          }
        }
      }
    };
  },

  props: {
    image: {
      type: String,
      default: '/assets/images/main-promo.bgd.jpg'
    },
    video: {
      type: String,
      default: 'mp4:/assets/videos/main-promo.love-coding.mp4'
    },
    title: {
      type: String,
      default: 'Привет, я Алексей'
    },
    subtitle: {
      type: String,
      default: 'Front-end developer'
    },
    text: {
      type: String,
      default: `
        <p>Я занимаюсь версткой и разработкой клиентской части для коммерческих проектов.</p>
        <p>7 лет опыта работы, более 60 сертификатов и 200 завершенных проектов.</p>
        <p>Текущий стэк: Vue | Vuex | Vue router, PostCSS | BEM</p>
      `
    },
    isAvatarVisible: {
      type: Boolean,
      default: false
    }
  },

  computed: {
    ...mapState('MainPromo', ['data']),
    ...mapGetters('MainPromo', ['currentStage'])
  },

  methods: {
    ...mapMutations('MainPromo', []),

    initParallax() {
      jarallax(this.$el, this.plugins.jarallax.options);
    }
  },

  mounted() {
    if (this.plugins.jarallax.enable) this.initParallax();
  }
};

Vue.component('MainPromo', MainPromo);

export default MainPromo;
