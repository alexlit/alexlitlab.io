import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import 'jarallax';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|------------------------------------------------------------------------------
| Регистрация состояния компонента
|------------------------------------------------------------------------------
*/
store.registerModule('SimpleParallax', {
  namespaced: true,

  state() {
    return {};
  },

  getters: {},

  mutations: {}
});

/*
|------------------------------------------------------------------------------
| Объявление объекта опций компонента
|------------------------------------------------------------------------------
*/
const SimpleParallax = {
  template: require('./simple-parallax.htm'),

  data() {
    return {
      options: {
        isBlur: true
      },
      plugins: {
        jarallax: {
          enable: true,
          options: {
            speed: 0.2
          }
        }
      }
    };
  },

  props: {
    // путь до изображения
    image: {
      type: String
    }
  },

  computed: {
    ...mapState('SimpleParallax', []),

    ...mapState('PortfolioItem', ['currentItem']),

    ...mapGetters('SimpleParallax', [])
  },

  methods: {
    ...mapMutations('SimpleParallax', []),

    /**
     * Подключение параллакса
     */
    initParallax() {
      jarallax(this.$el, this.plugins.jarallax.options);
    },

    /**
     * Ослеживание видимости элемента
     * @param {Boolean} isVisible видим ли эелемент
     */
    visibilityChanged(isVisible) {
      this.options.isBlur = !isVisible;
    }
  },

  mounted() {
    if (this.plugins.jarallax.enable) this.initParallax();
  }
};

Vue.component('SimpleParallax', SimpleParallax);

export default SimpleParallax;
