/* eslint no-undef: 0 */

import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import 'lightgallery.js/dist/css/lightgallery.css';
import 'lightgallery.js';
import 'lg-zoom.js';
import 'lg-thumbnail.js';
// import 'lg-hash.js';
import 'lg-fullscreen.js';
import 'jarallax';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|------------------------------------------------------------------------------
| Регистрация состояния компонента
|------------------------------------------------------------------------------
*/
store.registerModule('PortfolioItemPromo', {
  namespaced: true,

  state() {
    return {};
  },

  getters: {},

  mutations: {}
});

/*
|------------------------------------------------------------------------------
| Объявление объекта опций компонента
|------------------------------------------------------------------------------
*/
const PortfolioItemPromo = {
  template: require('./portfolio-item-promo.htm'),

  data() {
    return {
      id: `portfolio-item-promo-${Math.random() // id для инициализации галерей
        .toString(36)
        .substring(2)}`,
      isPreviewAvailable: false, // доступен ли сайт для отображения во фрейме
      plugins: {
        jarallax: {
          enable: true,
          options: {
            speed: -0.6
          }
        }
      }
    };
  },

  computed: {
    ...mapState('PortfolioItemPromo', []),
    ...mapState('PortfolioItem', ['currentItem']),
    ...mapState('WorkFrame', ['isActive']),

    ...mapGetters('PortfolioItemPromo', []),

    agency() {
      switch (this.currentItem.agency) {
      case 'WebSEOHelp':
        return 'Веб-студия "WebSEOHelp"';
      case 'WebMargin':
        return 'Веб-студия "WebMargin"';
      case 'Growwweb':
        return 'Веб-студия "Growwweb"';
      case 'Krown':
        return 'Аналитическое digital-агентство "Krown"';
      case 'Yellow-Media':
        return 'Веб-студия "Yellow-Media"';
      case 'Прибыльный контекст':
        return 'Веб-студия "Прибыльный контекст"';
      case 'частный заказ':
        return 'частный заказ';
      default:
        return 'личный проект';
      }
    }
  },

  methods: {
    ...mapMutations('PortfolioItemPromo', []),
    ...mapMutations('WorkFrame', ['setFrameVisibility']),

    /**
     * Инициализация параллакса
     */
    initParallax() {
      jarallax(
        document.querySelectorAll('.portfolio-item-promo[data-parallax]'),
        this.plugins.jarallax.options
      );
    },

    /**
     * Проверка домена на доступность
     * Проверяется только домен, если домен доступен но сайт лежит то все равно
     * будет считаться что сайт доступен и кнопка "посмотреть сайт" отобразится.
     * @param {String} link - УРЛ сайта для проверки
     */
    checkDonorSite(link) {
      if (!link) return;
      fetch(link, { mode: 'no-cors' })
        .then(() => {
          this.isPreviewAvailable = true;
        })
        .catch(() => {
          this.isPreviewAvailable = false;
        });
    },

    /**
     * Инициализация галереи
     * @param {String} id
     * @param {Object} options
     */
    initLightGallery(id, options) {
      lightGallery(document.getElementById(id), options);
    }
  },

  filters: {
    /**
     * Формирование названия работы из алиаса
     * @param {String} value - значение
     */
    formatName(value) {
      return value.replace(/-/g, ' ');
    }
  },

  beforeMount() {
    this.checkDonorSite(this.currentItem.link);
  },

  mounted() {
    if (this.plugins.jarallax.enable) this.initParallax();
  }
};

Vue.component('PortfolioItemPromo', PortfolioItemPromo);

export default PortfolioItemPromo;
