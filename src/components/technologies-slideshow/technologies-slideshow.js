/* eslint no-unused-vars: 0 */
import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import Flickity from 'flickity';
import 'flickity-as-nav-for';
import 'flickity-imagesloaded';
import 'flickity/css/flickity.css';
import _ from 'lodash';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|------------------------------------------------------------------------------
| Регистрация состояния компонента
|------------------------------------------------------------------------------
*/
store.registerModule('TechnologiesSlideshow', {
  namespaced: true,

  state() {
    return {
      db: require('./technologies-slideshow.json'),
      currentItemTechnologies: []
    };
  },

  getters: {},

  mutations: {
    /**
     * Фильтрация технологий
     * @param {[String]} payload массив технологий
     */
    getCurrentItemTechnologies(state, payload) {
      state.currentItemTechnologies = _.orderBy(
        state.db.filter(item => payload.indexOf(item.image) !== -1),
        'name',
        'asc'
      );
    }
  }
});

/*
|------------------------------------------------------------------------------
| Объявление объекта опций компонента
|------------------------------------------------------------------------------
*/
const TechnologiesSlideshow = {
  template: require('./technologies-slideshow.htm'),

  data() {
    return {
      plugins: {
        flickity: {
          enable: true,
          options: {
            cellAlign: 'center',
            contain: true,
            pageDots: false,
            prevNextButtons: false,
            lazyLoad: 1,
            wrapAround: true,
            autoPlay: 3000,
            pauseAutoPlayOnHover: false
          }
        }
      }
    };
  },

  computed: {
    ...mapState('TechnologiesSlideshow', ['db', 'currentItemTechnologies']),
    ...mapState('PortfolioItem', ['currentItem']),
    ...mapGetters('TechnologiesSlideshow', [])
  },

  methods: {
    ...mapMutations('TechnologiesSlideshow', ['getCurrentItemTechnologies']),

    /**
     * Инициализация карусели
     */
    initSlideshow() {
      const vm = this;
      const technologiesCarousel = new Flickity(
        '.technologies-slideshow [data-slideshow]',
        vm.plugins.flickity.options
      );
      Array.from(
        document.querySelectorAll(
          '.technologies-slideshow [data-slideshow-item]'
        )
      ).forEach(item => {
        item.style.height = 'inherit';
      });
    }
  },

  beforeMount() {
    this.getCurrentItemTechnologies(this.currentItem.technologies);
  },

  mounted() {
    this.initSlideshow();
  }
};

Vue.component('TechnologiesSlideshow', TechnologiesSlideshow);

export default TechnologiesSlideshow;
