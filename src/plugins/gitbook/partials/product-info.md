{% extends '../_layouts/custom.htm' %}

[//]: HEADER

{% block header %}

# ProductInfo

{% endblock %}

[//]: COMPONENT

{% block component %}product-info{% endblock %}

[//]: API

{% block api %}

API отсутствует.

{% endblock %}

[//]: ASSETS

{% block assets %}

{% method %}

### Пример использования

```HTML
<product-info inline-template v-cloak>
  <article class="product-info">
    <div class="product-info__content-wrap">
      <div class="product-info__terms typography">
        <h3>Условия аренды:</h3>
        <ul>
          <li>Погрузчик JCB 3CX может выполнить работы как в день, так и в ночь</li>
          <li>Работаем по Москве и Подмосковью</li>
          <li>Предлагаем аренду спецтехники с нашим водителем(экипажем)</li>
          <li>Окажем услуги в день работы, но не позже, чем за 2 часа до начала мероприятия</li>
          <li>Оформляем предварительные заказы на желаемую дату</li>
          <li>Скидки при долгосрочной аренде и регулярных заказах</li>
        </ul>
      </div>
      <div class="typography">
        <h3>Описание:</h3>
        <span></span>
      </div>
      <div class="product-info__caption">
        <div class="product-info__caption-col typography">
          <p>«Винтел» сдает в аренду экскаватор-погрузчик JCB 4CX колесного типа, характеризующийся высокой производительностью в своем классе при сниженном на 16–25 % потреблении топлива. Для минимизации расходов клиентам предоставляется выбор между посуточным или почасовым тарифами.</p>
          <p>Экскаватор-погрузчик JCB 4CX имеет 4 колеса и за счет большого просвета обладает высокой скоростью и маневренностью. Повышенная проходимость обеспечивается за счет особой конструкции мостов и геометрии покрышек. Кабина экскаватора-погрузчика JCB 4CX оснащена ЖК-панелью, на ней отображается точное время работы машины.</p>
          <p>Техника подходит для выполнения земляных и строительных работ в черте города и в условиях пересеченной местности. Она используется для копания, точного профилирования грунта, погрузочно-разгрузочных работ с сыпучими и штучными грузами. Машина подходит для эксплуатации в любое время года (независимо от климата).</p>
        </div>
        <aside class="product-info__caption-col">
          <img src="/assets/images/product-info.1.jpg" alt="">
        </aside>
      </div>
    </div>
  </article>
</product-info>
```

{% sample lang='template' %}

### Template

```TWIG
{% macro ProductInfo() %}
<!-- product-info -->
<product-info inline-template v-cloak>
  <article class="product-info">
    <div class="product-info__content-wrap">
      <div class="product-info__terms typography">
        <h3>Условия аренды:</h3>
        <ul>
          <li>Погрузчик JCB 3CX может выполнить работы как в день, так и в ночь</li>
          <li>Работаем по Москве и Подмосковью</li>
          <li>Предлагаем аренду спецтехники с нашим водителем(экипажем)</li>
          <li>Окажем услуги в день работы, но не позже, чем за 2 часа до начала мероприятия</li>
          <li>Оформляем предварительные заказы на желаемую дату</li>
          <li>Скидки при долгосрочной аренде и регулярных заказах</li>
        </ul>
      </div>
      <div class="typography">
        <h3>Описание:</h3><span></span>
      </div>
      <div class="product-info__caption">
        <div class="product-info__caption-col typography">
          <p>«Винтел» сдает в аренду экскаватор-погрузчик JCB 4CX колесного типа, характеризующийся высокой производительностью в своем классе при сниженном на 16–25 % потреблении топлива. Для минимизации расходов клиентам предоставляется выбор между посуточным или почасовым тарифами.</p>
          <p>Экскаватор-погрузчик JCB 4CX имеет 4 колеса и за счет большого просвета обладает высокой скоростью и маневренностью. Повышенная проходимость обеспечивается за счет особой конструкции мостов и геометрии покрышек. Кабина экскаватора-погрузчика JCB 4CX оснащена ЖК-панелью, на ней отображается точное время работы машины.</p>
          <p>Техника подходит для выполнения земляных и строительных работ в черте города и в условиях пересеченной местности. Она используется для копания, точного профилирования грунта, погрузочно-разгрузочных работ с сыпучими и штучными грузами. Машина подходит для эксплуатации в любое время года (независимо от климата).</p>
        </div>
        <aside class="product-info__caption-col">
          <img src="/assets/images/product-info.1.jpg" alt="">
        </aside>
      </div>
    </div>
  </article>
</product-info>
<!-- /product-info -->
{% endmacro %}
```

{% sample lang='script' %}

### Script

```JS
import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|-------------------------------------------------------------------------------
| Регистрация состояния компонента
|-------------------------------------------------------------------------------
*/
store.registerModule('ProductInfo', {
  namespaced: true,

  state() {
    return {};
  },

  getters: {},

  mutations: {}
});

/*
|-------------------------------------------------------------------------------
| Объявление объекта опций компонента
|-------------------------------------------------------------------------------
*/
const ProductInfo = {
  computed: {
    ...mapState('ProductInfo', []),

    ...mapGetters('ProductInfo', [])
  },

  methods: {
    ...mapMutations('ProductInfo', [])
  }
};

Vue.component('ProductInfo', ProductInfo);

export default ProductInfo;
```

{% sample lang='style' %}

### Style

```SCSS
.product-info {
  position: relative;
  width: 100%;
}

.product-info__content-wrap {
  position: relative;
  width: 100%;
  max-width: 940px;
  margin: 0 auto;
}

.product-info__terms {
  margin-bottom: 50px;

  @media screen and (--mobile) {
    margin-bottom: 30px;
  }
}

.product-info__caption {
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
}

.product-info__caption-col {
  width: 460px;

  @media screen and (--mobile) {
    &:nth-child(2) {
      order: 1;
    }

    &:nth-child(1) {
      order: 2;
    }
  }

  & > img {
    max-width: 100%;
    margin: 0 0 20px !important;
  }
}

```

{% sample lang='test' %}

### Tests

```JS
import ProductInfo from './product-info';

describe('ProductInfo', () => {
  it('компоненту передан объект опций', () => {
    expect(typeof ProductInfo).toBe('object');
  });
});
```

{% endmethod %}

{% endblock %}