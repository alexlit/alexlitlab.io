{% extends '../_layouts/custom.htm' %}

[//]: HEADER

{% block header %}

# AboutGallery

{% endblock %}

[//]: COMPONENT

{% block component %}about-gallery{% endblock %}

[//]: API

{% block api %}

API отсутствует.

{% endblock %}

[//]: ASSETS

{% block assets %}

{% method %}

### Пример использования

```HTML
<about-gallery inline-template v-cloak>
  <section class="about-gallery">
    <div class="about-gallery__content-wrap">
      <div class="about-gallery__items-wrap">
        <img src="/assets/images/about-gallery.1.jpg" alt="" class="about-gallery__item">
        <img src="/assets/images/about-gallery.2.jpg" alt="" class="about-gallery__item">
        <img src="/assets/images/about-gallery.3.jpg" alt="" class="about-gallery__item">
        <img src="/assets/images/about-gallery.4.jpg" alt="" class="about-gallery__item">
      </div>
    </div>
  </section>
</about-gallery>
```

{% sample lang='template' %}

### Template

```TWIG
{% macro AboutGallery() %}
<!-- about-gallery -->
<about-gallery inline-template v-cloak>
  <section class="about-gallery">
    <div class="about-gallery__content-wrap">
      <div class="about-gallery__items-wrap">

        {% for item in [
          '/assets/images/about-gallery.1.jpg',
          '/assets/images/about-gallery.2.jpg',
          '/assets/images/about-gallery.3.jpg',
          '/assets/images/about-gallery.4.jpg'
        ] %}

          <img src="{{ item }}" alt="" class="about-gallery__item">

        {% endfor %}

      </div>
    </div>
  </section>
</about-gallery>
<!-- /about-gallery -->
{% endmacro %}
```

{% sample lang='script' %}

### Script

```JS
import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|-------------------------------------------------------------------------------
| Регистрация состояния компонента
|-------------------------------------------------------------------------------
*/
store.registerModule('AboutGallery', {
  namespaced: true,

  state() {
    return {};
  },

  getters: {},

  mutations: {}
});

/*
|-------------------------------------------------------------------------------
| Объявление объекта опций компонента
|-------------------------------------------------------------------------------
*/
const AboutGallery = {
  computed: {
    ...mapState('AboutGallery', []),

    ...mapGetters('AboutGallery', [])
  },

  methods: {
    ...mapMutations('AboutGallery', [])
  }
};

Vue.component('AboutGallery', AboutGallery);

export default AboutGallery;
```

{% sample lang='style' %}

### Style

```SCSS
.about-gallery {
  position: relative;
  width: 100%;
  padding: 0 10px;

  @media screen and (--mobile) {
    padding: 0;
  }
}

.about-gallery__content-wrap {
  position: relative;
  width: 100%;
  max-width: 940px;
  margin: 0 auto;
  padding-bottom: 50px;

  @media screen and (--mobile) {
    padding-bottom: 30px;
  }
}

.about-gallery__items-wrap {
  position: relative;
  display: flex;
  flex-wrap: wrap;
}

.about-gallery__item {
  display: block;
  width: 50%;
  height: 340px;
  object-fit: cover;
  object-position: center;

  @media screen and (--mobile) {
    height: 116px;
  }
}

```

{% sample lang='test' %}

### Tests

```JS
import AboutGallery from './about-gallery';

describe('AboutGallery', () => {
  it('компоненту передан объект опций', () => {
    expect(typeof AboutGallery).toBe('object');
  });
});
```

{% endmethod %}

{% endblock %}