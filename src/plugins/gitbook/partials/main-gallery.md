{% extends '../_layouts/custom.htm' %}

[//]: HEADER

{% block header %}

# MainGallery

{% endblock %}

[//]: COMPONENT

{% block component %}main-gallery{% endblock %}

[//]: API

{% block api %}

API отсутствует.

{% endblock %}

[//]: ASSETS

{% block assets %}

{% method %}

### Пример использования

```HTML
<main-gallery inline-template v-cloak>
  <section class="main-gallery">
    <div class="main-gallery__content-wrap">
      <img src="/assets/images/main-gallery.1.jpg" alt="" class="main-gallery__item">
      <img src="/assets/images/main-gallery.2.jpg" alt="" class="main-gallery__item">
      <img src="/assets/images/main-gallery.3.jpg" alt="" class="main-gallery__item">
      <img src="/assets/images/main-gallery.4.jpg" alt="" class="main-gallery__item">
      <img src="/assets/images/main-gallery.5.jpg" alt="" class="main-gallery__item">
    </div>
  </section>
</main-gallery>
```

{% sample lang='template' %}

### Template

```TWIG
{% macro MainGallery() %}
<!-- main-gallery -->
<main-gallery inline-template v-cloak>
  <section class="main-gallery">
    <div class="main-gallery__content-wrap">

      {% for item in [
        '/assets/images/main-gallery.1.jpg',
        '/assets/images/main-gallery.2.jpg',
        '/assets/images/main-gallery.3.jpg',
        '/assets/images/main-gallery.4.jpg',
        '/assets/images/main-gallery.5.jpg'
      ] %}

        <img src="{{ item }}" alt="" class="main-gallery__item">

      {% endfor %}

    </div>
  </section>
</main-gallery>
<!-- /main-gallery -->
{% endmacro %}
```

{% sample lang='script' %}

### Script

```JS
import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|-------------------------------------------------------------------------------
| Регистрация состояния компонента
|-------------------------------------------------------------------------------
*/
store.registerModule('MainGallery', {
  namespaced: true,

  state() {
    return {};
  },

  getters: {},

  mutations: {}
});

/*
|-------------------------------------------------------------------------------
| Объявление объекта опций компонента
|-------------------------------------------------------------------------------
*/
const MainGallery = {
  computed: {
    ...mapState('MainGallery', []),

    ...mapGetters('MainGallery', [])
  },

  methods: {
    ...mapMutations('MainGallery', [])
  }
};

Vue.component('MainGallery', MainGallery);

export default MainGallery;
```

{% sample lang='style' %}

### Style

```SCSS
.main-gallery {
  position: relative;
  width: 100%;
  background-color: #fff;
}

.main-gallery__content-wrap {
  position: relative;
  display: flex;
  overflow: auto;
  width: 100%;
  margin: 0 auto;
  padding-bottom: 70px;
  -webkit-overflow-scrolling: touch;

  @media screen and (--mobile) {
    padding-top: 20px;
    padding-bottom: 0;
  }
}

.main-gallery__item {
  display: block;
  width: 204px;
  height: 204px;
  object-position: center;
  object-fit: cover;
}

```

{% sample lang='test' %}

### Tests

```JS
import MainGallery from './main-gallery';

describe('MainGallery', () => {
  it('компоненту передан объект опций', () => {
    expect(typeof MainGallery).toBe('object');
  });
});
```

{% endmethod %}

{% endblock %}