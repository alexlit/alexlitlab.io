{% extends '../\_layouts/custom.htm' %}

[//]: HEADER

{% block header %}

# FormFastOrder

{% endblock %}

[//]: COMPONENT

{% block component %}form-fast-order{% endblock %}

[//]: API

{% block api %}

API отсутствует.

{% endblock %}

[//]: ASSETS

{% block assets %}

{% method %}

### Пример использования

```HTML
<form-fast-order action="/assets/forms/form-fast-order.php" inline-template v-cloak>
  <form class="form-fast-order" @submit.prevent="validateBeforeSubmit(scope);" :data-vv-scope="scope" :action="action" novalidate>
    <div class="form-fast-order__content-wrap">
      <div class="form-fast-order__content">
        <p class="form-fast-order__suptitle">Быстрый заказ</p>
        <p class="form-fast-order__title">[[ formData.TITLE ]]</p>
        <div class="form-fast-order__conteiner">
          <transition enter-active-class="animated fadeIn" leave-active-class="animated fadeOut" duration="300" mode="out-in">
            <div class="form-fast-order__image-wrap" v-if="!success && $mq === 'mobile' || $mq !== 'mobile'">
              <img class="form-fast-order__image" :src="formData.IMAGE" :alt="formData.TITLE">
            </div>
          </transition>
          <transition enter-active-class="animated fadeIn" leave-active-class="animated fadeOut" duration="300" mode="out-in">
            <div class="form-fast-order__text" v-if="!success">
              <p class="form-fast-order__paragraph">Оставьте ваш номер телефона и мы перезвоним в кратчайшие сроки:</p>
              <div class="form-fast-order__field-wrap">
                <label for="" class="form-fast-order__label">Телефон:</label>
                <input :class="{'form-fast-order__input': true,'shake animated': shake && errors.has(scope + '.PHONE')}" v-model="formData.PHONE" data-inputmask="'mask': '+7 (999) 999-99-99'" placeholder="+7 (___) ___-__-__" name="PHONE" type="tel" @focus="inputmask();" v-validate.disable="{rules: {required: true,regex: /^\+7\s?\([0-9]{3}\)\s?[0-9]{3}-?[0-9]{2}-?[0-9]{2}$/}}" :data-invalid="errors.has(scope + '.PHONE')">
                <transition enter-active-class="animated fadeIn" leave-active-class="animated fadeOut" mode="out-in">
                  <p class="form-fast-order__policy-error" v-if="!agree && tries > 0">Вы не согласились с условиями обработки данных</p>
                </transition>
                <el-checkbox class="form-fast-order__el-checkbox" v-model="agree" v-validate.disable="'required'">Я согласен на обработку персональных данных и с условиями <a href="#" target="_blank">политики конфиденциальности</a></el-checkbox>
              </div>
              <button class="form-fast-order__button" type="send" @submit.prevent="validateBeforeSubmit(scope);">ОТПРАВИТЬ</button>
            </div>
            <figure class="form-fast-order__success" v-else>
              <message-success inline-template v-cloak>
                <section class="message-success">
                  <div class="message-success__content-wrap">
                    <header class="message-success__title">Ваш заказ оформлен!</header>
                    <p class="message-success__caption">Мы свяжемся с вами в самое ближайшее время для уточнения деталей заказа.</p>
                  </div>
                </section>
              </message-success>
            </figure>
          </transition>
        </div>
      </div>
    </div>
  </form>
</form-fast-order>
```

{% sample lang='template' %}

### Template

```TWIG
{% from 'components/message-success/message-success.htm' import MessageSuccess %}

{% macro FormFastOrder() %}
<!-- form-fast-order -->
<form-fast-order
  action="/assets/forms/form-fast-order.php"
  inline-template
  v-cloak
>
  <form
    class="form-fast-order"
    @submit.prevent="validateBeforeSubmit(scope);"
    :data-vv-scope="scope"
    :action="action"
    novalidate
  >
    <div class="form-fast-order__content-wrap">

      {# content #}
      <div class="form-fast-order__content">
        <p class="form-fast-order__suptitle">Быстрый заказ</p>
        <p class="form-fast-order__title">[[ formData.TITLE ]]</p>
        <div class="form-fast-order__conteiner">
          <transition
            enter-active-class="animated fadeIn"
            leave-active-class="animated fadeOut"
            duration="300"
            mode="out-in"
          >
            <div
              class="form-fast-order__image-wrap"
              v-if="!success && $mq === 'mobile' || $mq !== 'mobile'"
            >
              <img
                class="form-fast-order__image"
                :src="formData.IMAGE"
                :alt="formData.TITLE"
              >
            </div>
          </transition>
          <transition
            enter-active-class="animated fadeIn"
            leave-active-class="animated fadeOut"
            duration="300"
            mode="out-in"
          >

            {# text #}
            <div class="form-fast-order__text" v-if="!success">
              <p class="form-fast-order__paragraph">Оставьте ваш номер телефона и мы перезвоним в кратчайшие сроки:</p>
              <div class="form-fast-order__field-wrap">
                <label for="" class="form-fast-order__label">Телефон:</label>
                <input
                  :class="{
                    'form-fast-order__input': true,
                    'shake animated': shake && errors.has(scope + '.PHONE')
                  }"
                  v-model="formData.PHONE"
                  data-inputmask="'mask': '+7 (999) 999-99-99'"
                  placeholder="+7 (___) ___-__-__"
                  name="PHONE"
                  type="tel"
                  @focus="inputmask();"
                  v-validate.disable="{
                    rules: {
                      required: true,
                      regex: /^\+7\s?\([0-9]{3}\)\s?[0-9]{3}-?[0-9]{2}-?[0-9]{2}$/
                    }
                  }"
                  :data-invalid="errors.has(scope + '.PHONE')"
                >
                <transition
                  enter-active-class="animated fadeIn"
                  leave-active-class="animated fadeOut"
                  mode="out-in"
                >
                  <p
                    class="form-fast-order__policy-error"
                    v-if="!agree && tries > 0"
                  >Вы не согласились с условиями обработки данных</p>
                </transition>
                <el-checkbox
                  class="form-fast-order__el-checkbox"
                  v-model="agree"
                  v-validate.disable="'required'"
                >Я согласен на обработку персональных данных и с условиями <a href="#" target="_blank">политики конфиденциальности</a></el-checkbox>
              </div>
              <button
                class="form-fast-order__button"
                type="send"
                @submit.prevent="validateBeforeSubmit(scope);"
              >ОТПРАВИТЬ</button>
            </div>
            {# /text #}

            {# success #}
            <figure class="form-fast-order__success" v-else>

              {{ MessageSuccess() }}

            </figure>
            {# /success #}

          </transition>
        </div>
      </div>
      {# /content #}
    </div>
  </form>
</form-fast-order>

<!-- /form-fast-order -->
{% endmacro %}
```

{% sample lang='script' %}

### Script

```JS
import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import 'animate.css/animate.css';
import axios from 'axios';
import qs from 'qs';
import Inputmask from 'inputmask/dist/inputmask/inputmask';
import VeeValidate from 'vee-validate';
import store from '../app/app.store';
import './form-fast-order.php';

Vue.use(VeeValidate, {
  inject: false
});

Vue.config.productionTip = false;

/*
|-------------------------------------------------------------------------------
| Регистрация состояния компонента
|-------------------------------------------------------------------------------
*/
store.registerModule('FormFastOrder', {
  namespaced: true,

  state() {
    return {
      useUrlEncode: true,
      success: false,
      shake: false,
      tries: 0,
      formData: {}
    };
  },

  getters: {},

  mutations: {
    /**
     * Установка начальных значений формы
     * @param {*} state
     * @param {Object} payload - требуемые значения
     */
    setFormData(state, payload) {
      state.formData = { ...state.formData, ...payload };
    },

    /**
     * Счетчик попыток отправить форму
     * @param {*} state
     */
    incrementTries(state) {
      state.tries += 1;
      state.shake = true;
      setTimeout(() => {
        state.shake = false;
      }, 1000);
    },

    /**
     * Установка состояния "успешная отправка"
     * @param {*} state
     * @param {Boolean} payload
     */
    setSuccessState(state, payload) {
      state.success = payload;
    }
  }
});

/*
|-------------------------------------------------------------------------------
| Объявление объекта опций компонента
|-------------------------------------------------------------------------------
*/
const FormFastOrder = {
  data() {
    return {
      agree: false,
      scope: Math.random()
        .toString(36)
        .substring(2)
    };
  },

  props: {
    action: {
      type: String,
      default: '/assets/forms/form-fast-order.php'
    }
  },

  computed: {
    ...mapState('FormFastOrder', [
      'useUrlEncode',
      'success',
      'shake',
      'tries',
      'formData'
    ]),

    ...mapGetters('FormFastOrder', [])
  },

  inject: {
    $validator: '$validator'
  },

  methods: {
    ...mapMutations('FormFastOrder', ['incrementTries', 'setSuccessState']),

    /**
     * Валидация формы и отправка данных
     */
    validateBeforeSubmit(scope) {
      const vm = this;

      vm.$validator
        .validateAll(scope)
        .then(response => {
          if (response && vm.agree) {
            const post = vm.useUrlEncode
              ? qs.stringify(vm.formData)
              : vm.formData;

            axios
              .post(vm.action, post)
              .then(serverResponse => {
                vm.setSuccessState(true);
                console.info(`Отправлены данные:`);
                console.info(post);
                console.info(`Ответ сервера:`);
                console.info(serverResponse);
              })
              .catch(error => {
                console.error(`Ошибка сервера:`);
                console.info(error);
              });
          } else {
            vm.incrementTries();
          }
        })
        .catch(error => {
          console.log(error);
        });
    },

    /**
     * Подключение маски
     */
    inputmask() {
      const vm = this;
      Inputmask({ showMaskOnHover: false }).mask(
        vm.$el.querySelectorAll('[data-inputmask]')
      );
    }
  }
};

Vue.component('FormFastOrder', FormFastOrder);

export default FormFastOrder;

```

{% sample lang='style' %}

### Style

```SCSS
.form-fast-order {
  position: relative;
  width: 100%;
}

.form-fast-order__content-wrap {
  position: relative;
  width: 100%;
  max-width: 940px;
  margin: 0 auto;
}

.form-fast-order__content {
  position: relative;
  width: 100%;
}

.form-fast-order__suptitle {
  margin-bottom: 10px;
  letter-spacing: 1px;
  text-transform: uppercase;
  color: #000;
  font-size: 14px;
  font-weight: 700;
  line-height: 28px;
}

.form-fast-order__title {
  margin-bottom: 30px;
  color: #000;
  font-size: 36px;
  font-weight: 700;
  line-height: 42px;

  @media screen and (--mobile) {
    margin-bottom: 20px;
  }
}

.form-fast-order__conteiner {
  display: flex;
  justify-content: space-between;
  width: 100%;

  @media screen and (--mobile) {
    display: block;
  }
}

.form-fast-order__image-wrap {
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 320px;
  height: 320px;
  border: 1px solid #e8e8e8;
  background-color: #fff;

  @media screen and (--mobile) {
    width: 100%;
    height: 300px;
    margin-bottom: 20px;
  }
}

.form-fast-order__image {
  display: block;
  max-width: 100%;
  max-height: 100%;
}

.form-fast-order__text {
  width: calc(100% - 340px);

  @media screen and (--mobile) {
    width: 100%;
  }
}

.form-fast-order__paragraph {
  margin-bottom: 30px;
  color: #000;
  font-size: 18px;
  font-weight: 400;
  line-height: 22px;

  @media screen and (--mobile) {
    margin-bottom: 20px;
  }
}

.form-fast-order__field-wrap {
  position: relative;
  width: 100%;
  margin-bottom: 20px;
}

.form-fast-order__label {
  display: block;
  margin-bottom: 10px;
  letter-spacing: 1px;
  text-transform: uppercase;
  color: #000;
  font-size: 12px;
  font-weight: 500;
  line-height: 18px;
}

.form-fast-order__input {
  width: 100%;
  height: 80px;
  margin-bottom: 20px;
  padding: 0 20px;
  color: #000;
  border-bottom: 3px solid #fc0;
  background-color: #f5f5f5;
  font-size: 30px;
  font-weight: 300;
}

.form-fast-order__policy-error {
  margin-bottom: 20px;
  animation-duration: .4s;
  color: #F44336;
  font-size: 14px;
}

.form-fast-order__el-checkbox {
}

.form-fast-order__button {
  padding: 18px 21px;
  text-transform: uppercase;
  color: #000;
  border-radius: 2px;
  background-color: #fc0;
  font-size: 20px;
  font-weight: 700;

  &:hover {
    background-color: #ffe500;
  }
}

.form-fast-order__success {
  position: relative;
  width: calc(100% - 340px);

  @media screen and (--mobile) {
    width: 100%;
  }
}

```

{% sample lang='test' %}

### Tests

```JS
import FormFastOrder from './form-fast-order';

describe('FormFastOrder', () => {
  it('компоненту передан объект опций', () => {
    expect(typeof FormFastOrder).toBe('object');
  });
});
```

{% sample lang='serverside' %}

### ServerSide

```php
<?php

require __DIR__ . '/../../plugins/PHPMailer/src/PHPMailer.php';
require __DIR__ . '/../../plugins/PHPMailer/src/Exception.php';

use PHPMailer\PHPMailer\PHPMailer;

// Получаем данные в зависимости от типа
$json = json_decode(file_get_contents('php://input'), TRUE);
$post = $json ? $json : $_POST;

// Инициализируем объект
$mail = new PHPMailer();
$mail->CharSet = 'utf-8';
$mail->setFrom('noreply@vintel.ru','Винтел | Спецтехника');
$mail->addAddress('ljazzmail@gmail.com');
$mail->addAttachment(__DIR__.'/../../'.$post['IMAGE']);
$mail->isHTML(TRUE);
$mail->Subject = 'Быстрый заказ: '.$post['TITLE'];
$mail->Body = '
<html>
  <head>
    <title>'.$mail->Subject.'</title>
  </head>
  <body>
    <p>Телефон:'.$post['PHONE'].'</p>
  </body>
</html>';
$mail->send();

// Запись данных в файл для дебага
$fileData = fopen(basename(__FILE__, '.php') . '.response.md', 'w');

fputs($fileData, print_r("# MESSAGE:\r\n\r\n", TRUE));
fputs($fileData, print_r("```html\r\n", TRUE));
fputs($fileData, print_r($mail->Body, TRUE));
fputs($fileData, print_r("\r\n```\r\n\r\n", TRUE));

fputs($fileData, print_r("## POST:\r\n\r\n", TRUE));
fputs($fileData, print_r("```php\r\n", TRUE));
fputs($fileData, print_r($post, TRUE));
fputs($fileData, print_r("```\r\n\r\n", TRUE));

fputs($fileData, print_r("## FILES:\r\n\r\n", TRUE));
fputs($fileData, print_r("```php\r\n", TRUE));
fputs($fileData, print_r($_FILES, TRUE));
fputs($fileData, print_r("```\r\n\r\n", TRUE));

fputs($fileData, print_r("### REQUEST:\r\n\r\n", TRUE));
fputs($fileData, print_r("```php\r\n", TRUE));
fputs($fileData, print_r($_REQUEST, TRUE));
fputs($fileData, print_r("```\r\n\r\n", TRUE));

fclose($fileData);

// Возвращаем ответ
var_dump($post);

?>

```

{% endmethod %}

{% endblock %}
