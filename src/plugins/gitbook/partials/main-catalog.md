{% extends '../_layouts/custom.htm' %}

[//]: HEADER

{% block header %}

# MainCatalog

{% endblock %}

[//]: COMPONENT

{% block component %}main-catalog{% endblock %}

[//]: API

{% block api %}

API отсутствует.

{% endblock %}

[//]: ASSETS

{% block assets %}

{% method %}

### Пример использования

```HTML
<main-catalog inline-template v-cloak>
  <section class="main-catalog">
    <div class="main-catalog__content-wrap">
      <div class="main-catalog__items-wrap">
        <a href="#" class="main-catalog__item">
          <img class="main-catalog__item-image" src="/assets/images/main-catalog.1.jpg" alt="Автокраны">
          <span class="main-catalog__item-label">Автокраны</span>
        </a>
        <a href="#" class="main-catalog__item">
          <img class="main-catalog__item-image" src="/assets/images/main-catalog.2.jpg" alt="Самосвалы">
          <span class="main-catalog__item-label">Самосвалы</span>
        </a>
        <a href="#" class="main-catalog__item">
          <img class="main-catalog__item-image" src="/assets/images/main-catalog.3.jpg" alt="Экскаваторы">
          <span class="main-catalog__item-label">Экскаваторы</span>
        </a>
        <a href="#" class="main-catalog__item">
          <img class="main-catalog__item-image" src="/assets/images/main-catalog.4.jpg" alt="Катки">
          <span class="main-catalog__item-label">Катки</span>
        </a>
        <a href="#" class="main-catalog__item">
          <img class="main-catalog__item-image" src="/assets/images/main-catalog.5.jpg" alt="Манипуляторы">
          <span class="main-catalog__item-label">Манипуляторы</span>
        </a>
        <a href="#" class="main-catalog__item">
          <img class="main-catalog__item-image" src="/assets/images/main-catalog.6.jpg" alt="Бульдозеры">
          <span class="main-catalog__item-label">Бульдозеры</span>
        </a>
        <a href="#" class="main-catalog__item">
          <img class="main-catalog__item-image" src="/assets/images/main-catalog.7.jpg" alt="Тралы">
          <span class="main-catalog__item-label">Тралы</span>
        </a>
        <a href="#" class="main-catalog__item">
          <img class="main-catalog__item-image" src="/assets/images/main-catalog.8.jpg" alt="Погрузчики">
          <span class="main-catalog__item-label">Погрузчики</span>
        </a>
        <a href="#" class="main-catalog__item">
          <img class="main-catalog__item-image" src="/assets/images/main-catalog.9.jpg" alt="Бортовые полуприцепы">
          <span class="main-catalog__item-label">Бортовые полуприцепы</span>
        </a>
      </div>
    </div>
  </section>
</main-catalog>
```

{% sample lang='template' %}

### Template

```TWIG
{% macro MainCatalog(
  mod = null,
  items = [
    {
      image: '/assets/images/main-catalog.1.jpg',
      label: 'Автокраны'
    },
    {
      image: '/assets/images/main-catalog.2.jpg',
      label: 'Самосвалы'
    },
    {
      image: '/assets/images/main-catalog.3.jpg',
      label: 'Экскаваторы'
    },
    {
      image: '/assets/images/main-catalog.4.jpg',
      label: 'Катки'
    },
    {
      image: '/assets/images/main-catalog.5.jpg',
      label: 'Манипуляторы'
    },
    {
      image: '/assets/images/main-catalog.6.jpg',
      label: 'Бульдозеры'
    },
    {
      image: '/assets/images/main-catalog.7.jpg',
      label: 'Тралы'
    },
    {
      image: '/assets/images/main-catalog.8.jpg',
      label: 'Погрузчики'
    },
    {
      image: '/assets/images/main-catalog.9.jpg',
      label: 'Бортовые полуприцепы'
    }
  ]
) %}
<!-- main-catalog -->
<main-catalog inline-template v-cloak>
  <section class="main-catalog{% if mod %} main-catalog--{{ mod }}{% endif %}">
    <div class="main-catalog__content-wrap">
      <div class="main-catalog__items-wrap">

        {% for item in items %}

          <a
            href="#"
            class="main-catalog__item"
          >
            <img
              class="main-catalog__item-image"
              src="{{ item.image }}"
              alt="{{ item.label }}"
            >
            <span class="main-catalog__item-label">{{ item.label }}</span>
          </a>

        {% endfor %}

      </div>
    </div>
  </section>
</main-catalog>
<!-- /main-catalog -->
{% endmacro %}
```

{% sample lang='script' %}

### Script

```JS
import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|-------------------------------------------------------------------------------
| Регистрация состояния компонента
|-------------------------------------------------------------------------------
*/
store.registerModule('MainCatalog', {
  namespaced: true,

  state() {
    return {};
  },

  getters: {},

  mutations: {}
});

/*
|-------------------------------------------------------------------------------
| Объявление объекта опций компонента
|-------------------------------------------------------------------------------
*/
const MainCatalog = {
  computed: {
    ...mapState('MainCatalog', []),

    ...mapGetters('MainCatalog', [])
  },

  methods: {
    ...mapMutations('MainCatalog', [])
  }
};

Vue.component('MainCatalog', MainCatalog);

export default MainCatalog;
```

{% sample lang='style' %}

### Style

```SCSS
.main-catalog {
  position: relative;
  width: 100%;
  padding: 0 10px;

  &--padded {
    padding-bottom: 40px;
  }

  &--subcategory {
  }
}

.main-catalog__content-wrap {
  position: relative;
  width: 100%;
  max-width: 940px;
  margin: 0 auto;
}

.main-catalog__items-wrap {
  position: relative;
  display: flex;
  flex-wrap: wrap;
  width: calc(100% + 20px);
  margin: 0 -10px;

  @media screen and (--mobile) {
    display: block;
    width: 100%;
    margin: 0;
  }
}

.main-catalog__item {
  position: relative;
  display: block;
  width: 300px;
  height: 200px;
  margin: 0 10px 20px;
  background-color: #f7f8fa;
  box-shadow: 10px 10px 0 0 #f0f1f2;

  @media screen and (--mobile) {
    width: 100%;
    height: 100px;
    margin: 0 0 20px;
  }

  @nest .main-catalog--subcategory & {
    @media screen and (--mobile) {
      height: 200px;
    }
  }
}

.main-catalog__item-image {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  object-fit: contain;
  object-position: center;
}

.main-catalog__item-label {
  position: absolute;
  z-index: 1;
  top: 0;
  left: 0;
  padding: 10px;
  transition-duration: .4s;
  color: #fff;
  background-color: #000;
  font-size: 18px;
  font-weight: 500;
  line-height: 24px;

  @nest .main-catalog__item:hover & {
    color: #000;
    background-color: #ffd101;
  }
}

```

{% sample lang='test' %}

### Tests

```JS
import MainCatalog from './main-catalog';

describe('MainCatalog', () => {
  it('компоненту передан объект опций', () => {
    expect(typeof MainCatalog).toBe('object');
  });
});
```

{% endmethod %}

{% endblock %}