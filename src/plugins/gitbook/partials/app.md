{% extends '../\_layouts/custom.htm' %}

[//]: HEADER

{% block header %}

# App

Глобальный экземпляр приложения.

{% endblock %}

[//]: COMPONENT

{% block component %}app{% endblock %}

[//]: API

{% block api %}

API отсутствует.

{% endblock %}

[//]: ASSETS

{% block assets %}

{% method %}

### Пример использования

```HTML
<div id="app" class="app" :data-browser="device.browser" :data-browser-version="device.browserVersion" :data-browser-engine="device.browserEngine" :data-device-type="device.type" :data-os="device.os" :data-os-version="device.osVersion" :data-no-scroll="noScroll"></div>
```

{% sample lang='script' %}

### Script

```JS
/* eslint  no-unused-vars: 0 */

import 'element-ui/lib/theme-chalk/index.css';
import 'normalize.css/normalize.css';
import 'vue-clicky';
import ElementUI from 'element-ui';
import Inview from 'vueinview';
import PortalVue from 'portal-vue';
import Vue from 'vue/dist/vue';
import VueMq from 'vue-mq';
import VueScrollTo from 'vue-scrollto';
import bowser from 'bowser';
import locale from 'element-ui/lib/locale/lang/ru-RU';
import objectFitImages from 'object-fit-images';
import store from './app.store';

Vue.use(ElementUI, { locale });
Vue.use(PortalVue);
Vue.use(Inview);
Vue.use(VueScrollTo, {
  container: 'body',
  duration: 1000,
  easing: 'easeInOutCubic',
  offset: 0,
  cancelable: true,
  onDone: false,
  onCancel: false,
  x: false,
  y: true
});
Vue.use(VueMq, {
  breakpoints: {
    mobile: 480,
    laptop: Infinity
  }
});

Vue.config.productionTip = false;
Vue.config.performance = process.env.NODE_ENV === 'dev';
Vue.config.devtools = process.env.NODE_ENV === 'dev';
Vue.config.ignoredElements = ['vue-ignore', 'noindex', 'code', 'pre'];

/**
 * Разделитель тысяч в ценах
 *
 * @param {number|string} value - Значение
 * @returns строка с разделением на тысячные доли | пример: '7 550 000'
 */
Vue.filter('thousandSeparator', value =>
  value
    .toString()
    .replace(/\D+/g, '')
    .replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ')
);

// const cq = require('cq-prolyfill')({ preprocess: true });

const App = new Vue({
  el: '#app',

  name: 'App',

  delimiters: ['[[', ']]'],

  store,

  data: {
    device: {},

    options: {
      serviceWorkers: {
        enable: process.env.NODE_ENV !== 'dev'
      },

      disableUserSelect: {
        enable: false
      }
    },

    plugins: {
      bowser: {
        enable: true
      },

      objectFitImages: {
        enable: true,
        options: {
          watchMQ: true
        }
      }
    },

    noScroll: false
  },

  methods: {
    /**
     * Регистрация сервис-воркера
     */
    registerServiceWorkers() {
      if ('serviceWorker' in navigator) {
        navigator.serviceWorker.register('service-worker.js');
      }
    },

    /**
     * Запрет пользователю копировать содержимое
     */
    disableUserSelect() {
      document.ondragstart = false;
      document.onselectstart = false;
      document.oncontextmenu = false;
    },

    /**
     * Получение сведений о браузере и системе пользователя
     */
    bowser() {
      function detectBrowserEngine() {
        let browser;
        if (bowser.blink) {
          browser = 'blink';
        } else if (bowser.webkit) {
          browser = 'webkit';
        } else if (bowser.gecko) {
          browser = 'gecko';
        } else if (bowser.msie) {
          browser = 'msie';
        } else if (bowser.msedge) {
          browser = 'msedge';
        }
        return browser;
      }

      function detectDeviceType() {
        let deviceType;
        if (bowser.mobile) {
          deviceType = 'mobile';
        } else if (bowser.tablet) {
          deviceType = 'tablet';
        } else {
          deviceType = 'laptop';
        }
        return deviceType;
      }

      function detectOs() {
        let os;
        if (bowser.mac) {
          os = 'mac';
        } else if (bowser.windows) {
          os = 'windows';
        } else if (bowser.windowsphone) {
          os = 'windowsphone';
        } else if (bowser.linux) {
          os = 'linux';
        } else if (bowser.chromeos) {
          os = 'chromeos';
        } else if (bowser.android) {
          os = 'android';
        } else if (bowser.ios) {
          os = 'ios';
        } else if (bowser.blackberry) {
          os = 'blackberry';
        } else if (bowser.firefoxos) {
          os = 'firefoxos';
        } else if (bowser.webos) {
          os = 'webos';
        } else if (bowser.bada) {
          os = 'bada';
        } else if (bowser.tizen) {
          os = 'tizen';
        } else if (bowser.sailfish) {
          os = 'sailfish';
        }
        return os;
      }

      this.device.browser = bowser.name;
      this.device.browserVersion = bowser.version;
      this.device.browserEngine = detectBrowserEngine();
      this.device.type = detectDeviceType();
      this.device.os = detectOs();
      this.device.osVersion = bowser.osversion;
    },

    /**
     * Инициализация полифила для object-fit
     */
    objectFitImages() {
      objectFitImages(null, this.plugins.objectFitImages.options);
    }
  },

  beforeMount() {
    if (this.plugins.bowser.enable) this.bowser();
  },

  mounted() {
    if (this.options.serviceWorkers.enable) this.registerServiceWorkers();
    if (this.options.disableUserSelect.enable) this.disableUserSelect();
    if (this.plugins.bowser.enable) this.bowser();
    if (this.plugins.objectFitImages.enable) this.objectFitImages();
  }
});

export default App;

```

{% sample lang='style' %}

### Style

```SCSS
/*
|--------------------------------------------------------------------------
| FONTS
|--------------------------------------------------------------------------
| Подключение шрифтов
*/

/*
|--------------------------------------------------------------------------
| ROOT
|--------------------------------------------------------------------------
| Объявление корневых свойств
*/

:root {
  --app__box-shadow: 0 2px 16px rgba(0, 0, 0, .1);
  --app__color--blue: #409eff;
  --app__color--invalid: #ff3b3b;
  --app__color--placeholder: #858585;
  --app__color--primary: #215ed4;
  --app__color--success: #67c23a;
  --app__color--text--light: #f2f6fc;
  --app__color--text: #000;
  --app__font-family: Roboto, sans-serif;

  color: #212121;
  font-family: var(--app__font-family);
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;

  @custom-media --tablet (width <= 960px);
  @custom-media --mobile (width <= 480px);
}

/*
|--------------------------------------------------------------------------
| NATIVE ELEMENTS
|--------------------------------------------------------------------------
| Кастомизация нативных элементов браузера
*/

* {
  box-sizing: border-box;
  margin: 0;
  padding: 0;

  @supports (outline: none) {
    outline: none;
    outline-color: transparent;
  }
}

::before,
::after {
  box-sizing: border-box;
  transition: inherit;
}

::selection {
  color: #fff;
  background-color: #FFC107;
}

::placeholder {
  transition-duration: .4s;
  color: #9E9E9E;

  @nest :focus& {
    transform: translateX(1rem);
    opacity: 0;
    color: transparent;
  }
}

h1,
h2,
h3,
h4,
h5,
h6,
div,
p,
address,
article,
aside,
details,
figcaption,
figure,
footer,
header,
hgroup,
main,
menu,
nav,
section,
summary {
  display: block;
  margin: 0;
  padding: 0;
  font-style: normal;
}

body {
  width: 100%;
  padding: 0;
}

a {
  cursor: pointer !important;
  transition-duration: .4s;
  text-decoration: none;
  font-family: inherit;
}

button {
  margin: 0;
  padding: 0;
  cursor: pointer !important;
  user-select: none;
  transition-duration: .4s;
  border: none;
  border-radius: 0;
  background-color: transparent;
  font-family: inherit;

  &[disabled] {
    pointer-events: none;
    filter: grayscale(1);
  }
}

input {
  overflow-x: hidden;
  width: 100%;
  height: 60px;
  padding: 0;
  transition-duration: .4s;
  border-top: none;
  border-right: none;
  border-bottom: none;
  border-left: none;
  background-color: transparent;
  font-family: var(--app__font-family);

  &:focus {
    border-color: #FFC107;
  }

  &[data-invalid] {
    border-color: #F44336;
  }

  &[type='number'] {
    -moz-appearance: textfield;
    appearance: none;

    &::-webkit-inner-spin-button {
      margin: 0;
      -webkit-appearance: none;
    }

    &::-webkit-outer-spin-button {
      margin: 0;
      -webkit-appearance: none;
    }
  }
}

textarea {
  overflow-x: hidden;
  width: 100%;
  padding: 0;
  resize: vertical;
  transition-duration: .4s;
  border-top: none;
  border-right: none;
  border-bottom: none;
  border-left: none;
  background-color: transparent;
  font-family: var(--app__font-family);

  &:focus {
    border-color: #FFC107;
  }

  &[data-invalid] {
    border-color: #F44336;
  }
}

fieldset {
  margin: 0;
  padding: 0;
  border: none;
}

/*
|--------------------------------------------------------------------------
| APP
|--------------------------------------------------------------------------
| Приложение
*/

.app {
  width: 100%;
  min-width: 1020px;
  min-height: 100vh;
  color: #212121;
  background-color: #083ea7;
  background-image: url('../images/app.bgd.jpg');
  background-repeat: no-repeat;
  background-position: center top;
  background-size: 100% auto;

  @media screen and (--mobile) {
    min-width: 320px;
  }

  & [v-cloak] {
    display: none;
  }

  &[data-no-scroll] {
    overflow: hidden;
    height: 100vh;

    &[data-device-type='laptop'] {
      padding-right: 15px;
    }
  }
}

.app__container {
  position: relative;
  width: 100%;
  max-width: 1020px;
  margin: 0 auto;
  background-color: #fff;
  box-shadow: 30px 30px 0 0 #215ed4;
}

```

{% sample lang='store' %}

### Store

```JS
import Vue from 'vue/dist/vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    App: {
      namespaced: true,

      state() {
        return {
          modal: {
            isShow: false,
            content: ''
          }
        };
      },

      mutations: {
        /**
         * Показ модального окна
         * @param {*} state
         * @param {String} payload - имя модалки в dash-case
         */
        showModal(state, payload) {
          state.modal.isShow = true;
          state.modal.content = payload;
        }
      }
    }
  }
});

```

{% sample lang='test' %}

### Tests

```JS
import App from './app';

describe('App', () => {
  it('компоненту передан объект опций', () => {
    expect(typeof App).toBe('object');
  });
});

describe('filters: thousandSeparator', () => {
  it('1000 -> 1 000', () => {
    expect(App.$options.filters.thousandSeparator(1000)).toBe('1 000');
  });

  it("'123456789' -> 123 456 789", () => {
    expect(App.$options.filters.thousandSeparator('123456789')).toBe(
      '123 456 789'
    );
  });

  it('350 -> 350', () => {
    expect(App.$options.filters.thousandSeparator(350)).toBe('350');
  });

  it('Hello -> Hello', () => {
    expect(App.$options.filters.thousandSeparator('Hello')).toBe('');
  });

  it('Try66666ololo1 -> 666 661', () => {
    expect(App.$options.filters.thousandSeparator('Try66666ololo1')).toBe(
      '666 661'
    );
  });
});

```

{% endmethod %}

{% endblock %}
