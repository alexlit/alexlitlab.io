{% extends '../_layouts/custom.htm' %}

[//]: HEADER

{% block header %}

# MainPromo

{% endblock %}

[//]: COMPONENT

{% block component %}main-promo{% endblock %}

[//]: API

{% block api %}

API отсутствует.

{% endblock %}

[//]: ASSETS

{% block assets %}

{% method %}

### Пример использования

```HTML
<main-promo inline-template v-cloak>
  <section class="main-promo">
    <div class="main-promo__content-wrap swiper-conteiner" data-swiper>
      <div class="swiper-wrapper">
        <figure class="main-promo__slide swiper-slide" style="background-image:url(/assets/images/main-promo.1.jpg)">
          <figcaption class="main-promo__slide-caption">
            <p class="main-promo__slide-title">Аренда
              <br>спецтехники</p>
            <p class="main-promo__slide-subtitle">для строительства и перевозок по Москве и Московской области</p><a href="#" class="main-promo__slide-readmore">Каталог спецтехники</a></figcaption>
        </figure>
        <figure class="main-promo__slide swiper-slide" style="background-image:url(/assets/images/main-promo.2.jpg)">
          <figcaption class="main-promo__slide-caption">
            <p class="main-promo__slide-title">Аренда
              <br>спецтехники</p>
            <p class="main-promo__slide-subtitle">для строительства и перевозок по Москве и Московской области</p><a href="#" class="main-promo__slide-readmore">Каталог спецтехники</a></figcaption>
        </figure>
      </div>
      <nav class="main-promo__pagination swiper-pagination" data-swiper-pagination></nav>
    </div>
  </section>
</main-promo>
```

{% sample lang='template' %}

### Template

```TWIG
{% macro MainPromo() %}
<!-- main-promo -->
<main-promo inline-template v-cloak>
  <section class="main-promo">
    <div class="main-promo__content-wrap swiper-conteiner" data-swiper>
      <div class="swiper-wrapper">

        {% for item in [
          {
            image: '/assets/images/main-promo.1.jpg',
            title: 'Аренда<br>спецтехники',
            subtitle: 'для строительства и перевозок по Москве и Московской области',
            readmoreText: 'Каталог спецтехники'
          }, {
            image: '/assets/images/main-promo.2.jpg',
            title: 'Аренда<br>спецтехники',
            subtitle: 'для строительства и перевозок по Москве и Московской области',
            readmoreText: 'Каталог спецтехники'
          }
        ] %}

          <figure
            class="main-promo__slide swiper-slide"
            style="background-image: url('{{ item.image }}')"
          >
            <figcaption class="main-promo__slide-caption">
              <p class="main-promo__slide-title">{{ item.title | safe }}</p>
              <p class="main-promo__slide-subtitle">{{ item.subtitle | safe }}</p>
              <a href="#" class="main-promo__slide-readmore">{{ item.readmoreText }}</a>
            </figcaption>
          </figure>

        {% endfor %}

      </div>
      <nav class="main-promo__pagination swiper-pagination" data-swiper-pagination></nav>
    </div>
  </section>
</main-promo>
<!-- /main-promo -->
{% endmacro %}
```

{% sample lang='script' %}

### Script

```JS
import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import Swiper from 'swiper/dist/js/swiper.min';
import 'swiper/dist/css/swiper.min.css';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|-------------------------------------------------------------------------------
| Регистрация состояния компонента
|-------------------------------------------------------------------------------
*/
store.registerModule('MainPromo', {
  namespaced: true,

  state() {
    return {};
  },

  getters: {},

  mutations: {}
});

/*
|-------------------------------------------------------------------------------
| Объявление объекта опций компонента
|-------------------------------------------------------------------------------
*/
const MainPromo = {
  computed: {
    ...mapState('MainPromo', []),

    ...mapGetters('MainPromo', [])
  },

  methods: {
    ...mapMutations('MainPromo', [])
  },

  mounted() {
    const swiper = new Swiper('.main-promo [data-swiper]', {
      centeredSlides: true,
      slidesPerView: 1,
      speed: 1000,
      loop: true,
      effect: 'fade',
      autoHeight: false,
      fadeEffect: {
        crossFade: true
      },
      autoplay: {
        delay: 5000,
        disableOnInteraction: false
      },
      pagination: {
        el: '.main-promo [data-swiper-pagination]',
        clickable: true
      }
    });
    swiper.autoplay.start();
  }
};

Vue.component('MainPromo', MainPromo);

export default MainPromo;

```

{% sample lang='style' %}

### Style

```SCSS
.main-promo {
  position: relative;
  width: 100%;
  background-color: #083ea7;
}

.main-promo__content-wrap {
  position: relative;
  width: 100%;
}

.main-promo__slide {
  position: relative;
  align-self: stretch;
  width: 100%;
  height: auto;
  padding: 130px 40px 87px;
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;

  @media screen and (--mobile) {
    padding: 88px 10px 140px;

    &::before {
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      content: '';
      background-color: color(#245dba alpha(.8));
    }
  }
}

.main-promo__slide-caption {
  position: relative;
  z-index: 1;
  max-width: 460px;
}

.main-promo__slide-title {
  margin-bottom: 10px;
  color: #fff;
  font-size: 42px;
  font-weight: 700;
  line-height: 48px;
}

.main-promo__slide-subtitle {
  margin-bottom: 40px;
  color: #fff;
  font-size: 18px;
  font-weight: 300;
  line-height: 28px;
}

.main-promo__slide-readmore {
  display: inline-block;
  padding: 30px 20px;
  white-space: nowrap;
  color: #000;
  background-color: #fc0;
  font-size: 18px;
  font-weight: 500;

  &:hover {
    background-color: #ffe500;
  }
}

.main-promo__pagination {
  &.swiper-pagination {
    position: absolute;
    bottom: 30px;
    left: 40px;
    display: flex;
    width: calc(100% - 80px);
    text-align: left;

    @media screen and (--mobile) {
      left: 10px;
      width: calc(100% - 20px);
    }

    & .swiper-pagination-bullet {
      display: block;
      width: 60px;
      height: 5px;
      margin-right: 10px;
      margin-left: 0;
      transition-duration: .4s;
      opacity: 1;
      border-radius: 0;
      background-color: #d8d8d8;

      &:hover {
        background-color: #ffe500;
      }

      &.swiper-pagination-bullet-active {
        background-color: #fc0;
      }
    }
  }
}

```

{% sample lang='test' %}

### Tests

```JS
import MainPromo from './main-promo';

describe('MainPromo', () => {
  it('компоненту передан объект опций', () => {
    expect(typeof MainPromo).toBe('object');
  });
});
```

{% endmethod %}

{% endblock %}