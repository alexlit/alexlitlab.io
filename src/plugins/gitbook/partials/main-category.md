{% extends '../_layouts/custom.htm' %}

[//]: HEADER

{% block header %}

# MainCategory

{% endblock %}

[//]: COMPONENT

{% block component %}main-category{% endblock %}

[//]: API

{% block api %}

API отсутствует.

{% endblock %}

[//]: ASSETS

{% block assets %}

{% method %}

### Пример использования

```HTML
<main-category inline-template v-cloak>
  <section class="main-category">
    <div class="main-category__content-wrap">
      <div class="main-category__items-wrap">
        <figure class="main-category__item">
          <div class="main-category__item-image" style="background-image:url(/assets/images/main-category.1.jpg)" v-if="$mq !== 'mobile'"></div>
          <figcaption class="main-category__item-caption">
            <p class="main-category__item-title">Экскаватор JCB BCX Super</p>
            <div class="main-category__item-caption-container">
              <div class="main-category__item-spec">
                <div class="main-category__item-image" style="background-image:url(/assets/images/main-category.1.jpg)" v-if="$mq === 'mobile'"></div>
                <ul class="main-category__item-spec-list">
                  <li>Цена за час - 900 руб.</li>
                  <li>Объем ковша - 1,1/0,4</li>
                  <li>Доп. гидромолот</li>
                  <li>Масса - 8800 кг.</li>
                </ul>
              </div>
              <div class="main-category__item-cost">
                <p class="main-category__item-cost-title">Цена за смену:</p>
                <p class="main-category__item-cost-summary">от [[ 13000 | thousandSeparator ]] руб.</p>
              </div>
              <div class="main-category__item-buttons-wrap">
                <button class="main-category__item-order-button" type="button" @click="setSuccessState(false);setFormData({IMAGE: '/assets/images/main-category.1.jpg',TITLE: 'Экскаватор JCB BCX Super'});showModal('form-fast-order');">Быстрый заказ</button> <a href="#" class="main-category__item-readmore">Подробнее</a></div>
            </div>
          </figcaption>
        </figure>
        <figure class="main-category__item">
          <div class="main-category__item-image" style="background-image:url(/assets/images/main-category.1.jpg)" v-if="$mq !== 'mobile'"></div>
          <figcaption class="main-category__item-caption">
            <p class="main-category__item-title">Экскаватор-погрузчик NewHolland 115</p>
            <div class="main-category__item-caption-container">
              <div class="main-category__item-spec">
                <div class="main-category__item-image" style="background-image:url(/assets/images/main-category.1.jpg)" v-if="$mq === 'mobile'"></div>
                <ul class="main-category__item-spec-list">
                  <li>Цена за час - 900 руб.</li>
                  <li>Объем ковша - 1,1/0,4</li>
                  <li>Доп. гидромолот</li>
                  <li>Масса - 8800 кг.</li>
                </ul>
              </div>
              <div class="main-category__item-cost">
                <p class="main-category__item-cost-title">Цена за смену:</p>
                <p class="main-category__item-cost-summary">от [[ 9000 | thousandSeparator ]] руб.</p>
              </div>
              <div class="main-category__item-buttons-wrap">
                <button class="main-category__item-order-button" type="button" @click="setSuccessState(false);setFormData({IMAGE: '/assets/images/main-category.1.jpg',TITLE: 'Экскаватор-погрузчик NewHolland 115'});showModal('form-fast-order');">Быстрый заказ</button> <a href="#" class="main-category__item-readmore">Подробнее</a></div>
            </div>
          </figcaption>
        </figure>
        <figure class="main-category__item">
          <div class="main-category__item-image" style="background-image:url(/assets/images/main-category.1.jpg)" v-if="$mq !== 'mobile'"></div>
          <figcaption class="main-category__item-caption">
            <p class="main-category__item-title">Экскаватор JCB BCX Super</p>
            <div class="main-category__item-caption-container">
              <div class="main-category__item-spec">
                <div class="main-category__item-image" style="background-image:url(/assets/images/main-category.1.jpg)" v-if="$mq === 'mobile'"></div>
                <ul class="main-category__item-spec-list">
                  <li>Цена за час - 900 руб.</li>
                  <li>Объем ковша - 1,1/0,4</li>
                  <li>Доп. гидромолот</li>
                  <li>Масса - 8800 кг.</li>
                </ul>
              </div>
              <div class="main-category__item-cost">
                <p class="main-category__item-cost-title">Цена за смену:</p>
                <p class="main-category__item-cost-summary">от [[ 13000 | thousandSeparator ]] руб.</p>
              </div>
              <div class="main-category__item-buttons-wrap">
                <button class="main-category__item-order-button" type="button" @click="setSuccessState(false);setFormData({IMAGE: '/assets/images/main-category.1.jpg',TITLE: 'Экскаватор JCB BCX Super'});showModal('form-fast-order');">Быстрый заказ</button> <a href="#" class="main-category__item-readmore">Подробнее</a></div>
            </div>
          </figcaption>
        </figure>
        <figure class="main-category__item">
          <div class="main-category__item-image" style="background-image:url(/assets/images/main-category.1.jpg)" v-if="$mq !== 'mobile'"></div>
          <figcaption class="main-category__item-caption">
            <p class="main-category__item-title">Экскаватор-погрузчик NewHolland 115</p>
            <div class="main-category__item-caption-container">
              <div class="main-category__item-spec">
                <div class="main-category__item-image" style="background-image:url(/assets/images/main-category.1.jpg)" v-if="$mq === 'mobile'"></div>
                <ul class="main-category__item-spec-list">
                  <li>Цена за час - 900 руб.</li>
                  <li>Объем ковша - 1,1/0,4</li>
                  <li>Доп. гидромолот</li>
                  <li>Масса - 8800 кг.</li>
                </ul>
              </div>
              <div class="main-category__item-cost">
                <p class="main-category__item-cost-title">Цена за смену:</p>
                <p class="main-category__item-cost-summary">от [[ 9000 | thousandSeparator ]] руб.</p>
              </div>
              <div class="main-category__item-buttons-wrap">
                <button class="main-category__item-order-button" type="button" @click="setSuccessState(false);setFormData({IMAGE: '/assets/images/main-category.1.jpg',TITLE: 'Экскаватор-погрузчик NewHolland 115'});showModal('form-fast-order');">Быстрый заказ</button> <a href="#" class="main-category__item-readmore">Подробнее</a></div>
            </div>
          </figcaption>
        </figure>
        <figure class="main-category__item">
          <div class="main-category__item-image" style="background-image:url(/assets/images/main-category.1.jpg)" v-if="$mq !== 'mobile'"></div>
          <figcaption class="main-category__item-caption">
            <p class="main-category__item-title">Экскаватор JCB BCX Super</p>
            <div class="main-category__item-caption-container">
              <div class="main-category__item-spec">
                <div class="main-category__item-image" style="background-image:url(/assets/images/main-category.1.jpg)" v-if="$mq === 'mobile'"></div>
                <ul class="main-category__item-spec-list">
                  <li>Цена за час - 900 руб.</li>
                  <li>Объем ковша - 1,1/0,4</li>
                  <li>Доп. гидромолот</li>
                  <li>Масса - 8800 кг.</li>
                </ul>
              </div>
              <div class="main-category__item-cost">
                <p class="main-category__item-cost-title">Цена за смену:</p>
                <p class="main-category__item-cost-summary">от [[ 13000 | thousandSeparator ]] руб.</p>
              </div>
              <div class="main-category__item-buttons-wrap">
                <button class="main-category__item-order-button" type="button" @click="setSuccessState(false);setFormData({IMAGE: '/assets/images/main-category.1.jpg',TITLE: 'Экскаватор JCB BCX Super'});showModal('form-fast-order');">Быстрый заказ</button> <a href="#" class="main-category__item-readmore">Подробнее</a></div>
            </div>
          </figcaption>
        </figure>
        <figure class="main-category__item">
          <div class="main-category__item-image" style="background-image:url(/assets/images/main-category.1.jpg)" v-if="$mq !== 'mobile'"></div>
          <figcaption class="main-category__item-caption">
            <p class="main-category__item-title">Экскаватор-погрузчик NewHolland 115</p>
            <div class="main-category__item-caption-container">
              <div class="main-category__item-spec">
                <div class="main-category__item-image" style="background-image:url(/assets/images/main-category.1.jpg)" v-if="$mq === 'mobile'"></div>
                <ul class="main-category__item-spec-list">
                  <li>Цена за час - 900 руб.</li>
                  <li>Объем ковша - 1,1/0,4</li>
                  <li>Доп. гидромолот</li>
                  <li>Масса - 8800 кг.</li>
                </ul>
              </div>
              <div class="main-category__item-cost">
                <p class="main-category__item-cost-title">Цена за смену:</p>
                <p class="main-category__item-cost-summary">от [[ 9000 | thousandSeparator ]] руб.</p>
              </div>
              <div class="main-category__item-buttons-wrap">
                <button class="main-category__item-order-button" type="button" @click="setSuccessState(false);setFormData({IMAGE: '/assets/images/main-category.1.jpg',TITLE: 'Экскаватор-погрузчик NewHolland 115'});showModal('form-fast-order');">Быстрый заказ</button> <a href="#" class="main-category__item-readmore">Подробнее</a></div>
            </div>
          </figcaption>
        </figure>
        <figure class="main-category__item">
          <div class="main-category__item-image" style="background-image:url(/assets/images/main-category.1.jpg)" v-if="$mq !== 'mobile'"></div>
          <figcaption class="main-category__item-caption">
            <p class="main-category__item-title">Экскаватор JCB BCX Super</p>
            <div class="main-category__item-caption-container">
              <div class="main-category__item-spec">
                <div class="main-category__item-image" style="background-image:url(/assets/images/main-category.1.jpg)" v-if="$mq === 'mobile'"></div>
                <ul class="main-category__item-spec-list">
                  <li>Цена за час - 900 руб.</li>
                  <li>Объем ковша - 1,1/0,4</li>
                  <li>Доп. гидромолот</li>
                  <li>Масса - 8800 кг.</li>
                </ul>
              </div>
              <div class="main-category__item-cost">
                <p class="main-category__item-cost-title">Цена за смену:</p>
                <p class="main-category__item-cost-summary">от [[ 13000 | thousandSeparator ]] руб.</p>
              </div>
              <div class="main-category__item-buttons-wrap">
                <button class="main-category__item-order-button" type="button" @click="setSuccessState(false);setFormData({IMAGE: '/assets/images/main-category.1.jpg',TITLE: 'Экскаватор JCB BCX Super'});showModal('form-fast-order');">Быстрый заказ</button> <a href="#" class="main-category__item-readmore">Подробнее</a></div>
            </div>
          </figcaption>
        </figure>
        <figure class="main-category__item">
          <div class="main-category__item-image" style="background-image:url(/assets/images/main-category.1.jpg)" v-if="$mq !== 'mobile'"></div>
          <figcaption class="main-category__item-caption">
            <p class="main-category__item-title">Экскаватор-погрузчик NewHolland 115</p>
            <div class="main-category__item-caption-container">
              <div class="main-category__item-spec">
                <div class="main-category__item-image" style="background-image:url(/assets/images/main-category.1.jpg)" v-if="$mq === 'mobile'"></div>
                <ul class="main-category__item-spec-list">
                  <li>Цена за час - 900 руб.</li>
                  <li>Объем ковша - 1,1/0,4</li>
                  <li>Доп. гидромолот</li>
                  <li>Масса - 8800 кг.</li>
                </ul>
              </div>
              <div class="main-category__item-cost">
                <p class="main-category__item-cost-title">Цена за смену:</p>
                <p class="main-category__item-cost-summary">от [[ 9000 | thousandSeparator ]] руб.</p>
              </div>
              <div class="main-category__item-buttons-wrap">
                <button class="main-category__item-order-button" type="button" @click="setSuccessState(false);setFormData({IMAGE: '/assets/images/main-category.1.jpg',TITLE: 'Экскаватор-погрузчик NewHolland 115'});showModal('form-fast-order');">Быстрый заказ</button> <a href="#" class="main-category__item-readmore">Подробнее</a></div>
            </div>
          </figcaption>
        </figure>
      </div>
    </div>
  </section>
</main-category>
```

{% sample lang='template' %}

### Template

```TWIG
{% macro MainCategory(
  mod = null,
  items = [
    {
      image: '/assets/images/main-category.1.jpg',
      title: 'Экскаватор JCB BCX Super',
      summary: 13000
    },{
      image: '/assets/images/main-category.1.jpg',
      title: 'Экскаватор-погрузчик NewHolland 115',
      summary: 9000
    },{
      image: '/assets/images/main-category.1.jpg',
      title: 'Экскаватор JCB BCX Super',
      summary: 13000
    },{
      image: '/assets/images/main-category.1.jpg',
      title: 'Экскаватор-погрузчик NewHolland 115',
      summary: 9000
    },{
      image: '/assets/images/main-category.1.jpg',
      title: 'Экскаватор JCB BCX Super',
      summary: 13000
    },{
      image: '/assets/images/main-category.1.jpg',
      title: 'Экскаватор-погрузчик NewHolland 115',
      summary: 9000
    },{
      image: '/assets/images/main-category.1.jpg',
      title: 'Экскаватор JCB BCX Super',
      summary: 13000
    },{
      image: '/assets/images/main-category.1.jpg',
      title: 'Экскаватор-погрузчик NewHolland 115',
      summary: 9000
    }
  ]
) %}
<!-- main-category -->
<main-category inline-template v-cloak>
  <section class="main-category{% if mod %} main-category--{{ mod }}{% endif %}">
    <div class="main-category__content-wrap">
      <div class="main-category__items-wrap">

        {% for item in items %}

          <figure class="main-category__item">
            <div
              class="main-category__item-image"
              style="background-image: url({{ item.image }});"
              v-if="$mq !== 'mobile'"
            ></div>
            <figcaption class="main-category__item-caption">
              <p class="main-category__item-title">{{ item.title }}</p>
              <div class="main-category__item-caption-container">
                <div class="main-category__item-spec">
                  <div
                    class="main-category__item-image"
                    style="background-image: url({{ item.image }});"
                    v-if="$mq === 'mobile'"
                  ></div>
                  <ul class="main-category__item-spec-list">
                    <li>Цена за час - 900 руб.</li>
                    <li>Объем ковша - 1,1/0,4</li>
                    <li>Доп. гидромолот</li>
                    <li>Масса - 8800 кг.</li>
                  </ul>
                </div>
                <div class="main-category__item-cost">
                  <p class="main-category__item-cost-title">Цена за смену:</p>
                  <p class="main-category__item-cost-summary">от [[ {{ item.summary }} | thousandSeparator ]] руб.</p>
                </div>
                <div class="main-category__item-buttons-wrap">
                  <button
                    class="main-category__item-order-button"
                    type="button"
                    @click="
                      setSuccessState(false);
                      setFormData({
                        IMAGE: '{{ item.image }}',
                        TITLE: '{{ item.title }}'
                      });
                      showModal('form-fast-order');
                    "
                  >Быстрый заказ</button>
                  <a href="#" class="main-category__item-readmore">Подробнее</a>
                </div>
              </div>
            </figcaption>
          </figure>

        {% endfor %}

      </div>
    </div>
  </section>
</main-category>
<!-- /main-category -->
{% endmacro %}
```

{% sample lang='script' %}

### Script

```JS
import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|-------------------------------------------------------------------------------
| Регистрация состояния компонента
|-------------------------------------------------------------------------------
*/
store.registerModule('MainCategory', {
  namespaced: true,

  state() {
    return {};
  },

  getters: {},

  mutations: {}
});

/*
|-------------------------------------------------------------------------------
| Объявление объекта опций компонента
|-------------------------------------------------------------------------------
*/
const MainCategory = {
  computed: {
    ...mapState('MainCategory', []),
    ...mapState('App', ['modal']),

    ...mapGetters('MainCategory', [])
  },

  methods: {
    ...mapMutations('MainCategory', []),
    ...mapMutations('App', ['showModal']),
    ...mapMutations('FormFastOrder', ['setSuccessState', 'setFormData'])
  }
};

Vue.component('MainCategory', MainCategory);

export default MainCategory;

```

{% sample lang='style' %}

### Style

```SCSS
.main-category {
  position: relative;
  width: 100%;
  margin-top: 20px;
  padding: 0 10px;

  @media screen and (--mobile) {
    margin-top: 40px;
  }
}

.main-category__content-wrap {
  position: relative;
  width: 100%;
  max-width: 940px;
  margin: 0 auto;
  padding: 30px 0 0;
  border-top: 1px solid #eaeaea;
}

.main-category__items-wrap {
  position: relative;
}

.main-category__item {
  position: relative;
  display: flex;
  width: 100%;
  background-color: #fff;
  box-shadow: 0 2px 11px rgba(0, 0, 0, .15);

  &:not(:last-child) {
    margin-bottom: 20px;
  }
}

.main-category__item-image {
  width: 220px;
  background-color: #ebebeb;
  background-repeat: no-repeat;
  background-position: center;
  background-size: contain;
  background-blend-mode: multiply;

  @media screen and (--mobile) {
    width: 100px;
    height: 80px;
    padding: 10px;
    background-origin: content-box;
  }
}

.main-category__item-caption {
  flex-grow: 1;
  padding: 15px 20px;

  @media screen and (--mobile) {
    padding: 10px;
  }
}

.main-category__item-title {
  margin-bottom: 20px;
  color: #000;
  font-size: 24px;
  font-weight: 500;
  line-height: 30px;

  @media screen and (--mobile) {
    margin-bottom: 10px;
    font-size: 18px;
    line-height: 22px;
  }
}

.main-category__item-caption-container {
  display: flex;
  justify-content: space-between;
  width: 100%;

  @media screen and (--mobile) {
    display: block;
  }
}

.main-category__item-spec {
  width: calc((100% - 20px * 2) / 3);

  @media screen and (--mobile) {
    display: flex;
    justify-content: space-between;
    width: 100%;
    margin-bottom: 5px;
  }
}

.main-category__item-spec-list {
  list-style: none;

  @media screen and (--mobile) {
    width: calc(100% - 110px);
  }

  & li {
    color: #000;
    font-size: 16px;
    font-weight: 300;
    line-height: 24px;

    @media screen and (--mobile) {
      font-size: 14px;
      line-height: 21px;
    }
  }
}

.main-category__item-cost {
  width: calc((100% - 20px * 2) / 3);

  @media screen and (--mobile) {
    width: 100%;
    margin-bottom: 15px;
  }
}

.main-category__item-cost-title {
  margin-bottom: 5px;
  letter-spacing: 1px;
  text-transform: uppercase;
  color: #000;
  font-size: 12px;
  font-weight: 500;
  line-height: 18px;

  @media screen and (--mobile) {
    font-size: 10px;
    line-height: 14px;
  }
}

.main-category__item-cost-summary {
  color: #000;
  font-size: 30px;
  font-weight: 500;
  line-height: 36px;

  @media screen and (--mobile) {
    font-size: 24px;
    line-height: 30px;
  }
}

.main-category__item-buttons-wrap {
  display: flex;
  align-items: flex-start;
  flex-direction: column;
  justify-content: flex-start;
  width: calc((100% - 20px * 2) / 3);

  @media screen and (--mobile) {
    flex-direction: row;
    width: 100%;
  }
}

.main-category__item-order-button {
  margin-bottom: 5px;
  padding: 15px 10px;
  letter-spacing: 1px;
  text-transform: uppercase;
  color: #000;
  background-color: #ffd101;
  font-size: 12px;
  font-weight: 500;
  line-height: 18px;

  @media screen and (--mobile) {
    margin-right: 10px;
    margin-bottom: 0;
  }

  &:hover {
    background-color: #ffe500;
  }
}

.main-category__item-readmore {
  display: block;
  padding: 15px 10px;
  letter-spacing: 1px;
  text-transform: uppercase;
  color: #fff;
  background-color: #000;
  font-size: 12px;
  font-weight: 500;
  line-height: 18px;

  &:hover {
    background-color: #215ed4;
  }
}

```

{% sample lang='test' %}

### Tests

```JS
import MainCategory from './main-category';

describe('MainCategory', () => {
  it('компоненту передан объект опций', () => {
    expect(typeof MainCategory).toBe('object');
  });
});
```

{% endmethod %}

{% endblock %}