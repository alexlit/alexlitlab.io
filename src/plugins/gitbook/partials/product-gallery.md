{% extends '../_layouts/custom.htm' %}

[//]: HEADER

{% block header %}

# ProductGallery

{% endblock %}

[//]: COMPONENT

{% block component %}product-gallery{% endblock %}

[//]: API

{% block api %}

API отсутствует.

{% endblock %}

[//]: ASSETS

{% block assets %}

{% method %}

### Пример использования

```HTML
<product-gallery inline-template v-cloak>
  <section class="product-gallery">
    <div class="product-gallery__content-wrap">
      <div class="product-gallery__top swiper-container" data-gallery="top">
        <div class="swiper-wrapper">
          <img class="product-gallery__top-item swiper-slide" src="/assets/images/product-gallery.1.jpg" alt="">
          <img class="product-gallery__top-item swiper-slide" src="/assets/images/main-promo.2.jpg" alt="">
          <img class="product-gallery__top-item swiper-slide" src="/assets/images/main-promo.1.jpg" alt="">
          <img class="product-gallery__top-item swiper-slide" src="/assets/images/product-gallery.1.jpg" alt="">
          <img class="product-gallery__top-item swiper-slide" src="/assets/images/main-promo.2.jpg" alt="">
          <img class="product-gallery__top-item swiper-slide" src="/assets/images/main-promo.1.jpg" alt="">
        </div>
      </div>
      <nav class="product-gallery__thumbs swiper-container" data-gallery="thumbs">
        <div class="swiper-wrapper">
          <img class="product-gallery__thumbs-item swiper-slide" src="/assets/images/product-gallery.1.jpg" alt="">
          <img class="product-gallery__thumbs-item swiper-slide" src="/assets/images/main-promo.2.jpg" alt="">
          <img class="product-gallery__thumbs-item swiper-slide" src="/assets/images/main-promo.1.jpg" alt="">
          <img class="product-gallery__thumbs-item swiper-slide" src="/assets/images/product-gallery.1.jpg" alt="">
          <img class="product-gallery__thumbs-item swiper-slide" src="/assets/images/main-promo.2.jpg" alt="">
          <img class="product-gallery__thumbs-item swiper-slide" src="/assets/images/main-promo.1.jpg" alt="">
        </div>
      </nav>
    </div>
  </section>
</product-gallery>
```

{% sample lang='template' %}

### Template

```TWIG
{% macro ProductGallery(
  items = [
    '/assets/images/product-gallery.1.jpg',
    '/assets/images/main-promo.2.jpg',
    '/assets/images/main-promo.1.jpg',
    '/assets/images/product-gallery.1.jpg',
    '/assets/images/main-promo.2.jpg',
    '/assets/images/main-promo.1.jpg'
  ]
) %}
<!-- product-gallery -->
<product-gallery inline-template v-cloak>
  <section class="product-gallery">
    <div class="product-gallery__content-wrap">
      <div class="product-gallery__top swiper-container" data-gallery="top">
        <div class="swiper-wrapper">

          {% for item in items %}

            <img class="product-gallery__top-item swiper-slide" src="{{ item }}" alt="">

          {% endfor %}

        </div>
      </div>
      <nav class="product-gallery__thumbs swiper-container" data-gallery="thumbs">
        <div class="swiper-wrapper">

          {% for item in items %}

            <img class="product-gallery__thumbs-item swiper-slide" src="{{ item }}" alt="">

          {% endfor %}

        </div>
      </nav>
    </div>
  </section>
</product-gallery>
<!-- /product-gallery -->
{% endmacro %}
```

{% sample lang='script' %}

### Script

```JS
import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import Swiper from 'swiper/dist/js/swiper.min';
import 'swiper/dist/css/swiper.min.css';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|-------------------------------------------------------------------------------
| Регистрация состояния компонента
|-------------------------------------------------------------------------------
*/
store.registerModule('ProductGallery', {
  namespaced: true,

  state() {
    return {};
  },

  getters: {},

  mutations: {}
});

/*
|-------------------------------------------------------------------------------
| Объявление объекта опций компонента
|-------------------------------------------------------------------------------
*/
const ProductGallery = {
  computed: {
    ...mapState('ProductGallery', []),

    ...mapGetters('ProductGallery', [])
  },

  methods: {
    ...mapMutations('ProductGallery', [])
  },

  mounted() {
    const galleryTop = new Swiper('.product-gallery [data-gallery="top"]', {
      spaceBetween: 10,
      slidesPerView: 1,
      loop: true,
      loopedSlides: 5,
      speed: 1000
    });
    const galleryThumbs = new Swiper(
      '.product-gallery [data-gallery="thumbs"]',
      {
        spaceBetween: 10,
        centeredSlides: false,
        slidesPerView: 'auto',
        touchRatio: 0.2,
        slideToClickedSlide: true,
        loop: true,
        loopedSlides: 5,
        speed: 1000
      }
    );
    galleryTop.controller.control = galleryThumbs;
    galleryThumbs.controller.control = galleryTop;
  }
};

Vue.component('ProductGallery', ProductGallery);

export default ProductGallery;

```

{% sample lang='style' %}

### Style

```SCSS
.product-gallery {
  position: relative;
  width: 100%;
  background-color: #f7f8fa;

  @media screen and (--mobile) {
    background-color: #fff;
  }
}

.product-gallery__content-wrap {
  position: relative;
  width: 100%;
  max-width: 940px;
  margin: 0 auto;
  padding-bottom: 20px;
}

.product-gallery__top {
  width: 100%;
  margin-bottom: 20px;
  cursor: grab;

  @media screen and (--mobile) {
    margin-bottom: 10px;
    background-color: #f7f8fa;
  }
}

.product-gallery__top-item {
  position: relative;
  display: block;
  height: 425px;
  object-fit: cover;
  object-position: center;

  @media screen and (--mobile) {
    height: 225px;
  }
}

.product-gallery__thumbs {
  width: calc(100% - 40px);
  margin: 0 auto;
  cursor: grab;

  @media screen and (--mobile) {
    width: calc(100% - 20px);
  }
}

.product-gallery__thumbs-item {
  display: block;
  width: 100px;
  height: 60px;
  cursor: pointer;
  transition-duration: .4s;
  transition-property: border-color;
  border: 2px solid #fff;
  background-color: #f7f8fa;
  object-fit: cover;
  object-position: center;

  &.swiper-slide-active,
  &:hover {
    border-color: #ffd101;
  }
}

```

{% sample lang='test' %}

### Tests

```JS
import ProductGallery from './product-gallery';

describe('ProductGallery', () => {
  it('компоненту передан объект опций', () => {
    expect(typeof ProductGallery).toBe('object');
  });
});
```

{% endmethod %}

{% endblock %}