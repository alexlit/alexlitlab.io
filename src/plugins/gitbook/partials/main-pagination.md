{% extends '../_layouts/custom.htm' %}

[//]: HEADER

{% block header %}

# MainPagination

{% endblock %}

[//]: COMPONENT

{% block component %}main-pagination{% endblock %}

[//]: API

{% block api %}

API отсутствует.

{% endblock %}

[//]: ASSETS

{% block assets %}

{% method %}

### Пример использования

```HTML
<main-pagination inline-template v-cloak>
  <nav class="main-pagination">
    <div class="main-pagination__content-wrap"><a href="#" class="main-pagination__item main-pagination__item--active">1</a> <a href="#" class="main-pagination__item">2</a> <a href="#" class="main-pagination__item">3</a> <a href="#" class="main-pagination__item">4</a></div>
  </nav>
</main-pagination>
```

{% sample lang='template' %}

### Template

```TWIG
{% macro MainPagination() %}
<!-- main-pagination -->
<main-pagination inline-template v-cloak>
  <nav class="main-pagination">
    <div class="main-pagination__content-wrap">
      <a href="#" class="main-pagination__item main-pagination__item--active">1</a>
      <a href="#" class="main-pagination__item">2</a>
      <a href="#" class="main-pagination__item">3</a>
      <a href="#" class="main-pagination__item">4</a>
    </div>
  </nav>
</main-pagination>
<!-- /main-pagination -->
{% endmacro %}
```

{% sample lang='script' %}

### Script

```JS
import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|-------------------------------------------------------------------------------
| Регистрация состояния компонента
|-------------------------------------------------------------------------------
*/
store.registerModule('MainPagination', {
  namespaced: true,

  state() {
    return {};
  },

  getters: {},

  mutations: {}
});

/*
|-------------------------------------------------------------------------------
| Объявление объекта опций компонента
|-------------------------------------------------------------------------------
*/
const MainPagination = {
  computed: {
    ...mapState('MainPagination', []),

    ...mapGetters('MainPagination', [])
  },

  methods: {
    ...mapMutations('MainPagination', [])
  }
};

Vue.component('MainPagination', MainPagination);

export default MainPagination;
```

{% sample lang='style' %}

### Style

```SCSS
.main-pagination {
  position: relative;
  width: 100%;
  margin: 50px 0;
  padding: 0 10px;

  @media screen and (--mobile) {
    margin: 30px 0;
  }
}

.main-pagination__content-wrap {
  position: relative;
  display: flex;
  width: 100%;
  max-width: 940px;
  margin: 0 auto;
}

.main-pagination__item {
  display: block;
  width: 60px;
  user-select: none;
  text-align: center;
  color: #fff;
  background-color: #000;
  font-size: 18px;
  font-weight: 500;
  line-height: 60px;

  &:not(:last-child) {
    margin-right: 5px;
  }

  &--active {
    pointer-events: none;
    background-color: #215ed4;
  }

  &:hover {
    background-color: #437eef;
  }
}

```

{% sample lang='test' %}

### Tests

```JS
import MainPagination from './main-pagination';

describe('MainPagination', () => {
  it('компоненту передан объект опций', () => {
    expect(typeof MainPagination).toBe('object');
  });
});
```

{% endmethod %}

{% endblock %}