{% extends "../_layouts/custom.htm" %}

[//]: HEADER

{% block header %}

# UiSpinner

Анимированый css-прелоадер. Рализован на основе идеи
[SpinKit](http://tobiasahlin.com/spinkit/).

У родительского контейнера св-во `position` должно иметь значение != `static`.

{% endblock %}

[//]: COMPONENT

{% block component %}ui-spinner{% endblock %}

[//]: API

{% block api %}

| Атрибут   | Описание                     | Тип     | Возможные значения                                                                                                                                              | По-умолчанию     |
| --------- | ---------------------------- | ------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------- |
| `random`  | Установить рандомный шаблон. | Boolean | -                                                                                                                                                               | `false`          |
| `layout`  | Шаблон спиннера              | String  | `chasing-dots` `circle` `cube-grid` `double-bounce` `fading-circle` `folding-cube` `pulse` `rectangle-bounce` `rotating-plane` `three-bounce` `wandering-cubes` | `rotating-plane` |
| `color`   | Цвет спиннера                | String  | Любой цветовой формат поддерживаемый css                                                                                                                        | `#fff`           |
| `opacity` | Прозрачность спиннера        | Number  | От 0 до 1                                                                                                                                                       | `1`              |
| `size`    | Размер спиннера в px         | Number  | -                                                                                                                                                               | `40`             |

{% endblock %}

[//]: ASSETS

{% block assets %}

{% method %}

### Пример использования

```HTML
<div style="display:flex;flex-wrap:wrap;padding:20px">
  <div style="width:auto;position:relative;height:180px;width:180px;text-align:center">
    <strong>chasing-dots</strong>
    <ui-spinner layout="chasing-dots" color="#413C58" :size="80" :opacity="1"></ui-spinner>
  </div>
  <div style="width:auto;position:relative;height:180px;width:180px;text-align:center">
    <strong>circle</strong>
    <ui-spinner layout="circle" color="#6665DD" :size="80" :opacity="1"></ui-spinner>
  </div>
  <div style="width:auto;position:relative;height:180px;width:180px;text-align:center">
    <strong>cube-grid</strong>
    <ui-spinner layout="cube-grid" color="#BFD7B5" :size="80" :opacity="0.8"></ui-spinner>
  </div>
  <div style="width:auto;position:relative;height:180px;width:180px;text-align:center">
    <strong>double-bounce</strong>
    <ui-spinner layout="double-bounce" color="#A39BA8" :size="80" :opacity="1"></ui-spinner>
  </div>
  <div style="width:auto;position:relative;height:180px;width:180px;text-align:center">
    <strong>fading-circle</strong>
    <ui-spinner layout="fading-circle" color="#9CE37D" :size="80" :opacity="1"></ui-spinner>
  </div>
  <div style="width:auto;position:relative;height:180px;width:180px;text-align:center">
    <strong>folding-cube</strong>
    <ui-spinner layout="folding-cube" color="#7E4E60" :size="80" :opacity="1"></ui-spinner>
  </div>
  <div style="width:auto;position:relative;height:180px;width:180px;text-align:center">
    <strong>pulse</strong>
    <ui-spinner layout="pulse" color="#B8336A" :size="80" :opacity="1"></ui-spinner>
  </div>
  <div style="width:auto;position:relative;height:180px;width:180px;text-align:center">
    <strong>rectangle-bounce</strong>
    <ui-spinner layout="rectangle-bounce" color="#A44A3F" :size="80" :opacity="1"></ui-spinner>
  </div>
  <div style="width:auto;position:relative;height:180px;width:180px;text-align:center">
    <strong>rotating-plane</strong>
    <ui-spinner layout="rotating-plane" color="#81E979" :size="80" :opacity="1"></ui-spinner>
  </div>
  <div style="width:auto;position:relative;height:180px;width:180px;text-align:center">
    <strong>three-bounce</strong>
    <ui-spinner layout="three-bounce" color="#595A4A" :size="80" :opacity="1"></ui-spinner>
  </div>
  <div style="width:auto;position:relative;height:180px;width:180px;text-align:center">
    <strong>wandering-cubes</strong>
    <ui-spinner layout="wandering-cubes" color="#8FBB99" :size="80" :opacity="1"></ui-spinner>
  </div>
</div>
```

{% sample lang="template" %}

### Вид

```TWIG
<figure
  class="ui-spinner"
  :style="{
    width: size + 'px',
    height: size + 'px',
    opacity: opacity,
    color: color
  }"
>

  <!-- rotating-plane -->
  <div
    v-if="activeLayout === 'rotating-plane'"
    class="ui-spinner__rotating-plane"
  ></div>

  <!-- double-bounce -->
  <div
    v-if="activeLayout === 'double-bounce'"
    class="ui-spinner__double-bounce"
  >
    <span
      v-for="i in 2"
      :class="[
        'ui-spinner__double-bounce-item',
        `ui-spinner__double-bounce-item--position--${ i }`
      ]"
    ></span>
  </div>

  <!-- rectangle-bounce -->
  <div
    v-if="activeLayout === 'rectangle-bounce'"
    class="ui-spinner__rectangle-bounce"
  >
    <span
      v-for="i in 5"
      :class="[
        'ui-spinner__rectangle-bounce-item',
        `ui-spinner__rectangle-bounce-item--position--${ i }`
      ]"
    ></span>
  </div>

  <!-- wandering-cubes -->
  <div
    v-if="activeLayout === 'wandering-cubes'"
    class="ui-spinner__wandering-cubes"
  >
    <span
      v-for="i in 2"
      :class="[
        'ui-spinner__wandering-cubes-item',
        `ui-spinner__wandering-cubes-item--position--${ i }`
      ]"
    ></span>
  </div>

  <!-- pulse -->
  <div
    v-if="activeLayout === 'pulse'"
    class="ui-spinner__pulse"
  ></div>

  <!-- chasing-dots -->
  <div
    v-if="activeLayout === 'chasing-dots'"
    class="ui-spinner__chasing-dots"
  >
    <span
      v-for="i in 2"
      :class="[
        'ui-spinner__chasing-dots-item',
        `ui-spinner__chasing-dots-item--position--${ i }`
      ]"
    ></span>
  </div>

  <!-- three-bounce -->
  <div
    v-if="activeLayout === 'three-bounce'"
    class="ui-spinner__three-bounce"
  >
    <span
      v-for="i in 3"
      :class="[
        'ui-spinner__three-bounce-item',
        `ui-spinner__three-bounce-item--position--${ i }`
      ]"
    ></span>
  </div>

  <!-- circle -->
  <div
    v-if="activeLayout === 'circle'"
    class="ui-spinner__circle"
  >
    <span
      v-for="i in 12"
      :class="[
        'ui-spinner__circle-item',
        `ui-spinner__circle-item--position--${ i }`
      ]"
    ></span>
  </div>

  <!-- cube-grid -->
  <div
    v-if="activeLayout === 'cube-grid'"
    class="ui-spinner__cube-grid"
  >
    <span
      v-for="i in 9"
      :class="[
        'ui-spinner__cube-grid-item',
        `ui-spinner__cube-grid-item--position--${ i }`
      ]"
    ></span>
  </div>

  <!-- fading-circle -->
  <div
    v-if="activeLayout === 'fading-circle'"
    class="ui-spinner__fading-circle"
  >
    <span
      v-for="i in 12"
      :class="[
        'ui-spinner__fading-circle-item',
        `ui-spinner__fading-circle-item--position--${ i }`
      ]"
    ></span>
  </div>

  <!-- folding-cube -->
  <div
    v-if="activeLayout === 'folding-cube'"
    class="ui-spinner__folding-cube"
  >
    <span
      v-for="i in 4"
      :class="[
        'ui-spinner__folding-cube-item',
        `ui-spinner__folding-cube-item--position--${ i }`
      ]"
    ></span>
  </div>

</figure>
```

{% sample lang="script" %}

### Модель

```JS
import Vue from 'vue/dist/vue';

Vue.config.productionTip = false;

/*
|-------------------------------------------------------------------------------
| Объявление объекта опций компонента
|-------------------------------------------------------------------------------
*/
const UiSpinner = {
  delimiters: ['[[', ']]'],

  template: require('./ui-spinner.htm'),

  data() {
    return {
      // возможные шаблоны
      layouts: [
        'chasing-dots',
        'circle',
        'cube-grid',
        'double-bounce',
        'fading-circle',
        'folding-cube',
        'pulse',
        'rectangle-bounce',
        'rotating-plane',
        'three-bounce',
        'wandering-cubes'
      ],
      activeLayout: null
    };
  },

  computed: {
    /**
     * Случайный шаблон
     */
    randomLayout() {
      return this.layouts[Math.floor(Math.random() * this.layouts.length)];
    }
  },

  props: {
    // использовать случайный шаблон?
    random: {
      type: Boolean,
      default: false
    },

    // имя шаблона
    layout: {
      type: String,
      default: 'rotating-plane',
      validator(value) {
        return (
          [
            'chasing-dots',
            'circle',
            'cube-grid',
            'double-bounce',
            'fading-circle',
            'folding-cube',
            'pulse',
            'rectangle-bounce',
            'rotating-plane',
            'three-bounce',
            'wandering-cubes'
          ].indexOf(value) !== -1
        );
      }
    },

    // цвет
    color: {
      type: String,
      default: '#fff'
    },

    // прозрачность
    opacity: {
      type: Number,
      default: 1,
      validator(value) {
        return value >= 0 && value <= 1;
      }
    },

    // размер в px
    size: {
      type: Number,
      default: 40
    }
  },

  mounted() {
    if (this.random) {
      this.activeLayout = this.randomLayout;
    } else {
      this.activeLayout = this.layout;
    }
  }
};

Vue.component('UiSpinner', UiSpinner);

export default UiSpinner;

```

{% sample lang="style" %}

### Style

```SCSS
.ui-spinner {
  position: absolute;
  top: 50%;
  left: 50%;
  box-sizing: border-box;
  margin: 0;
  padding: 0;
  transform: translate(-50%, -50%);
}

/* rotating-plane */

.ui-spinner__rotating-plane {
  position: relative;
  width: 100%;
  height: 100%;
  animation: uiSpinnerRotatingPlane 1.2s infinite ease-in-out;
  color: inherit;
  background-color: currentColor;
}

@keyframes uiSpinnerRotatingPlane {
  0% {
    transform: perspective(120px) rotateX(0deg) rotateY(0deg);
  }

  50% {
    transform: perspective(120px) rotateX(-180.1deg) rotateY(0deg);
  }

  100% {
    transform: perspective(120px) rotateX(-180deg) rotateY(-179.9deg);
  }
}

/* double-bounce */

.ui-spinner__double-bounce {
  position: relative;
  width: 100%;
  height: 100%;
  color: inherit;
}

.ui-spinner__double-bounce-item {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  animation: uiSpinnerDoubleBounce 2s infinite ease-in-out;
  opacity: .6;
  color: inherit;
  border-radius: 50%;
  background-color: currentColor;

  &--position {
    &--1 {
      animation-delay: 0s;
    }

    &--2 {
      animation-delay: -1s;
    }
  }
}

@keyframes uiSpinnerDoubleBounce {
  0%,
  100% {
    transform: scale(0);
  }

  50% {
    transform: scale(1);
  }
}

/* rectangle-bounce */

.ui-spinner__rectangle-bounce {
  position: relative;
  display: flex;
  justify-content: space-between;
  width: 100%;
  height: 100%;
  color: inherit;
}

.ui-spinner__rectangle-bounce-item {
  display: block;
  width: 20%;
  height: 100%;
  animation: uiSpinnerRectangleBounce 1.2s infinite ease-in-out;
  color: inherit;
  border: 1px solid rgba(0, 0, 0, 0);
  background-color: currentColor;
  background-clip: content-box;

  &--position {
    &--1 {
      animation-delay: 0s;
    }

    &--2 {
      animation-delay: -1.1s;
    }

    &--3 {
      animation-delay: -1s;
    }

    &--4 {
      animation-delay: -.9s;
    }

    &--5 {
      animation-delay: -.8s;
    }
  }
}

@keyframes uiSpinnerRectangleBounce {
  0%,
  40%,
  100% {
    transform: scaleY(.4);
  }

  20% {
    transform: scaleY(1);
  }
}

/* wandering-cubes */

.ui-spinner__wandering-cubes {
  position: relative;
  width: 100%;
  height: 100%;
  color: inherit;
}

.ui-spinner__wandering-cubes-item {
  position: absolute;
  top: 0;
  left: 0;
  display: block;
  width: 25%;
  height: 25%;
  animation: uiSpinnerWanderingCubes 1.8s infinite ease-in-out;
  color: inherit;
  background-color: currentColor;

  &--position {
    &--1 {
      animation-delay: 0s;
    }

    &--2 {
      animation-delay: -.9s;
    }
  }
}

@keyframes uiSpinnerWanderingCubes {
  25% {
    transform: translateX(300%) rotate(-90deg) scale(.5);
  }

  50% {
    transform: translateX(300%) translateY(300%) rotate(-179deg);
  }

  50.1% {
    transform: translateX(300%) translateY(300%) rotate(-180deg);
  }

  75% {
    transform: translateX(0%) translateY(300%) rotate(-270deg) scale(.5);
  }

  100% {
    transform: rotate(-360deg);
  }
}

/* pulse */

.ui-spinner__pulse {
  position: relative;
  width: 100%;
  height: 100%;
  animation: uiSpinnerPulse 1s infinite ease-in-out;
  color: inherit;
  border-radius: 100%;
  background-color: currentColor;
}

@keyframes uiSpinnerPulse {
  0% {
    transform: scale(0);
  }

  100% {
    transform: scale(1);
    opacity: 0;
  }
}

/* chasing-dots */

.ui-spinner__chasing-dots {
  position: relative;
  width: 100%;
  height: 100%;
  animation: uiSpinnerChasingDots 2s infinite linear;
  text-align: center;
  color: inherit;
}

.ui-spinner__chasing-dots-item {
  position: absolute;
  display: inline-block;
  width: 60%;
  height: 60%;
  animation: uiSpinnerDoubleBounce 2s infinite ease-in-out;
  color: inherit;
  border-radius: 100%;
  background-color: currentColor;

  &--position {
    &--1 {
      top: 0;
    }

    &--2 {
      top: auto;
      bottom: 0;
      animation-delay: -1s;
    }
  }
}

@keyframes uiSpinnerChasingDots {
  100% {
    transform: rotate(360deg);
  }
}

@keyframes uiSpinnerDoubleBounce {
  0%,
  100% {
    transform: scale(0);
  }

  50% {
    transform: scale(1);
  }
}

/* three-bounce */

.ui-spinner__three-bounce {
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  height: 100%;
  color: inherit;
}

.ui-spinner__three-bounce-item {
  display: block;
  width: 25%;
  height: 25%;
  animation: uiSpinnerThreeBounce 1.4s infinite ease-in-out both;
  color: inherit;
  border-radius: 100%;
  background-color: currentColor;

  &--position {
    &--1 {
      animation-delay: -.32s;
    }

    &--2 {
      animation-delay: -.16s;
    }

    &--3 {
      animation-delay: 0s;
    }
  }
}

@keyframes uiSpinnerThreeBounce {
  0%,
  80%,
  100% {
    transform: scale(0);
  }

  40% {
    transform: scale(1);
  }
}

/* circle */

.ui-spinner__circle {
  position: relative;
  width: 100%;
  height: 100%;
  color: inherit;
}

.ui-spinner__circle-item {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  color: inherit;

  &::before {
    display: block;
    width: 15%;
    height: 15%;
    margin: 0 auto;
    content: '';
    animation: uiSpinnerCircle 1.2s infinite ease-in-out both;
    border-radius: 100%;
    background-color: currentColor;
  }

  &--position {
    &--1 {
      transform: rotate(0deg);

      &::before {
        animation-delay: 0s;
      }
    }

    &--2 {
      transform: rotate(30deg);

      &::before {
        animation-delay: -1.1s;
      }
    }

    &--3 {
      transform: rotate(60deg);

      &::before {
        animation-delay: -1s;
      }
    }

    &--4 {
      transform: rotate(90deg);

      &::before {
        animation-delay: -.9s;
      }
    }

    &--5 {
      transform: rotate(120deg);

      &::before {
        animation-delay: -.8s;
      }
    }

    &--6 {
      transform: rotate(150deg);

      &::before {
        animation-delay: -.7s;
      }
    }

    &--7 {
      transform: rotate(180deg);

      &::before {
        animation-delay: -.6s;
      }
    }

    &--8 {
      transform: rotate(210deg);

      &::before {
        animation-delay: -.5s;
      }
    }

    &--9 {
      transform: rotate(240deg);

      &::before {
        animation-delay: -.4s;
      }
    }

    &--10 {
      transform: rotate(270deg);

      &::before {
        animation-delay: -.3s;
      }
    }

    &--11 {
      transform: rotate(300deg);

      &::before {
        animation-delay: -.2s;
      }
    }

    &--12 {
      transform: rotate(330deg);

      &::before {
        animation-delay: -.1s;
      }
    }
  }
}

@keyframes uiSpinnerCircle {
  0%,
  80%,
  100% {
    transform: scale(0);
  }

  40% {
    transform: scale(1);
  }
}

/* cube-grid */

.ui-spinner__cube-grid {
  position: relative;
  width: 100%;
  height: 100%;
  color: inherit;
}

.ui-spinner__cube-grid-item {
  float: left;
  width: calc(100% / 3);
  height: calc(100% / 3);
  animation: uiSpinnerCubeGrid 1.3s infinite ease-in-out;
  color: inherit;
  background-color: currentColor;

  &--position {
    &--1 {
      animation-delay: .2s;
    }

    &--2 {
      animation-delay: .4s;
    }

    &--3 {
      animation-delay: .4s;
    }

    &--4 {
      animation-delay: .1s;
    }

    &--5 {
      animation-delay: .2s;
    }

    &--6 {
      animation-delay: .4s;
    }

    &--7 {
      animation-delay: 0s;
    }

    &--8 {
      animation-delay: .1s;
    }

    &--9 {
      animation-delay: .2s;
    }
  }
}

@keyframes uiSpinnerCubeGrid {
  0%,
  70%,
  100% {
    transform: scale3d(1, 1, 1);
  }

  35% {
    transform: scale3d(0, 0, 1);
  }
}

/* fading-circle */

.ui-spinner__fading-circle-item {
  position: relative;
  width: 40px;
  height: 40px;
  color: inherit;
}

.ui-spinner__fading-circle-item {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  color: inherit;

  &::before {
    display: block;
    width: 15%;
    height: 15%;
    margin: 0 auto;
    content: '';
    animation: uiSpinnerFadingCircle 1.2s infinite ease-in-out both;
    border-radius: 100%;
    background-color: currentColor;
  }

  &--position {
    &--1 {
      transform: rotate(0deg);

      &::before {
        animation-delay: 0s;
      }
    }

    &--2 {
      transform: rotate(30deg);

      &::before {
        animation-delay: -1.1s;
      }
    }

    &--3 {
      transform: rotate(60deg);

      &::before {
        animation-delay: -1s;
      }
    }

    &--4 {
      transform: rotate(90deg);

      &::before {
        animation-delay: -.9s;
      }
    }

    &--5 {
      transform: rotate(120deg);

      &::before {
        animation-delay: -.8s;
      }
    }

    &--6 {
      transform: rotate(150deg);

      &::before {
        animation-delay: -.7s;
      }
    }

    &--7 {
      transform: rotate(180deg);

      &::before {
        animation-delay: -.6s;
      }
    }

    &--8 {
      transform: rotate(210deg);

      &::before {
        animation-delay: -.5s;
      }
    }

    &--9 {
      transform: rotate(240deg);

      &::before {
        animation-delay: -.4s;
      }
    }

    &--10 {
      transform: rotate(270deg);

      &::before {
        animation-delay: -.3s;
      }
    }

    &--11 {
      transform: rotate(300deg);

      &::before {
        animation-delay: -.2s;
      }
    }

    &--12 {
      transform: rotate(330deg);

      &::before {
        animation-delay: -.1s;
      }
    }
  }
}

@keyframes uiSpinnerFadingCircle {
  0%,
  39%,
  100% {
    opacity: 0;
  }

  40% {
    opacity: 1;
  }
}

/* folding-cube */

.ui-spinner__folding-cube {
  position: relative;
  width: 100%;
  height: 100%;
  transform: rotateZ(45deg);
  color: inherit;
}

.ui-spinner__folding-cube-item {
  position: relative;
  display: block;
  float: left;
  width: 50%;
  height: 50%;
  transform: scale(1.1);
  color: inherit;

  &::before {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    content: '';
    transform-origin: 100% 100%;
    animation: uiSpinnerFoldingCube 2.4s infinite linear both;
    background-color: currentColor;
  }

  &--position {
    &--1 {
    }

    &--2 {
      transform: scale(1.1) rotateZ(90deg);

      &::before {
        animation-delay: .4s;
      }
    }

    &--3 {
      transform: scale(1.1) rotateZ(270deg);

      &::before {
        animation-delay: .9s;
      }
    }

    &--4 {
      transform: scale(1.1) rotateZ(180deg);

      &::before {
        animation-delay: .6s;
      }
    }
  }
}

@keyframes uiSpinnerFoldingCube {
  0%,
  10% {
    transform: perspective(140px) rotateX(-180deg);
    opacity: 0;
  }

  25%,
  75% {
    transform: perspective(140px) rotateX(0deg);
    opacity: 1;
  }

  90%,
  100% {
    transform: perspective(140px) rotateY(180deg);
    opacity: 0;
  }
}
```

{% sample lang="tests" %}

### Tests

```JS
import UiSpinner from './ui-spinner';

describe('UiSpinner', () => {
  it('компоненту передан объект опций', () => {
    expect(typeof UiSpinner).toBe('object');
  });
});

```

{% endmethod %}

{% endblock %}
