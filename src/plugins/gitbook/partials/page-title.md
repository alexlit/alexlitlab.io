{% extends '../_layouts/custom.htm' %}

[//]: HEADER

{% block header %}

# PageTitle

{% endblock %}

[//]: COMPONENT

{% block component %}page-title{% endblock %}

[//]: API

{% block api %}

API отсутствует.

{% endblock %}

[//]: ASSETS

{% block assets %}

{% method %}

### Пример использования

```HTML
<page-title inline-template v-cloak>
  <header class="page-title">
    <div class="page-title__content-wrap">
      <h1 class="page-title__heading">Каталог спецтехники</h1>
      <div class="page-title__caption">Компания «Винтел» предоставляет услуги по аренде строительной спецтехники в Москве и МО. На сайте подробно описаны возможности ТС и оборудования.</div>
    </div>
  </header>
</page-title>
```

{% sample lang='template' %}

### Template

```TWIG
{% macro PageTitle(
  heading = 'Каталог спецтехники',
  caption = 'Компания «Винтел» предоставляет услуги по аренде строительной спецтехники в Москве и МО. На сайте подробно описаны возможности ТС и оборудования.'
) %}
<!-- page-title -->
<page-title inline-template v-cloak>
  <header class="page-title">
    <div class="page-title__content-wrap">
      <h1 class="page-title__heading">{{ heading }}</h1>

      {% if caption %}

        <div class="page-title__caption">{{ caption | safe }}</div>

      {% endif %}

    </div>
  </header>
</page-title>
<!-- /page-title -->
{% endmacro %}
```

{% sample lang='script' %}

### Script

```JS
import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|-------------------------------------------------------------------------------
| Регистрация состояния компонента
|-------------------------------------------------------------------------------
*/
store.registerModule('PageTitle', {
  namespaced: true,

  state() {
    return {};
  },

  getters: {},

  mutations: {}
});

/*
|-------------------------------------------------------------------------------
| Объявление объекта опций компонента
|-------------------------------------------------------------------------------
*/
const PageTitle = {
  computed: {
    ...mapState('PageTitle', []),

    ...mapGetters('PageTitle', [])
  },

  methods: {
    ...mapMutations('PageTitle', [])
  }
};

Vue.component('PageTitle', PageTitle);

export default PageTitle;
```

{% sample lang='style' %}

### Style

```SCSS
.page-title {
  position: relative;
  width: 100%;
  padding: 0 10px;

  & + .main-category {
    margin-top: -30px;

    @media screen and (--mobile) {
      margin-top: 0;
    }

    & .main-category__content-wrap {
      padding-top: 0;
      border: none;
    }
  }

  & + .faq-answers {
    margin-top: -10px;
  }
}

.page-title__content-wrap {
  position: relative;
  width: 100%;
  max-width: 940px;
  margin: 0 auto;
  padding-top: 30px;

  @media screen and (--mobile) {
    padding-top: 20px;
  }
}

.page-title__heading {
  margin-bottom: 50px;
  color: #000;
  font-size: 42px;
  font-weight: 700;
  line-height: 48px;

  @media screen and (--mobile) {
    margin-bottom: 30px;
    font-size: 36px;
    font-weight: 700;
    line-height: 42px;
  }
}

.page-title__caption {
  max-width: 780px;
  margin-bottom: 60px;
  color: #000;
  font-size: 18px;
  font-weight: 300;
  line-height: 28px;

  @media screen and (--mobile) {
    margin-bottom: 30px;
    font-size: 16px;
    line-height: 24px;
  }
}

```

{% sample lang='test' %}

### Tests

```JS
import PageTitle from './page-title';

describe('PageTitle', () => {
  it('компоненту передан объект опций', () => {
    expect(typeof PageTitle).toBe('object');
  });
});
```

{% endmethod %}

{% endblock %}