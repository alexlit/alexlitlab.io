{% extends '../_layouts/custom.htm' %}

[//]: HEADER

{% block header %}

# ProductSpec

{% endblock %}

[//]: COMPONENT

{% block component %}product-spec{% endblock %}

[//]: API

{% block api %}

API отсутствует.

{% endblock %}

[//]: ASSETS

{% block assets %}

{% method %}

### Пример использования

```HTML
<product-spec inline-template v-cloak>
  <section class="product-spec">
    <div class="product-spec__content-wrap">
      <h3 class="product-spec__title">Технические характеристики:</h3>
      <table class="product-spec__table">
        <tbody>
          <tr>
            <th>Классификация</th>
            <td>Экскаватор-погрузчик/ колесный</td>
          </tr>
          <tr>
            <th>Глубина копания</th>
            <td>5,53</td>
          </tr>
          <tr>
            <th>Объем ковша, м3</th>
            <td>1,1-04</td>
          </tr>
          <tr>
            <th>Доп. гидромолот</th>
            <td>да</td>
          </tr>
          <tr>
            <th>Масса, кг</th>
            <td>8860</td>
          </tr>
        </tbody>
      </table>
    </div>
  </section>
</product-spec>
```

{% sample lang='template' %}

### Template

```TWIG
{% macro ProductSpec() %}
<!-- product-spec -->
<product-spec inline-template v-cloak>
  <section class="product-spec">
    <div class="product-spec__content-wrap">
      <h3 class="product-spec__title">Технические характеристики:</h3>
      <table class="product-spec__table">
        <tbody>
          <tr>
            <th>Классификация</th>
            <td>Экскаватор-погрузчик/ колесный</td>
          </tr>
          <tr>
            <th>Глубина копания</th>
            <td>5,53</td>
          </tr>
          <tr>
            <th>Объем ковша, м3</th>
            <td>1,1-04</td>
          </tr>
          <tr>
            <th>Доп. гидромолот</th>
            <td>да</td>
          </tr>
          <tr>
            <th>Масса, кг</th>
            <td>8860</td>
          </tr>
        </tbody>
      </table>
    </div>
  </section>
</product-spec>
<!-- /product-spec -->
{% endmacro %}
```

{% sample lang='script' %}

### Script

```JS
import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|-------------------------------------------------------------------------------
| Регистрация состояния компонента
|-------------------------------------------------------------------------------
*/
store.registerModule('ProductSpec', {
  namespaced: true,

  state() {
    return {};
  },

  getters: {},

  mutations: {}
});

/*
|-------------------------------------------------------------------------------
| Объявление объекта опций компонента
|-------------------------------------------------------------------------------
*/
const ProductSpec = {
  computed: {
    ...mapState('ProductSpec', []),

    ...mapGetters('ProductSpec', [])
  },

  methods: {
    ...mapMutations('ProductSpec', [])
  }
};

Vue.component('ProductSpec', ProductSpec);

export default ProductSpec;
```

{% sample lang='style' %}

### Style

```SCSS
.product-spec {
  position: relative;
  width: 100%;
}

.product-spec__content-wrap {
  position: relative;
  width: 100%;
  max-width: 940px;
  margin: 0 auto;
}

.product-spec__title {
  margin-bottom: 30px;
  letter-spacing: 1px;
  text-transform: uppercase;
  color: #000;
  font-size: 14px;
  font-weight: 700;
  line-height: 28px;

  @media screen and (--mobile) {
    margin-bottom: 20px;
  }
}

.product-spec__table {
  width: 100%;
  border-collapse: collapse;

  @media screen and (--mobile) {
    display: block;
  }

  & thead,
  & tbody {
    @media screen and (--mobile) {
      display: block;
    }
  }

  & tr {
    @media screen and (--mobile) {
      display: block;
      padding: 10px;

      &:not(:last-child) {
        margin-bottom: 8px;
      }
    }

    &:nth-child(odd) {
      background-color: #f7f8fa;
    }

    &:nth-child(even) {
      background-color: #eff0f0;
    }
  }

  & th {
    padding: 16px 20px;
    text-align: left;
    color: #000;
    font-size: 18px;
    font-weight: 500;
    line-height: 28px;

    @media screen and (--mobile) {
      display: block;
      margin-bottom: 5px;
      padding: 0;
      font-size: 16px;
      line-height: 24px;
    }
  }

  & td {
    padding: 16px 20px;
    color: #000;
    font-size: 18px;
    font-weight: 300;
    line-height: 28px;

    @media screen and (--mobile) {
      display: block;
      padding: 0;
      font-size: 16px;
      line-height: 24px;
    }
  }
}

```

{% sample lang='test' %}

### Tests

```JS
import ProductSpec from './product-spec';

describe('ProductSpec', () => {
  it('компоненту передан объект опций', () => {
    expect(typeof ProductSpec).toBe('object');
  });
});
```

{% endmethod %}

{% endblock %}