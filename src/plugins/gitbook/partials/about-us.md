{% extends '../_layouts/custom.htm' %}

[//]: HEADER

{% block header %}

# AboutUs

{% endblock %}

[//]: COMPONENT

{% block component %}about-us{% endblock %}

[//]: API

{% block api %}

API отсутствует.

{% endblock %}

[//]: ASSETS

{% block assets %}

{% method %}

### Пример использования

```HTML
<about-us inline-template v-cloak>
  <section class="about-us">
    <div class="about-us__content-wrap">
      <article class="about-us__article">
        <div class="about-us__col">
          <div class="about-us__content">
            <p>Пpотопланетное облако прекрасно колеблет математический горизонт. Юпитер, как бы это ни казалось парадоксальным, дает спектральный класс. Фаза перечеркивает космический мусор - это солнечное затмение предсказал ионянам Фалес Милетский. Как мы уже знаем, прямое восхождение многопланово ищет керн. Высота ничтожно оценивает центральный зенит. Исполинская звездная спираль с поперечником в 50 кпк выслеживает Ганимед.</p>
            <p>Вселенная достаточно огромна, чтобы различное расположение вызывает астероидный Южный Треугольник. Тукан колеблет астероидный узел. Хотя хpонологи не увеpены, им кажется, что соединение выбирает непреложный Ганимед. Эклиптика, а там действительно могли быть видны звезды, о чем свидетельствует Фукидид дает годовой параллакс. Широта представляет собой азимут.</p>
          </div>
        </div>
        <aside class="about-us__col">
          <img src="/assets/images/about-us.1.jpg" alt="" class="about-us__image">
        </aside>
      </article>
      <div class="about-us__counts-wrap">
        <figure class="about-us__count">
          <p class="about-us__count-num">200</p>
          <figcaption class="about-us__count-caption">единиц
            <br>техники</figcaption>
        </figure>
        <figure class="about-us__count">
          <p class="about-us__count-num">10</p>
          <figcaption class="about-us__count-caption">лет на рынке</figcaption>
        </figure>
        <figure class="about-us__count">
          <p class="about-us__count-num">24</p>
          <figcaption class="about-us__count-caption">часа в сутки
            <br>принимаем заявки</figcaption>
        </figure>
      </div>
    </div>
  </section>
</about-us>
```

{% sample lang='template' %}

### Template

```TWIG
{% macro AboutUs() %}
<!-- about-us -->
<about-us inline-template v-cloak>
  <section class="about-us">
    <div class="about-us__content-wrap">
      <article class="about-us__article">
        <div class="about-us__col">
          <div class="about-us__content">
            <p>Пpотопланетное облако прекрасно колеблет математический горизонт. Юпитер, как бы это ни казалось парадоксальным, дает спектральный класс. Фаза перечеркивает космический мусор - это солнечное затмение предсказал ионянам Фалес Милетский. Как мы уже знаем, прямое восхождение многопланово ищет керн. Высота ничтожно оценивает центральный зенит. Исполинская звездная спираль с поперечником в 50 кпк выслеживает Ганимед.</p>
            <p>Вселенная достаточно огромна, чтобы различное расположение вызывает астероидный Южный Треугольник. Тукан колеблет астероидный узел. Хотя хpонологи не увеpены, им кажется, что соединение выбирает непреложный Ганимед. Эклиптика, а там действительно могли быть видны звезды, о чем свидетельствует Фукидид дает годовой параллакс. Широта представляет собой азимут.</p>
          </div>
        </div>
        <aside class="about-us__col">
          <img src="/assets/images/about-us.1.jpg" alt="" class="about-us__image">
        </aside>
      </article>
      <div class="about-us__counts-wrap">

        {% for item in [{
            num: '200',
            caption: 'единиц<br>техники'
          }, {
            num: '10',
            caption: 'лет на рынке'
          },{
            num: '24',
            caption: 'часа в сутки<br>принимаем заявки'
          }]
        %}

          <figure class="about-us__count">
            <p class="about-us__count-num">{{ item.num }}</p>
            <figcaption class="about-us__count-caption">{{ item.caption | safe }}</figcaption>
          </figure>

        {% endfor %}

      </div>
    </div>
  </section>
</about-us>
<!-- /about-us -->
{% endmacro %}
```

{% sample lang='script' %}

### Script

```JS
import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|-------------------------------------------------------------------------------
| Регистрация состояния компонента
|-------------------------------------------------------------------------------
*/
store.registerModule('AboutUs', {
  namespaced: true,

  state() {
    return {};
  },

  getters: {},

  mutations: {}
});

/*
|-------------------------------------------------------------------------------
| Объявление объекта опций компонента
|-------------------------------------------------------------------------------
*/
const AboutUs = {
  computed: {
    ...mapState('AboutUs', []),

    ...mapGetters('AboutUs', [])
  },

  methods: {
    ...mapMutations('AboutUs', [])
  }
};

Vue.component('AboutUs', AboutUs);

export default AboutUs;
```

{% sample lang='style' %}

### Style

```SCSS
.about-us {
  position: relative;
  width: 100%;
  padding: 0 10px;
}

.about-us__content-wrap {
  position: relative;
  width: 100%;
  max-width: 940px;
  margin: 0 auto;
  padding-bottom: 50px;
}

.about-us__article {
  display: flex;
  justify-content: space-between;
  width: 100%;
  margin-bottom: 50px;

  @media screen and (--mobile) {
    margin-bottom: 20px;
  }

  @media screen and (--mobile) {
    display: block;
  }
}

.about-us__col {
  position: relative;
  width: calc(50% - 20px);

  @media screen and (--mobile) {
    width: 100%;
  }
}

.about-us__content {
  @media screen and (--mobile) {
    margin-bottom: 20px;
  }

  & p {
    margin-bottom: 30px;
    color: #000;
    font-size: 18px;
    font-weight: 300;
    line-height: 28px;

    @media screen and (--mobile) {
      margin-bottom: 20px;
      font-size: 16px;
      line-height: 24px;
    }
  }

  & > *:last-child {
    margin-bottom: 0;
  }
}

.about-us__image {
  display: block;
  max-width: 100%;
  box-shadow: 20px 20px 0 0 #e8e8e8;

  @media screen and (--mobile) {
    box-shadow: none;
  }
}

.about-us__counts-wrap {
  display: flex;
  justify-content: space-between;
  width: 100%;

  @media screen and (--mobile) {
    display: block;
  }
}

.about-us__count {
  width: calc((100% - 30px * 2) / 3);

  @media screen and (--mobile) {
    width: 100%;

    &:not(:last-child) {
      margin-bottom: 20px;
    }
  }
}

.about-us__count-num {
  letter-spacing: -3px;
  color: #000;
  font-size: 144px;
  font-weight: 700;
}

.about-us__count-caption {
  letter-spacing: 1px;
  text-transform: uppercase;
  color: #000;
  font-size: 20px;
  font-weight: 700;
  line-height: 28px;
}

```

{% sample lang='test' %}

### Tests

```JS
import AboutUs from './about-us';

describe('AboutUs', () => {
  it('компоненту передан объект опций', () => {
    expect(typeof AboutUs).toBe('object');
  });
});
```

{% endmethod %}

{% endblock %}