{% extends '../_layouts/custom.htm' %}

[//]: HEADER

{% block header %}

# AppMenu

{% endblock %}

[//]: COMPONENT

{% block component %}app-menu{% endblock %}

[//]: API

{% block api %}

API отсутствует.

{% endblock %}

[//]: ASSETS

{% block assets %}

{% method %}

### Пример использования

```HTML
<app-menu inline-template v-cloak>
  <nav class="app-menu">
    <div class="app-menu__content-wrap"><a :class="{ 'app-menu__item': true, 'app-menu__item--active': false }" href="#">Автокраны</a> <a :class="{ 'app-menu__item': true, 'app-menu__item--active': false }" href="#">Самосвалы</a> <a :class="{ 'app-menu__item': true, 'app-menu__item--active': false }" href="#">Экскаваторы</a> <a :class="{ 'app-menu__item': true, 'app-menu__item--active': false }" href="#">Катки</a> <a :class="{ 'app-menu__item': true, 'app-menu__item--active': false }" href="#">Манипуляторы</a> <a :class="{ 'app-menu__item': true, 'app-menu__item--active': false }" href="#">Бульдозеры</a> <a :class="{ 'app-menu__item': true, 'app-menu__item--active': false }" href="#">Тралы</a> <a :class="{ 'app-menu__item': true, 'app-menu__item--active': false }" href="#">Погрузчики</a> <a class="app-menu__readmore" href="#" v-if="$mq !== 'mobile'">Вся техника</a> <a :class="{ 'app-menu__item': true, 'app-menu__item--active': false }" href="#" v-if="$mq === 'mobile'">Борт. полуприцепы</a> <a :class="{ 'app-menu__item': true, 'app-menu__item--active': false }" href="#" v-if="$mq === 'mobile'">ФРЕЗА</a> <a :class="{ 'app-menu__item': true, 'app-menu__item--active': false }" href="#" v-if="$mq === 'mobile'">Бетононасос</a> <a :class="{ 'app-menu__item': true, 'app-menu__item--active': false }" href="#" v-if="$mq === 'mobile'">Бетономешалки</a> <a :class="{ 'app-menu__item': true, 'app-menu__item--active': false }" href="#" v-if="$mq === 'mobile'">Асфальтоукладчики</a></div>
  </nav>
</app-menu>
```

{% sample lang='template' %}

### Template

```TWIG
{% macro AppMenu() %}
<!-- app-menu -->
<app-menu inline-template v-cloak>
  <nav class="app-menu">
    <div class="app-menu__content-wrap">
      <a :class="{ 'app-menu__item': true, 'app-menu__item--active': false }" href="#">Автокраны</a>
      <a :class="{ 'app-menu__item': true, 'app-menu__item--active': false }" href="#">Самосвалы</a>
      <a :class="{ 'app-menu__item': true, 'app-menu__item--active': false }" href="#">Экскаваторы</a>
      <a :class="{ 'app-menu__item': true, 'app-menu__item--active': false }" href="#">Катки</a>
      <a :class="{ 'app-menu__item': true, 'app-menu__item--active': false }" href="#">Манипуляторы</a>
      <a :class="{ 'app-menu__item': true, 'app-menu__item--active': false }" href="#">Бульдозеры</a>
      <a :class="{ 'app-menu__item': true, 'app-menu__item--active': false }" href="#">Тралы</a>
      <a :class="{ 'app-menu__item': true, 'app-menu__item--active': false }" href="#">Погрузчики</a>
      <a class="app-menu__readmore" href="#" v-if="$mq !== 'mobile'">Вся техника</a>
      <a :class="{ 'app-menu__item': true, 'app-menu__item--active': false }" href="#" v-if="$mq === 'mobile'">Борт. полуприцепы</a>
      <a :class="{ 'app-menu__item': true, 'app-menu__item--active': false }" href="#" v-if="$mq === 'mobile'">ФРЕЗА</a>
      <a :class="{ 'app-menu__item': true, 'app-menu__item--active': false }" href="#" v-if="$mq === 'mobile'">Бетононасос</a>
      <a :class="{ 'app-menu__item': true, 'app-menu__item--active': false }" href="#" v-if="$mq === 'mobile'">Бетономешалки</a>
      <a :class="{ 'app-menu__item': true, 'app-menu__item--active': false }" href="#" v-if="$mq === 'mobile'">Асфальтоукладчики</a>
    </div>
  </nav>
</app-menu>
<!-- /app-menu -->
{% endmacro %}


















```

{% sample lang='script' %}

### Script

```JS
import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|-------------------------------------------------------------------------------
| Регистрация состояния компонента
|-------------------------------------------------------------------------------
*/
store.registerModule('AppMenu', {
  namespaced: true,

  state() {
    return {};
  },

  getters: {},

  mutations: {}
});

/*
|-------------------------------------------------------------------------------
| Объявление объекта опций компонента
|-------------------------------------------------------------------------------
*/
const AppMenu = {
  computed: {
    ...mapState('AppMenu', []),

    ...mapGetters('AppMenu', [])
  },

  methods: {
    ...mapMutations('AppMenu', [])
  }
};

Vue.component('AppMenu', AppMenu);

export default AppMenu;
```

{% sample lang='style' %}

### Style

```SCSS
.app-menu {
  position: relative;
  width: 100%;
  padding: 0 10px;
}

.app-menu__content-wrap {
  position: relative;
  display: flex;
  justify-content: space-between;
  width: 100%;
  max-width: 940px;
  margin: 0 auto;
}

.app-menu__item {
  display: block;
  letter-spacing: .86px;
  text-transform: uppercase;
  color: #fff;
  font-size: 12px;
  font-weight: 500;
  line-height: 74px;

  &--active,
  &:hover {
    color: #fc0;
  }
}

.app-menu__readmore {
  display: block;
  align-self: center;
  padding: 0 8px 0 41px;
  letter-spacing: .86px;
  text-transform: uppercase;
  color: #fff;
  border: 1px solid currentColor;
  border-radius: 4px;
  background-image: url('../images/app-menu.readmore.svg');
  background-repeat: no-repeat;
  background-position: 8px center;
  font-size: 12px;
  font-weight: 500;
  line-height: 40px;

  &:hover {
    color: #fc0;
    background-image: url('../images/app-menu.readmore@active.svg');
  }
}

```

{% sample lang='test' %}

### Tests

```JS
import AppMenu from './app-menu';

describe('AppMenu', () => {
  it('компоненту передан объект опций', () => {
    expect(typeof AppMenu).toBe('object');
  });
});
```

{% endmethod %}

{% endblock %}