{% extends '../\_layouts/custom.htm' %}

[//]: HEADER

{% block header %}

# FormOrder

{% endblock %}

[//]: COMPONENT

{% block component %}form-order{% endblock %}

[//]: API

{% block api %}

API отсутствует.

{% endblock %}

[//]: ASSETS

{% block assets %}

{% method %}

### Пример использования

```HTML
<form-order action="/assets/forms/form-order.php" inline-template v-cloak>
  <form class="form-order" @submit.prevent="validateBeforeSubmit(scope);" :data-vv-scope="scope" :action="action" novalidate>
    <div class="form-order__content-wrap">
      <transition enter-active-class="animated fadeIn" leave-active-class="animated fadeOut" duration="300" mode="out-in">
        <header class="form-order__heading">
          <div class="form-order__image-wrap" v-if="!success && $mq === 'mobile' || $mq !== 'mobile'">
            <img class="form-order__image" :src="formData.IMAGE" :alt="formData.TITLE">
          </div>
          <div class="form-order__heading-text">
            <p class="form-order__suptitle">заказ спецтехники</p>
            <p class="form-order__title">[[ formData.TITLE ]]</p>
          </div>
        </header>
      </transition>
      <transition enter-active-class="animated fadeIn" leave-active-class="animated fadeOut" duration="300" mode="out-in">
        <div class="form-order__fields-wrap" v-if="!success">
          <div class="form-order__fields-wrap-row">
            <div class="form-order__fields-wrap-col">
              <label for="NAME" class="form-order__label">Имя*:</label>
              <input :class="{'form-order__field': true,'shake animated': shake && errors.has(scope + '.NAME')}" type="text" name="NAME" id="NAME" v-model="formData.NAME" v-validate.disable="'required'" :data-invalid="errors.has(scope + '.NAME')">
              <transition enter-active-class="animated fadeIn" leave-active-class="animated fadeOut" mode="out-in">
                <p class="form-order__policy-error form-order__policy-error--absolute" v-if="errors.has(scope + '.NAME')">Вы не ввели имя</p>
              </transition>
            </div>
            <div class="form-order__fields-wrap-col">
              <label for="ADDRESS" class="form-order__label">Адрес:</label>
              <input class="form-order__field" type="text" name="ADDRESS" id="ADDRESS" v-model="formData.ADDRESS">
            </div>
          </div>
          <div class="form-order__fields-wrap-row">
            <div class="form-order__fields-wrap-col">
              <label for="DATE" class="form-order__label">Дата:</label>
              <input class="form-order__field" type="date" name="DATE" id="DATE" v-model="formData.DATE">
            </div>
            <div class="form-order__fields-wrap-col">
              <label for="PHONE" class="form-order__label">Телефон*:</label>
              <input :class="{'form-order__field': true,'shake animated': shake && errors.has(scope + '.PHONE')}" v-model="formData.PHONE" data-inputmask="'mask': '+7 (999) 999-99-99'" placeholder="+7 (___) ___-__-__" name="PHONE" id="PHONE" type="tel" @focus="inputmask();" v-validate.disable="{rules: {required: true,regex: /^\+7\s?\([0-9]{3}\)\s?[0-9]{3}-?[0-9]{2}-?[0-9]{2}$/}}" :data-invalid="errors.has(scope + '.PHONE')">
              <transition enter-active-class="animated fadeIn" leave-active-class="animated fadeOut" mode="out-in">
                <p class="form-order__policy-error form-order__policy-error--absolute" v-if="errors.has(scope + '.PHONE')">Вы не ввели номер телефона</p>
              </transition>
            </div>
          </div>
          <div class="form-order__fields-wrap-row">
            <div class="form-order__fields-wrap-col">
              <label for="MESSAGE" class="form-order__label">Комментарий:</label>
              <textarea class="form-order__textarea" name="MESSAGE" id="MESSAGE" v-model="formData.MESSAGE" placeholder="Укажите какие-либо особенности заказа"></textarea>
              <transition enter-active-class="animated fadeIn" leave-active-class="animated fadeOut" mode="out-in">
                <p class="form-order__policy-error form-order__policy-error--absolute" v-if="!agree && tries > 0">Вы не согласились с условиями обработки данных</p>
              </transition>
            </div>
          </div>
          <div class="form-order__fields-wrap-row">
            <el-checkbox class="form-order__el-checkbox" v-model="agree" v-validate.disable="'required'">Я согласен на обработку персональных данных и с условиями <a href="#" target="_blank">политики конфиденциальности</a></el-checkbox>
          </div>
          <footer class="form-order__buttons-wrap">
            <button class="form-order__button" type="send" @submit.prevent="validateBeforeSubmit(scope);">ОТПРАВИТЬ</button>
          </footer>
        </div>
        <figure class="form-order__success" v-else>
          <message-success inline-template v-cloak>
            <section class="message-success">
              <div class="message-success__content-wrap">
                <header class="message-success__title">Ваш заказ оформлен!</header>
                <p class="message-success__caption">Мы свяжемся с вами в самое ближайшее время для уточнения деталей заказа.</p>
              </div>
            </section>
          </message-success>
        </figure>
      </transition>
    </div>
  </form>
</form-order>
```

{% sample lang='template' %}

### Template

```TWIG
{% from 'components/message-success/message-success.htm' import MessageSuccess %}

{% macro FormOrder() %}
<!-- form-order -->
<form-order
  action="/assets/forms/form-order.php"
  inline-template
  v-cloak
>
  <form
    class="form-order"
    @submit.prevent="validateBeforeSubmit(scope);"
    :data-vv-scope="scope"
    :action="action"
    novalidate
  >
    <div class="form-order__content-wrap">
      <transition
        enter-active-class="animated fadeIn"
        leave-active-class="animated fadeOut"
        duration="300"
        mode="out-in"
      >
        <header class="form-order__heading">
          <div
            class="form-order__image-wrap"
            v-if="!success && $mq === 'mobile' || $mq !== 'mobile'"
          >
            <img
              class="form-order__image"
              :src="formData.IMAGE"
              :alt="formData.TITLE"
            >
          </div>
          <div class="form-order__heading-text">
            <p class="form-order__suptitle">заказ спецтехники</p>
            <p class="form-order__title">[[ formData.TITLE ]]</p>
          </div>
        </header>
      </transition>
      <transition
        enter-active-class="animated fadeIn"
        leave-active-class="animated fadeOut"
        duration="300"
        mode="out-in"
      >

        {# fields wrap #}
        <div class="form-order__fields-wrap" v-if="!success">
          <div class="form-order__fields-wrap-row">
            <div class="form-order__fields-wrap-col">
              <label for="NAME" class="form-order__label">Имя*:</label>
              <input
                :class="{
                  'form-order__field': true,
                  'shake animated': shake && errors.has(scope + '.NAME')
                }"
                type="text"
                name="NAME"
                id="NAME"
                v-model="formData.NAME"
                v-validate.disable="'required'"
                :data-invalid="errors.has(scope + '.NAME')"
              >
              <transition
                enter-active-class="animated fadeIn"
                leave-active-class="animated fadeOut"
                mode="out-in"
              >
                <p
                  class="form-order__policy-error form-order__policy-error--absolute"
                  v-if="errors.has(scope + '.NAME')"
                >Вы не ввели имя</p>
              </transition>
            </div>
            <div class="form-order__fields-wrap-col">
              <label for="ADDRESS" class="form-order__label">Адрес:</label>
              <input
                class="form-order__field"
                type="text"
                name="ADDRESS"
                id="ADDRESS"
                v-model="formData.ADDRESS"
              >
            </div>
          </div>
          <div class="form-order__fields-wrap-row">
            <div class="form-order__fields-wrap-col">
              <label for="DATE" class="form-order__label">Дата:</label>
              <input
                class="form-order__field"
                type="date"
                name="DATE"
                id="DATE"
                v-model="formData.DATE"
              >
            </div>
            <div class="form-order__fields-wrap-col">
              <label for="PHONE" class="form-order__label">Телефон*:</label>
              <input
                :class="{
                  'form-order__field': true,
                  'shake animated': shake && errors.has(scope + '.PHONE')
                }"
                v-model="formData.PHONE"
                data-inputmask="'mask': '+7 (999) 999-99-99'"
                placeholder="+7 (___) ___-__-__"
                name="PHONE"
                id="PHONE"
                type="tel"
                @focus="inputmask();"
                v-validate.disable="{
                  rules: {
                    required: true,
                    regex: /^\+7\s?\([0-9]{3}\)\s?[0-9]{3}-?[0-9]{2}-?[0-9]{2}$/
                  }
                }"
                :data-invalid="errors.has(scope + '.PHONE')"
              >
              <transition
                enter-active-class="animated fadeIn"
                leave-active-class="animated fadeOut"
                mode="out-in"
              >
                <p
                  class="form-order__policy-error form-order__policy-error--absolute"
                  v-if="errors.has(scope + '.PHONE')"
                >Вы не ввели номер телефона</p>
              </transition>
            </div>
          </div>
          <div class="form-order__fields-wrap-row">
            <div class="form-order__fields-wrap-col">
              <label for="MESSAGE" class="form-order__label">Комментарий:</label>
              <textarea
                class="form-order__textarea"
                name="MESSAGE"
                id="MESSAGE"
                v-model="formData.MESSAGE"
                placeholder="Укажите какие-либо особенности заказа"
              ></textarea>
              <transition
                enter-active-class="animated fadeIn"
                leave-active-class="animated fadeOut"
                mode="out-in"
              >
                <p
                  class="form-order__policy-error form-order__policy-error--absolute"
                  v-if="!agree && tries > 0"
                >Вы не согласились с условиями обработки данных</p>
              </transition>
            </div>
          </div>
          <div class="form-order__fields-wrap-row">
            <el-checkbox
              class="form-order__el-checkbox"
              v-model="agree"
              v-validate.disable="'required'"
            >Я согласен на обработку персональных данных и с условиями <a href="#" target="_blank">политики конфиденциальности</a></el-checkbox>
          </div>
          <footer class="form-order__buttons-wrap">
            <button
              class="form-order__button"
              type="send"
              @submit.prevent="validateBeforeSubmit(scope);"
            >ОТПРАВИТЬ</button>
          </footer>
        </div>
        {# /fields wrap #}

        {# success #}
        <figure class="form-order__success" v-else>

          {{ MessageSuccess() }}

        </figure>
        {# /success #}

      </transition>
    </div>
  </form>
</form-order>

<!-- /form-order -->
{% endmacro %}
```

{% sample lang='script' %}

### Script

```JS
import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import 'animate.css/animate.css';
import axios from 'axios';
import qs from 'qs';
import Inputmask from 'inputmask/dist/inputmask/inputmask';
import VeeValidate from 'vee-validate';
import store from '../app/app.store';
import './form-order.php';

Vue.use(VeeValidate, {
  inject: false
});

Vue.config.productionTip = false;

/*
|-------------------------------------------------------------------------------
| Регистрация состояния компонента
|-------------------------------------------------------------------------------
*/
store.registerModule('FormOrder', {
  namespaced: true,

  state() {
    return {
      useUrlEncode: true,
      success: false,
      shake: false,
      tries: 0,
      formData: {}
    };
  },

  getters: {},

  mutations: {
    /**
     * Установка начальных значений формы
     * @param {*} state
     * @param {Object} payload - требуемые значения
     */
    setFormData(state, payload) {
      state.formData = { ...state.formData, ...payload };
    },

    /**
     * Счетчик попыток отправить форму
     * @param {*} state
     */
    incrementTries(state) {
      state.tries += 1;
      state.shake = true;
      setTimeout(() => {
        state.shake = false;
      }, 1000);
    },

    /**
     * Установка состояния "успешная отправка"
     * @param {*} state
     * @param {Boolean} payload
     */
    setSuccessState(state, payload) {
      state.success = payload;
    }
  }
});

/*
|-------------------------------------------------------------------------------
| Объявление объекта опций компонента
|-------------------------------------------------------------------------------
*/
const FormOrder = {
  data() {
    return {
      agree: false,
      scope: Math.random()
        .toString(36)
        .substring(2)
    };
  },

  props: {
    action: {
      type: String,
      default: '/assets/forms/form-order.php'
    }
  },

  computed: {
    ...mapState('FormOrder', [
      'useUrlEncode',
      'success',
      'shake',
      'tries',
      'formData'
    ]),

    ...mapGetters('FormOrder', [])
  },

  inject: {
    $validator: '$validator'
  },

  methods: {
    ...mapMutations('FormOrder', ['incrementTries', 'setSuccessState']),

    /**
     * Валидация формы и отправка данных
     */
    validateBeforeSubmit(scope) {
      const vm = this;

      vm.$validator
        .validateAll(scope)
        .then(response => {
          if (response && vm.agree) {
            const post = vm.useUrlEncode
              ? qs.stringify(vm.formData)
              : vm.formData;

            axios
              .post(vm.action, post)
              .then(serverResponse => {
                vm.setSuccessState(true);
                console.info(`Отправлены данные:`);
                console.info(post);
                console.info(`Ответ сервера:`);
                console.info(serverResponse);
              })
              .catch(error => {
                console.error(`Ошибка сервера:`);
                console.info(error);
              });
          } else {
            vm.incrementTries();
          }
        })
        .catch(error => {
          console.log(error);
        });
    },

    /**
     * Подключение маски
     */
    inputmask() {
      const vm = this;
      Inputmask({ showMaskOnHover: false }).mask(
        vm.$el.querySelectorAll('[data-inputmask]')
      );
    }
  }
};

Vue.component('FormOrder', FormOrder);

export default FormOrder;

```

{% sample lang='style' %}

### Style

```SCSS
.form-order {
  position: relative;
  width: 100%;
}

.form-order__content-wrap {
  position: relative;
  width: 100%;
  max-width: 940px;
  margin: 0 auto;
}

.form-order__heading {
  display: flex;
  justify-content: space-between;
  width: 100%;
  margin-bottom: 20px;

  @media screen and (--mobile) {
    flex-direction: column;
  }
}

.form-order__image-wrap {
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 80px;
  height: 80px;
  margin-top: 10px;
  border: 1px solid #e8e8e8;
  background-color: #fff;

  @media screen and (--mobile) {
    order: 2;
    width: 100%;
    height: 300px;
  }
}

.form-order__image {
  display: block;
  max-width: 100%;
  max-height: 100%;
}

.form-order__heading-text {
  width: calc(100% - 100px);

  @media screen and (--mobile) {
    order: 1;
    width: 100%;
  }
}

.form-order__suptitle {
  margin-bottom: 10px;
  letter-spacing: 1px;
  text-transform: uppercase;
  color: #000;
  font-size: 14px;
  font-weight: 700;
  line-height: 28px;
}

.form-order__title {
  color: #000;
  font-size: 36px;
  font-weight: 700;
  line-height: 42px;

  @media screen and (--mobile) {
    margin-bottom: 20px;
  }
}

.form-order__fields-wrap {
  position: relative;
  width: 100%;
}

.form-order__fields-wrap-row {
  position: relative;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  width: 100%;

  @media screen and (--mobile) {
    display: block;
  }

  &:not(:last-of-type) {
    margin-bottom: 30px;

    @media screen and (--mobile) {
      margin-bottom: 20px;
    }
  }
}

.form-order__fields-wrap-col {
  position: relative;
  width: calc(50% - 10px);

  @media screen and (--mobile) {
    width: 100%;
    margin-bottom: 20px;
  }

  &:only-child {
    width: 100%;
  }
}

.form-order__label {
  display: inline-block;
  margin-bottom: 10px;
  cursor: pointer;
  letter-spacing: 1px;
  text-transform: uppercase;
  color: #000;
  font-size: 12px;
  font-weight: 500;
  line-height: 18px;
}

.form-order__field {
  width: 100%;
  height: 80px;
  padding: 0 20px;
  color: #000;
  border-bottom: 3px solid #fc0;
  background-color: #f5f5f5;
  font-size: 30px;
  font-weight: 300;

  &[type='date'] {
    cursor: pointer;

    &::-webkit-inner-spin-button {
      opacity: 0;
    }

    &::-webkit-calendar-picker-indicator {
      position: absolute;
      top: 0;
      left: 0;
      box-sizing: border-box;
      width: 100%;
      height: 100%;
      cursor: pointer;
      opacity: 0;
    }

    &::-webkit-clear-button {
      opacity: 0;
    }
  }
}

.form-order__textarea {
  width: 100%;
  height: 150px;
  padding: 20px;
  color: #000;
  border-bottom: 3px solid #fc0;
  background-color: #f5f5f5;
  font-size: 18px;
  font-weight: 300;
  line-height: 28px;

  @media screen and (--mobile) {
    height: 100px;
  }

  &::placeholder {
    color: #000;
  }
}

.form-order__policy-error {
  width: 100%;
  padding-bottom: 5px;
  animation-duration: .4s;
  color: #ff3b3b;
  font-size: 18px;
  font-weight: 400;
  line-height: 24px;

  &--absolute {
    position: absolute;
    top: 100%;
    left: 0;
    padding-top: 5px;
    padding-bottom: 0;

    @media screen and (--mobile) {
      position: relative;
    }
  }
}

.form-order__el-checkbox {
  &.el-checkbox {
    max-width: 480px;
  }
}

.form-order__buttons-wrap {
  width: 100%;
  padding-top: 20px;
}

.form-order__button {
  padding: 18px 21px;
  color: #000;
  border-radius: 2px;
  background-color: #fc0;
  font-size: 20px;
  font-weight: 700;

  &:hover {
    background-color: #ffe500;
  }
}

.form-order__success {
  position: relative;
  width: 100%;
  margin-top: 50px;
}

```

{% sample lang='test' %}

### Tests

```JS
import FormOrder from './form-order';

describe('FormOrder', () => {
  it('компоненту передан объект опций', () => {
    expect(typeof FormOrder).toBe('object');
  });
});
```

{% sample lang='serverside' %}

### ServerSide

```php
<?php

require __DIR__ . '/../../plugins/PHPMailer/src/PHPMailer.php';
require __DIR__ . '/../../plugins/PHPMailer/src/Exception.php';

use PHPMailer\PHPMailer\PHPMailer;

// Получаем данные в зависимости от типа
$json = json_decode(file_get_contents('php://input'), TRUE);
$post = $json ? $json : $_POST;

// Инициализируем объект
$mail = new PHPMailer();
$mail->CharSet = 'utf-8';
$mail->setFrom('noreply@vintel.ru','Винтел | Спецтехника');
$mail->addAddress('ljazzmail@gmail.com');
$mail->addAttachment(__DIR__.'/../../'.$post['IMAGE']);
$mail->isHTML(TRUE);
$mail->Subject = 'Аренда: '.$post['TITLE'];
$mail->Body = '
<html>
  <head>
    <title>'.$mail->Subject.'</title>
  </head>
  <body>
    <p>Имя:'.$post['NAME'].'</p>
    <p>Адрес:'.$post['ADDRESS'].'</p>
    <p>Дата:'.$post['DATE'].'</p>
    <p>Телефон:'.$post['PHONE'].'</p>
    <p>Комментарий:'.$post['MESSAGE'].'</p>
  </body>
</html>';
$mail->send();

// Запись данных в файл для дебага
$fileData = fopen(basename(__FILE__, '.php') . '.response.md', 'w');

fputs($fileData, print_r("# MESSAGE:\r\n\r\n", TRUE));
fputs($fileData, print_r("```html\r\n", TRUE));
fputs($fileData, print_r($mail->Body, TRUE));
fputs($fileData, print_r("\r\n```\r\n\r\n", TRUE));

fputs($fileData, print_r("## POST:\r\n\r\n", TRUE));
fputs($fileData, print_r("```php\r\n", TRUE));
fputs($fileData, print_r($post, TRUE));
fputs($fileData, print_r("```\r\n\r\n", TRUE));

fputs($fileData, print_r("## FILES:\r\n\r\n", TRUE));
fputs($fileData, print_r("```php\r\n", TRUE));
fputs($fileData, print_r($_FILES, TRUE));
fputs($fileData, print_r("```\r\n\r\n", TRUE));

fputs($fileData, print_r("### REQUEST:\r\n\r\n", TRUE));
fputs($fileData, print_r("```php\r\n", TRUE));
fputs($fileData, print_r($_REQUEST, TRUE));
fputs($fileData, print_r("```\r\n\r\n", TRUE));

fclose($fileData);

// Возвращаем ответ
var_dump($post);

?>

```

{% endmethod %}

{% endblock %}
