{% extends "../\_layouts/custom.htm" %}

[//]: HEADER

{% block header %}

# StyleguidePages

Список страниц в стайлгайде. Генерируется при запуске стайлгайд-таска.

{% endblock %}

[//]: COMPONENT

{% block component %}styleguide-pages{% endblock %}

[//]: API

{% block api %}

API отсутствует.

{% endblock %}

[//]: ASSETS

{% block assets %}

{% method %}

### Пример использования

Используется только в шаблоне генератора документации.

{% sample lang='template' %}

### Template

```TWIG
{% macro StyleguidePages() %}
<!-- styleguide-pages -->
<styleguide-pages inline-template v-cloak>
  <ul class="styleguide-pages">
    <li
      class="styleguide-pages__item"
      v-for="value, key in $store.state.StyleguidePages.pages"
    >
      <a
        class="styleguide-pages__item-link"
        :href="'/' + key"
        target="_blank"
      >[[ value ]]</a>
    </li>
  </ul>
</styleguide-pages>
<!-- /styleguide-pages -->
{% endmacro %}
```

{% sample lang='script' %}

### Script

```JS
import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|------------------------------------------------------------------------------
| Регистрация состояния компонента
|------------------------------------------------------------------------------
*/
store.registerModule('StyleguidePages', {
  namespaced: true,

  state() {
    return {
      pages: process.env.STYLEGUIDE.pagesObject
    };
  },

  getters: {},

  mutations: {}
});

/*
|------------------------------------------------------------------------------
| Объявление объекта опций компонента
|------------------------------------------------------------------------------
*/
const StyleguidePages = {
  computed: {
    ...mapState('StyleguidePages', ['pages']),

    ...mapGetters('StyleguidePages', [])
  },

  methods: {
    ...mapMutations('StyleguidePages', [])
  }
};

Vue.component('StyleguidePages', StyleguidePages);

export default StyleguidePages;

```

{% sample lang='style' %}

### Style

```SCSS
.styleguide-pages {
  position: relative;
  width: 100%;
  padding-left: 20px;
  list-style: none;
}

.styleguide-pages__item {
  position: relative;
}

.styleguide-pages__item-link {
  position: relative;
  display: block;
  overflow: hidden;
  padding: 10px 15px;
  white-space: nowrap;
  text-overflow: ellipsis;
  color: #364149;
  border-bottom: none;
  background: 0 0;
  font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 14px;

  &:hover {
    text-decoration: underline;
  }
}
```

{% endmethod %}

{% endblock %}
