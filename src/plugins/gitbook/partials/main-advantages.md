{% extends '../_layouts/custom.htm' %}

[//]: HEADER

{% block header %}

# MainAdvantages

{% endblock %}

[//]: COMPONENT

{% block component %}main-advantages{% endblock %}

[//]: API

{% block api %}

API отсутствует.

{% endblock %}

[//]: ASSETS

{% block assets %}

{% method %}

### Пример использования

```HTML
<main-advantages inline-template v-cloak>
  <section class="main-advantages">
    <div class="main-advantages__content-wrap">
      <figure class="main-advantages__item" v-for="item in [{image: '/assets/images/main-advantages.1.svg',caption: 'Выгодные цены'},{image: '/assets/images/main-advantages.2.svg',caption: 'Спец. условия для постоянных клиентов'},{image: '/assets/images/main-advantages.3.svg',caption: 'Возможность привлечения сторонней техники'},{image: '/assets/images/main-advantages.4.svg',caption: 'Пропуск МКАД, ТТК, центр Москвы'},{image: '/assets/images/main-advantages.5.svg',caption: 'Круглосуточный прием заявок'},{image: '/assets/images/main-advantages.6.svg',caption: 'Опытные водители'}]">
        <div class="main-advantages__item-image" :style="{ backgroundImage: 'url(' + item.image + ')' }"></div>
        <figcaption class="main-advantages__item-caption">[[ item.caption ]]</figcaption>
      </figure>
    </div>
  </section>
</main-advantages>
```

{% sample lang='template' %}

### Template

```TWIG
{% macro MainAdvantages() %}
<!-- main-advantages -->
<main-advantages inline-template v-cloak>
  <section class="main-advantages">
    <div class="main-advantages__content-wrap">
      <figure
        class="main-advantages__item"
        v-for="item in [
          {
            image: '/assets/images/main-advantages.1.svg',
            caption: 'Выгодные цены'
          },{
            image: '/assets/images/main-advantages.2.svg',
            caption: 'Спец. условия для постоянных клиентов'
          },{
            image: '/assets/images/main-advantages.3.svg',
            caption: 'Возможность привлечения сторонней техники'
          },{
            image: '/assets/images/main-advantages.4.svg',
            caption: 'Пропуск МКАД, ТТК, центр Москвы'
          },{
            image: '/assets/images/main-advantages.5.svg',
            caption: 'Круглосуточный прием заявок'
          },{
            image: '/assets/images/main-advantages.6.svg',
            caption: 'Опытные водители'
          }
        ]"
      >
        <div
          class="main-advantages__item-image"
          :style="{ backgroundImage: 'url(' + item.image + ')' }"
        ></div>
        <figcaption class="main-advantages__item-caption">[[ item.caption ]]</figcaption>
      </figure>
    </div>
  </section>
</main-advantages>
<!-- /main-advantages -->
{% endmacro %}
```

{% sample lang='script' %}

### Script

```JS
import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|-------------------------------------------------------------------------------
| Регистрация состояния компонента
|-------------------------------------------------------------------------------
*/
store.registerModule('MainAdvantages', {
  namespaced: true,

  state() {
    return {};
  },

  getters: {},

  mutations: {}
});

/*
|-------------------------------------------------------------------------------
| Объявление объекта опций компонента
|-------------------------------------------------------------------------------
*/
const MainAdvantages = {
  computed: {
    ...mapState('MainAdvantages', []),

    ...mapGetters('MainAdvantages', [])
  },

  methods: {
    ...mapMutations('MainAdvantages', [])
  }
};

Vue.component('MainAdvantages', MainAdvantages);

export default MainAdvantages;
```

{% sample lang='style' %}

### Style

```SCSS
.main-advantages {
  position: relative;
  width: 100%;
  padding: 0 10px;
  background-color: #fff0ac;
}

.main-advantages__content-wrap {
  position: relative;
  display: flex;
  justify-content: space-between;
  width: 100%;
  max-width: 940px;
  margin: 0 auto;
  padding: 30px 0;

  @media screen and (--mobile) {
    flex-wrap: wrap;
  }
}

.main-advantages__item {
  width: calc((100% - 20px * 5) / 6);

  @media screen and (--mobile) {
    width: calc(50% - 10px);

    &:not(:nth-last-child(1), :nth-last-child(2)) {
      margin-bottom: 20px;
    }
  }
}

.main-advantages__item-image {
  width: 66px;
  height: 66px;
  margin-bottom: 20px;
  background-repeat: no-repeat;
  background-position: left center;
}

.main-advantages__item-caption {
  color: #000;
  font-size: 14px;
  font-weight: 300;
  line-height: 21px;
}

```

{% sample lang='test' %}

### Tests

```JS
import MainAdvantages from './main-advantages';

describe('MainAdvantages', () => {
  it('компоненту передан объект опций', () => {
    expect(typeof MainAdvantages).toBe('object');
  });
});
```

{% endmethod %}

{% endblock %}