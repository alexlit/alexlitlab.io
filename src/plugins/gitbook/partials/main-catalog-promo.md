{% extends '../_layouts/custom.htm' %}

[//]: HEADER

{% block header %}

# MainCatalogPromo

{% endblock %}

[//]: COMPONENT

{% block component %}main-catalog-promo{% endblock %}

[//]: API

{% block api %}

API отсутствует.

{% endblock %}

[//]: ASSETS

{% block assets %}

{% method %}

### Пример использования

```HTML
<main-catalog-promo inline-template v-cloak>
  <section class="main-catalog-promo">
    <div class="main-catalog-promo__content-wrap">
      <div class="main-catalog-promo_text">Компания «Винтел» предоставляет услуги по аренде строительной спецтехники в Москве и МО. На сайте подробно описаны возможности ТС и оборудования.</div>
      <div class="main-catalog-promo__catalog-wrap">
        <main-catalog inline-template v-cloak>
          <section class="main-catalog">
            <div class="main-catalog__content-wrap">
              <div class="main-catalog__items-wrap">
                <a href="#" class="main-catalog__item">
                  <img class="main-catalog__item-image" src="/assets/images/main-catalog.1.jpg" alt="Автокраны">
                  <span class="main-catalog__item-label">Автокраны</span>
                </a>
                <a href="#" class="main-catalog__item">
                  <img class="main-catalog__item-image" src="/assets/images/main-catalog.2.jpg" alt="Самосвалы">
                  <span class="main-catalog__item-label">Самосвалы</span>
                </a>
                <a href="#" class="main-catalog__item">
                  <img class="main-catalog__item-image" src="/assets/images/main-catalog.3.jpg" alt="Экскаваторы">
                  <span class="main-catalog__item-label">Экскаваторы</span>
                </a>
                <a href="#" class="main-catalog__item">
                  <img class="main-catalog__item-image" src="/assets/images/main-catalog.4.jpg" alt="Катки">
                  <span class="main-catalog__item-label">Катки</span>
                </a>
                <a href="#" class="main-catalog__item">
                  <img class="main-catalog__item-image" src="/assets/images/main-catalog.5.jpg" alt="Манипуляторы">
                  <span class="main-catalog__item-label">Манипуляторы</span>
                </a>
                <a href="#" class="main-catalog__item">
                  <img class="main-catalog__item-image" src="/assets/images/main-catalog.6.jpg" alt="Бульдозеры">
                  <span class="main-catalog__item-label">Бульдозеры</span>
                </a>
                <a href="#" class="main-catalog__item">
                  <img class="main-catalog__item-image" src="/assets/images/main-catalog.7.jpg" alt="Тралы">
                  <span class="main-catalog__item-label">Тралы</span>
                </a>
                <a href="#" class="main-catalog__item">
                  <img class="main-catalog__item-image" src="/assets/images/main-catalog.8.jpg" alt="Погрузчики">
                  <span class="main-catalog__item-label">Погрузчики</span>
                </a>
                <a href="#" class="main-catalog__item">
                  <img class="main-catalog__item-image" src="/assets/images/main-catalog.9.jpg" alt="Бортовые полуприцепы">
                  <span class="main-catalog__item-label">Бортовые полуприцепы</span>
                </a>
              </div>
            </div>
          </section>
        </main-catalog>
      </div>
    </div>
  </section>
</main-catalog-promo>
```

{% sample lang='template' %}

### Template

```TWIG
{% from 'components/main-catalog/main-catalog.htm' import MainCatalog %}

{% macro MainCatalogPromo() %}
<!-- main-catalog-promo -->
<main-catalog-promo inline-template v-cloak>
  <section class="main-catalog-promo">
    <div class="main-catalog-promo__content-wrap">
      <div class="main-catalog-promo_text">Компания «Винтел» предоставляет услуги по аренде строительной спецтехники в Москве и МО. На сайте подробно описаны возможности ТС и оборудования.</div>
      <div class="main-catalog-promo__catalog-wrap">

        {{ MainCatalog() }}

      </div>
    </div>
  </section>
</main-catalog-promo>
<!-- /main-catalog-promo -->
{% endmacro %}
```

{% sample lang='script' %}

### Script

```JS
import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|-------------------------------------------------------------------------------
| Регистрация состояния компонента
|-------------------------------------------------------------------------------
*/
store.registerModule('MainCatalogPromo', {
  namespaced: true,

  state() {
    return {};
  },

  getters: {},

  mutations: {}
});

/*
|-------------------------------------------------------------------------------
| Объявление объекта опций компонента
|-------------------------------------------------------------------------------
*/
const MainCatalogPromo = {
  computed: {
    ...mapState('MainCatalogPromo', []),

    ...mapGetters('MainCatalogPromo', [])
  },

  methods: {
    ...mapMutations('MainCatalogPromo', [])
  }
};

Vue.component('MainCatalogPromo', MainCatalogPromo);

export default MainCatalogPromo;
```

{% sample lang='style' %}

### Style

```SCSS
.main-catalog-promo {
  position: relative;
  width: 100%;
  padding: 0 10px;
}

.main-catalog-promo__content-wrap {
  position: relative;
  width: 100%;
  max-width: 940px;
  margin: 0 auto;
  padding: 50px 0 20px;

  @media screen and (--mobile) {
    padding: 30px 0 40px;
  }
}

.main-catalog-promo_text {
  margin-bottom: 50px;
  text-align: center;
  color: #000;
  font-size: 18px;
  font-weight: 300;
  line-height: 28px;

  @media screen and (--mobile) {
    margin-bottom: 30px;
    text-align: left;
  }
}

.main-catalog-promo__catalog-wrap {
  & .main-catalog {
    padding: 0;
  }
}

```

{% sample lang='test' %}

### Tests

```JS
import MainCatalogPromo from './main-catalog-promo';

describe('MainCatalogPromo', () => {
  it('компоненту передан объект опций', () => {
    expect(typeof MainCatalogPromo).toBe('object');
  });
});
```

{% endmethod %}

{% endblock %}