{% extends '../_layouts/custom.htm' %}

[//]: HEADER

{% block header %}

# ProductPrice

{% endblock %}

[//]: COMPONENT

{% block component %}product-price{% endblock %}

[//]: API

{% block api %}

API отсутствует.

{% endblock %}

[//]: ASSETS

{% block assets %}

{% method %}

### Пример использования

```HTML
<product-price inline-template v-cloak>
  <section class="product-price">
    <div class="product-price__content-wrap">
      <div class="product-price__table-wrap">
        <table class="product-price__table">
          <thead>
            <tr>
              <th>Аренда</th>
              <th>юр. лица
                <br>+18% НДС</th>
              <th>физ. лица</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th>час</th>
              <td>1062 руб.</td>
              <td>900 руб.</td>
            </tr>
            <tr>
              <th>Смена</th>
              <td>5900 руб.</td>
              <td>5000 руб.</td>
            </tr>
            <tr>
              <th>Переработка</th>
              <td>1180 руб.</td>
              <td>1000 руб.</td>
            </tr>
            <tr>
              <th>1 км. за МКАД</th>
              <td>50 руб.</td>
              <td>50 руб.</td>
            </tr>
          </tbody>
        </table>
      </div>
      <footer class="product-price__footer">
        <button class="product-price__button product-price__button--order" type="button" @click="setSuccessStateFormFastOrder(false);setFormDataFormFastOrder({IMAGE: '/assets/images/product-gallery.1.jpg',TITLE: 'Экскаватор-погрузчик JCB BCX Super'});showModal('form-fast-order');">Быстрый заказ</button>
        <button class="product-price__button" type="button" @click="setSuccessStateFormOrder(false);setFormDataFormOrder({IMAGE: '/assets/images/product-gallery.1.jpg',TITLE: 'Экскаватор-погрузчик JCB BCX Super'});showModal('form-order');">Арендовать</button>
      </footer>
    </div>
  </section>
</product-price>
```

{% sample lang='template' %}

### Template

```TWIG
{% macro ProductPrice(
  image = '/assets/images/product-gallery.1.jpg',
  title = 'Экскаватор-погрузчик JCB BCX Super'
) %}
<!-- product-price -->
<product-price inline-template v-cloak>
  <section class="product-price">
    <div class="product-price__content-wrap">
      <div class="product-price__table-wrap">
        <table class="product-price__table">
          <thead>
            <tr>
              <th>Аренда</th>
              <th>юр. лица<br>+18% НДС</th>
              <th>физ. лица</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th>час</th>
              <td>1062 руб.</td>
              <td>900 руб.</td>
            </tr>
            <tr>
              <th>Смена</th>
              <td>5900 руб.</td>
              <td>5000 руб.</td>
            </tr>
            <tr>
              <th>Переработка</th>
              <td>1180 руб.</td>
              <td>1000 руб.</td>
            </tr>
            <tr>
              <th>1 км. за МКАД</th>
              <td>50 руб.</td>
              <td>50 руб.</td>
            </tr>
          </tbody>
        </table>
      </div>
      <footer class="product-price__footer">
        <button
          class="product-price__button product-price__button--order"
          type="button"
          @click="
            setSuccessStateFormFastOrder(false);
            setFormDataFormFastOrder({
              IMAGE: '{{ image }}',
              TITLE: '{{ title }}'
            });
            showModal('form-fast-order');
          "
        >Быстрый заказ</button>
        <button
          class="product-price__button"
          type="button"
          @click="
            setSuccessStateFormOrder(false);
            setFormDataFormOrder({
              IMAGE: '{{ image }}',
              TITLE: '{{ title }}'
            });
            showModal('form-order');
          "
        >Арендовать</button>
      </footer>
    </div>
  </section>
</product-price>
<!-- /product-price -->
{% endmacro %}
```

{% sample lang='script' %}

### Script

```JS
import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|-------------------------------------------------------------------------------
| Регистрация состояния компонента
|-------------------------------------------------------------------------------
*/
store.registerModule('ProductPrice', {
  namespaced: true,

  state() {
    return {};
  },

  getters: {},

  mutations: {}
});

/*
|-------------------------------------------------------------------------------
| Объявление объекта опций компонента
|-------------------------------------------------------------------------------
*/
const ProductPrice = {
  computed: {
    ...mapState('ProductPrice', []),
    ...mapState('App', ['modal']),

    ...mapGetters('ProductPrice', [])
  },

  methods: {
    ...mapMutations('ProductPrice', []),
    ...mapMutations('App', ['showModal']),
    ...mapMutations('FormFastOrder', {
      setSuccessStateFormFastOrder: 'setSuccessState',
      setFormDataFormFastOrder: 'setFormData'
    }),
    ...mapMutations('FormOrder', {
      setSuccessStateFormOrder: 'setSuccessState',
      setFormDataFormOrder: 'setFormData'
    })
  }
};

Vue.component('ProductPrice', ProductPrice);

export default ProductPrice;

```

{% sample lang='style' %}

### Style

```SCSS
.product-price {
  position: relative;
  width: 100%;
}

.product-price__content-wrap {
  position: relative;
  width: 100%;
  max-width: 940px;
  margin: 0 auto;
}

.product-price__table-wrap {
  position: relative;
  width: 100%;
  margin-bottom: 20px;

  @media screen and (--mobile) {
    overflow: auto;
    -webkit-overflow-scrolling: touch;
  }
}

.product-price__table {
  width: 100%;
  border-collapse: collapse;

  & thead {
    background-color: #215ed4;

    & th {
      vertical-align: top;
      color: #fff;
    }
  }

  & tbody {
    & tr {
      &:nth-child(odd) {
        background-color: #eff0f0;
      }

      &:nth-child(even) {
        background-color: #f7f8fa;
      }
    }
  }

  & th {
    padding: 21px 10px;
    text-align: left;
    letter-spacing: 1px;
    text-transform: uppercase;
    color: #000;
    font-size: 12px;
    font-weight: 500;
    line-height: 18px;

    @media screen and (--mobile) {
      white-space: nowrap;
    }
  }

  & td {
    padding: 21px 10px;
    color: #000;
    font-size: 18px;
    font-weight: 300;
    line-height: 28px;

    @media screen and (--mobile) {
      white-space: nowrap;
    }
  }
}

.product-price__footer {
  position: relative;
  display: flex;
  align-items: flex-start;
  flex-wrap: wrap;
}

.product-price__button {
  display: block;
  padding: 20px 15px;
  white-space: nowrap;
  letter-spacing: 1px;
  text-transform: uppercase;
  color: #000;
  background-color: #eee;
  font-size: 16px;
  font-weight: 500;
  line-height: 18px;

  &:hover {
    background-color: #ffd101;
  }

  &--order {
    background-color: #ffd101;

    &:hover {
      background-color: #ffe500;
    }
  }

  &:not(:last-child) {
    margin-right: 10px;

    @media screen and (--mobile) {
      margin-bottom: 10px;
    }
  }
}

```

{% sample lang='test' %}

### Tests

```JS
import ProductPrice from './product-price';

describe('ProductPrice', () => {
  it('компоненту передан объект опций', () => {
    expect(typeof ProductPrice).toBe('object');
  });
});
```

{% endmethod %}

{% endblock %}