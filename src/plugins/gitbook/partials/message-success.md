{% extends '../_layouts/custom.htm' %}

[//]: HEADER

{% block header %}

# MessageSuccess

{% endblock %}

[//]: COMPONENT

{% block component %}message-success{% endblock %}

[//]: API

{% block api %}

API отсутствует.

{% endblock %}

[//]: ASSETS

{% block assets %}

{% method %}

### Пример использования

```HTML
<message-success inline-template v-cloak>
  <section class="message-success">
    <div class="message-success__content-wrap">
      <header class="message-success__title">Ваш заказ оформлен!</header>
      <p class="message-success__caption">Мы свяжемся с вами в самое ближайшее время для уточнения деталей заказа.</p>
    </div>
  </section>
</message-success>
```

{% sample lang='template' %}

### Template

```TWIG
{% macro MessageSuccess() %}
<!-- message-success -->
<message-success inline-template v-cloak>
  <section class="message-success">
    <div class="message-success__content-wrap">
      <header class="message-success__title">Ваш заказ оформлен!</header>
      <p class="message-success__caption">Мы свяжемся с вами в самое ближайшее время для уточнения деталей заказа.</p>
    </div>
  </section>
</message-success>
<!-- /message-success -->
{% endmacro %}
```

{% sample lang='script' %}

### Script

```JS
import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|-------------------------------------------------------------------------------
| Регистрация состояния компонента
|-------------------------------------------------------------------------------
*/
store.registerModule('MessageSuccess', {
  namespaced: true,

  state() {
    return {};
  },

  getters: {},

  mutations: {}
});

/*
|-------------------------------------------------------------------------------
| Объявление объекта опций компонента
|-------------------------------------------------------------------------------
*/
const MessageSuccess = {
  computed: {
    ...mapState('MessageSuccess', []),

    ...mapGetters('MessageSuccess', [])
  },

  methods: {
    ...mapMutations('MessageSuccess', [])
  }
};

Vue.component('MessageSuccess', MessageSuccess);

export default MessageSuccess;
```

{% sample lang='style' %}

### Style

```SCSS
.message-success {
  position: relative;
  width: 100%;
}

.message-success__content-wrap {
  position: relative;
  width: 100%;
}

.message-success__title {
  margin-bottom: 20px;
  padding-top: 160px;
  color: #000;
  background-image: url('../images/message-success.check.svg');
  background-repeat: no-repeat;
  background-position: left top;
  font-size: 30px;
  font-weight: 700;
  line-height: 36px;
}

.message-success__caption {
  color: #000;
  font-size: 18px;
  font-weight: 400;
  line-height: 24px;
}

```

{% sample lang='test' %}

### Tests

```JS
import MessageSuccess from './message-success';

describe('MessageSuccess', () => {
  it('компоненту передан объект опций', () => {
    expect(typeof MessageSuccess).toBe('object');
  });
});
```

{% endmethod %}

{% endblock %}