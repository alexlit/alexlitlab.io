{% extends '../_layouts/custom.htm' %}

[//]: HEADER

{% block header %}

# MainSeo

{% endblock %}

[//]: COMPONENT

{% block component %}main-seo{% endblock %}

[//]: API

{% block api %}

API отсутствует.

{% endblock %}

[//]: ASSETS

{% block assets %}

{% method %}

### Пример использования

```HTML
<main-seo inline-template v-cloak>
  <section class="main-seo">
    <div class="main-seo__content-wrap">
      <p class="main-seo__title">Аренда экскаваторов</p>
      <div class="main-seo__text typography">
        <p>Универсальное техническое мобильное устройство предназначенной для проведения землеройных работ, подъемов различных грузов их перемещением на значительные расстояния, называется экскаватор-погрузчик. Такая многофункциональная машина своим присутствием на строительной площадке может заменить сразу несколько единиц спецтехники. Она одновременно служит классическим фронтальным погрузчиком и выполняет функции, свойственные землеройным машинам. Помимо погрузочно-разгрузочных и транспортировочных действия с помощью экскаватора-погрузчика производится планировка грунтов.</p>
      </div>
    </div>
  </section>
</main-seo>
```

{% sample lang='template' %}

### Template

```TWIG
{% macro MainSeo(
  title = 'Аренда экскаваторов',
  text = '<p>Универсальное техническое мобильное устройство предназначенной для проведения землеройных работ, подъемов различных грузов их перемещением на значительные расстояния, называется экскаватор-погрузчик. Такая многофункциональная машина своим присутствием на строительной площадке может заменить сразу несколько единиц спецтехники. Она одновременно служит классическим фронтальным погрузчиком и выполняет функции, свойственные землеройным машинам. Помимо погрузочно-разгрузочных и транспортировочных действия с помощью экскаватора-погрузчика производится планировка грунтов.</p>'
) %}
<!-- main-seo -->
<main-seo inline-template v-cloak>
  <section class="main-seo">
    <div class="main-seo__content-wrap">
      <p class="main-seo__title">{{ title }}</p>
      <div class="main-seo__text typography">
        {{ text | safe }}
      </div>
    </div>
  </section>
</main-seo>
<!-- /main-seo -->
{% endmacro %}
```

{% sample lang='script' %}

### Script

```JS
import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|-------------------------------------------------------------------------------
| Регистрация состояния компонента
|-------------------------------------------------------------------------------
*/
store.registerModule('MainSeo', {
  namespaced: true,

  state() {
    return {};
  },

  getters: {},

  mutations: {}
});

/*
|-------------------------------------------------------------------------------
| Объявление объекта опций компонента
|-------------------------------------------------------------------------------
*/
const MainSeo = {
  computed: {
    ...mapState('MainSeo', []),

    ...mapGetters('MainSeo', [])
  },

  methods: {
    ...mapMutations('MainSeo', [])
  }
};

Vue.component('MainSeo', MainSeo);

export default MainSeo;
```

{% sample lang='style' %}

### Style

```SCSS
.main-seo {
  position: relative;
  width: 100%;
  padding: 0 10px;
}

.main-seo__content-wrap {
  position: relative;
  width: 100%;
  max-width: 940px;
  margin: 0 auto;
  padding: 30px 0 60px;
  border-top: 1px solid #eaeaea;

  @media screen and (--mobile) {
    padding: 30px 0;
  }
}

.main-seo__title {
  margin-bottom: 30px;
  color: #000;
  font-size: 30px;
  font-weight: 700;
  line-height: 36px;

  @media screen and (--mobile) {
    font-size: 24px;
    line-height: 30px;
  }
}

.main-seo__text {
  &.typography {
    position: relative;
    color: #000;
    font-size: 18px;
    font-weight: 300;
    line-height: 28px;

    @media screen and (--mobile) {
      font-size: 16px;
      line-height: 24px;
    }
  }
}

```

{% sample lang='test' %}

### Tests

```JS
import MainSeo from './main-seo';

describe('MainSeo', () => {
  it('компоненту передан объект опций', () => {
    expect(typeof MainSeo).toBe('object');
  });
});
```

{% endmethod %}

{% endblock %}