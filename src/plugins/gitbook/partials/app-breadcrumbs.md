{% extends '../_layouts/custom.htm' %}

[//]: HEADER

{% block header %}

# AppBreadcrumbs

{% endblock %}

[//]: COMPONENT

{% block component %}app-breadcrumbs{% endblock %}

[//]: API

{% block api %}

API отсутствует.

{% endblock %}

[//]: ASSETS

{% block assets %}

{% method %}

### Пример использования

```HTML
<app-breadcrumbs inline-template v-cloak>
  <nav class="app-breadcrumbs">
    <div class="app-breadcrumbs__content-wrap"><a href="#" class="app-breadcrumbs__item">СПЕЦТЕХНИКА</a> <a href="#" class="app-breadcrumbs__item">ЭКСКАВАТОРЫ</a></div>
  </nav>
</app-breadcrumbs>
```

{% sample lang='template' %}

### Template

```TWIG
{% macro AppBreadcrumbs(
  items = ['СПЕЦТЕХНИКА', 'ЭКСКАВАТОРЫ']
) %}
<!-- app-breadcrumbs -->
<app-breadcrumbs inline-template v-cloak>
  <nav class="app-breadcrumbs">
    <div class="app-breadcrumbs__content-wrap">

      {% for item in items %}

        <a href="#" class="app-breadcrumbs__item">{{ item }}</a>

      {% endfor %}

    </div>
  </nav>
</app-breadcrumbs>
<!-- /app-breadcrumbs -->
{% endmacro %}
```

{% sample lang='script' %}

### Script

```JS
import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|-------------------------------------------------------------------------------
| Регистрация состояния компонента
|-------------------------------------------------------------------------------
*/
store.registerModule('AppBreadcrumbs', {
  namespaced: true,

  state() {
    return {};
  },

  getters: {},

  mutations: {}
});

/*
|-------------------------------------------------------------------------------
| Объявление объекта опций компонента
|-------------------------------------------------------------------------------
*/
const AppBreadcrumbs = {
  computed: {
    ...mapState('AppBreadcrumbs', []),

    ...mapGetters('AppBreadcrumbs', [])
  },

  methods: {
    ...mapMutations('AppBreadcrumbs', [])
  }
};

Vue.component('AppBreadcrumbs', AppBreadcrumbs);

export default AppBreadcrumbs;
```

{% sample lang='style' %}

### Style

```SCSS
.app-breadcrumbs {
  position: relative;
  width: 100%;
  padding: 0 10px;

  & + .page-title {
    .page-title__content-wrap {
      padding-top: 0;
    }
  }
}

.app-breadcrumbs__content-wrap {
  position: relative;
  display: flex;
  flex-wrap: wrap;
  width: 100%;
  max-width: 940px;
  margin: 0 auto;
  padding: 30px 0 10px;

  @media screen and (--mobile) {
    padding: 20px 0 10px;
  }
}

.app-breadcrumbs__item {
  display: block;
  letter-spacing: 1px;
  text-transform: uppercase;
  color: #000;
  font-size: 12px;
  font-weight: 500;
  line-height: 18px;

  &:hover {
    color: #215ed4;
  }

  &:last-child {
    pointer-events: none;
  }

  &:not(:last-child) {
    &::after {
      padding: 0 3px;
      content: '-';
      color: #000 !important;
    }
  }
}

```

{% sample lang='test' %}

### Tests

```JS
import AppBreadcrumbs from './app-breadcrumbs';

describe('AppBreadcrumbs', () => {
  it('компоненту передан объект опций', () => {
    expect(typeof AppBreadcrumbs).toBe('object');
  });
});
```

{% endmethod %}

{% endblock %}