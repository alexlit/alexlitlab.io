{% extends '../_layouts/custom.htm' %}

[//]: HEADER

{% block header %}

# TestimonialsCarousel

{% endblock %}

[//]: COMPONENT

{% block component %}testimonials-carousel{% endblock %}

[//]: API

{% block api %}

API отсутствует.

{% endblock %}

[//]: ASSETS

{% block assets %}

{% method %}

### Пример использования

```HTML
<testimonials-carousel inline-template v-cloak>
  <section class="testimonials-carousel">
    <div class="testimonials-carousel__content-wrap swiper-container" data-swiper>
      <div class="swiper-wrapper">
        <figure class="testimonials-carousel__slide swiper-slide">
          <img src="/assets/images/testimonials-carousel.1.jpg" alt="" class="testimonials-carousel__slide-image">
          <figcaption class="testimonials-carousel__slide-caption">
            <p class="testimonials-carousel__slide-text">Работали с “Винтел” по долгосрочному договору. Заказывал 25-тонный кран для подъема металлоконструкций при строительстве склада. Техника доставлялась вовремя, крановщик делал свою работу. Все без замечаний.</p>
            <p class="testimonials-carousel__slide-name">Сергей Климов</p>
            <p class="testimonials-carousel__slide-company">СтройМаршКонсалтинг</p>
          </figcaption>
        </figure>
        <figure class="testimonials-carousel__slide swiper-slide">
          <img src="/assets/images/testimonials-carousel.1.jpg" alt="" class="testimonials-carousel__slide-image">
          <figcaption class="testimonials-carousel__slide-caption">
            <p class="testimonials-carousel__slide-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae dolores dolore sint doloremque esse quia, error deserunt nisi iusto, id labore illo! Aperiam nemo doloremque dignissimos in sit natus commodi.</p>
            <p class="testimonials-carousel__slide-name">Сергей Климов</p>
            <p class="testimonials-carousel__slide-company">СтройМаршКонсалтинг</p>
          </figcaption>
        </figure>
        <figure class="testimonials-carousel__slide swiper-slide">
          <img src="/assets/images/testimonials-carousel.1.jpg" alt="" class="testimonials-carousel__slide-image">
          <figcaption class="testimonials-carousel__slide-caption">
            <p class="testimonials-carousel__slide-text">Lorem ipsum dolor sit amet consec.</p>
            <p class="testimonials-carousel__slide-name">Сергей Климов</p>
            <p class="testimonials-carousel__slide-company">СтройМаршКонсалтинг</p>
          </figcaption>
        </figure>
      </div>
      <nav class="testimonials-carousel__pagination swiper-pagination" data-swiper-pagination></nav>
    </div>
  </section>
</testimonials-carousel>
```

{% sample lang='template' %}

### Template

```TWIG
{% macro TestimonialsCarousel() %}
<!-- testimonials-carousel -->
<testimonials-carousel inline-template v-cloak>
  <section class="testimonials-carousel">
    <div class="testimonials-carousel__content-wrap swiper-container" data-swiper>
      <div class="swiper-wrapper">

        {% for item in [
          {
            image: '/assets/images/testimonials-carousel.1.jpg',
            text: 'Работали с “Винтел” по долгосрочному договору. Заказывал 25-тонный кран для подъема металлоконструкций при строительстве склада. Техника доставлялась вовремя, крановщик делал свою работу. Все без замечаний.',
            name: 'Сергей Климов',
            company: 'СтройМаршКонсалтинг'
          },{
            image: '/assets/images/testimonials-carousel.1.jpg',
            text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae dolores dolore sint doloremque esse quia, error deserunt nisi iusto, id labore illo! Aperiam nemo doloremque dignissimos in sit natus commodi.',
            name: 'Сергей Климов',
            company: 'СтройМаршКонсалтинг'
          },{
            image: '/assets/images/testimonials-carousel.1.jpg',
            text: 'Lorem ipsum dolor sit amet consec.',
            name: 'Сергей Климов',
            company: 'СтройМаршКонсалтинг'
          }
        ] %}

          <figure class="testimonials-carousel__slide swiper-slide">
            <img src="{{ item.image }}" alt="" class="testimonials-carousel__slide-image">
            <figcaption class="testimonials-carousel__slide-caption">
              <p class="testimonials-carousel__slide-text">{{ item.text }}</p>
              <p class="testimonials-carousel__slide-name">{{ item.name }}</p>
              <p class="testimonials-carousel__slide-company">{{ item.company }}</p>
            </figcaption>
          </figure>

        {% endfor %}

      </div>
      <nav
        class="testimonials-carousel__pagination swiper-pagination"
        data-swiper-pagination
      ></nav>
    </div>
  </section>
</testimonials-carousel>
<!-- /testimonials-carousel -->
{% endmacro %}


```

{% sample lang='script' %}

### Script

```JS
import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import Swiper from 'swiper/dist/js/swiper.min';
import 'swiper/dist/css/swiper.min.css';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|-------------------------------------------------------------------------------
| Регистрация состояния компонента
|-------------------------------------------------------------------------------
*/
store.registerModule('TestimonialsCarousel', {
  namespaced: true,

  state() {
    return {};
  },

  getters: {},

  mutations: {}
});

/*
|-------------------------------------------------------------------------------
| Объявление объекта опций компонента
|-------------------------------------------------------------------------------
*/
const TestimonialsCarousel = {
  computed: {
    ...mapState('TestimonialsCarousel', []),

    ...mapGetters('TestimonialsCarousel', [])
  },

  methods: {
    ...mapMutations('TestimonialsCarousel', [])
  },

  mounted() {
    const swiper = new Swiper('.testimonials-carousel [data-swiper]', {
      autoplay: {
        delay: 5000,
        disableOnInteraction: false
      },
      slidesPerView: 1,
      spaceBetween: 0,
      centeredSlides: true,
      speed: 1000,
      loop: true,
      effect: 'fade',
      fadeEffect: {
        crossFade: true
      },
      pagination: {
        el: '.testimonials-carousel [data-swiper-pagination]',
        clickable: true
      }
    });
    swiper.autoplay.start();
  }
};

Vue.component('TestimonialsCarousel', TestimonialsCarousel);

export default TestimonialsCarousel;

```

{% sample lang='style' %}

### Style

```SCSS
.testimonials-carousel {
  position: relative;
  width: 100%;
  padding: 0 10px;
  padding: 70px 0 50px;
  background-image: url('../images/testimonials-carousel.bgd.jpg');
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;

  @media screen and (--mobile) {
    padding: 0 10px 10px;
    background-color: #fff;
    background-image: none;
  }
}

.testimonials-carousel__content-wrap {
  position: relative;
  width: 100%;
  max-width: 780px;
  margin: 0 auto;
  padding-top: 40px;
}

.testimonials-carousel__slide {
  position: relative;
  padding: 80px 50px 40px;
  background-color: #fff;

  @media screen and (--mobile) {
    padding: 70px 10px 50px;
    background-color: #f7f8fa;
  }
}

.testimonials-carousel__slide-image {
  position: absolute;
  z-index: 1;
  top: 0;
  left: 50%;
  display: block;
  overflow: hidden;
  width: 80px;
  height: 80px;
  transform: translate(-50%, -50%);
  border-radius: 50%;
  object-fit: cover;
  object-position: center;
}

.testimonials-carousel__slide-caption {
  position: relative;
  width: 100%;
}

.testimonials-carousel__slide-text {
  margin-bottom: 30px;
  text-align: center;
  color: #000;
  font-size: 18px;
  font-weight: 300;
  line-height: 28px;
}

.testimonials-carousel__slide-name {
  text-align: center;
  color: #000;
  font-size: 18px;
  font-weight: 300;
  line-height: 28px;
}

.testimonials-carousel__slide-company {
  text-align: center;
  text-transform: uppercase;
  color: #000;
  font-size: 12px;
  font-weight: 700;
  line-height: 28px;
}

.testimonials-carousel__pagination {
  &.swiper-pagination {
    position: static;
    display: flex;
    justify-content: center;
    width: 100%;
    margin-top: 30px;

    @media screen and (--mobile) {
      display: none;
    }

    & .swiper-pagination-bullet {
      display: block;
      width: 60px;
      height: 5px;
      margin-right: 10px;
      margin-left: 0;
      transition-duration: .4s;
      opacity: 1;
      border-radius: 0;
      background-color: #d8d8d8;

      &:hover {
        background-color: #437eef;
      }

      &.swiper-pagination-bullet-active {
        background-color: #215ed4;
      }
    }
  }
}

```

{% sample lang='test' %}

### Tests

```JS
import TestimonialsCarousel from './testimonials-carousel';

describe('TestimonialsCarousel', () => {
  it('компоненту передан объект опций', () => {
    expect(typeof TestimonialsCarousel).toBe('object');
  });
});
```

{% endmethod %}

{% endblock %}