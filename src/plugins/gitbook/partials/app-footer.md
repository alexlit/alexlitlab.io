{% extends '../_layouts/custom.htm' %}

[//]: HEADER

{% block header %}

# AppFooter

{% endblock %}

[//]: COMPONENT

{% block component %}app-footer{% endblock %}

[//]: API

{% block api %}

API отсутствует.

{% endblock %}

[//]: ASSETS

{% block assets %}

{% method %}

### Пример использования

```HTML
<app-footer inline-template v-cloak>
  <section class="app-footer">
    <div class="app-footer__content-wrap">
      <div class="app-footer__info">
        <a href="/" class="app-footer__logo">
          <img src="/assets/images/app-footer.logo.svg" alt="" class="app-footer__logo-image">
        </a>
        <p class="app-footer__logo-caption">Аренда спецтехники
          <br>в Москве и МО</p>
      </div>
      <nav class="app-footer__header-menu"><a :class="{'app-footer__header-menu-item': true,'app-footer__header-menu-item--active': false}" href="#" v-if="$mq !== 'mobile'">О компании</a> <a :class="{'app-footer__header-menu-item': true,'app-footer__header-menu-item--active': false}" href="#" v-if="$mq !== 'mobile'">Вопрос-ответ</a> <a :class="{'app-footer__header-menu-item': true,'app-footer__header-menu-item--active': false}" href="#" v-if="$mq === 'mobile'">Каталог спецтехники</a> <a :class="{'app-footer__header-menu-item': true,'app-footer__header-menu-item--active': false}" href="#">Условия аренды</a> <a :class="{'app-footer__header-menu-item': true,'app-footer__header-menu-item--active': false}" href="#">Контакты</a></nav>
      <nav class="app-footer__main-menu" v-if="$mq !== 'mobile'"><a :class="{'app-footer__main-menu-item': true,'app-footer__main-menu-item--active': false}" href="#">Автокраны</a> <a :class="{'app-footer__main-menu-item': true,'app-footer__main-menu-item--active': false}" href="#">Самосвалы</a> <a :class="{'app-footer__main-menu-item': true,'app-footer__main-menu-item--active': false}" href="#">Экскаваторы</a> <a :class="{'app-footer__main-menu-item': true,'app-footer__main-menu-item--active': false}" href="#">Катки</a> <a :class="{'app-footer__main-menu-item': true,'app-footer__main-menu-item--active': false}" href="#">Манипуляторы</a> <a :class="{'app-footer__main-menu-item': true,'app-footer__main-menu-item--active': false}" href="#">Бульдозеры</a> <a :class="{'app-footer__main-menu-item': true,'app-footer__main-menu-item--active': false}" href="#">Тралы</a> <a :class="{'app-footer__main-menu-item': true,'app-footer__main-menu-item--active': false}" href="#">Погрузчики</a></nav>
      <address class="app-footer__contacts"><a href="tel:" class="app-footer__phone">8 (495) 000-00-00</a>
        <button class="app-footer__calback-button" type="button" @click="setSuccessState(false);showModal('form-callback');">Перезвоните мне</button> <a href="http://krown.cc" class="app-footer__developer" target="_blank">Разработка и поддержка</a></address>
    </div>
  </section>
</app-footer>
```

{% sample lang='template' %}

### Template

```TWIG
{% macro AppFooter() %}
<!-- app-footer -->
<app-footer inline-template v-cloak>
  <section class="app-footer">
    <div class="app-footer__content-wrap">
      <div class="app-footer__info">
        <a href="/" class="app-footer__logo">
          <img src="/assets/images/app-footer.logo.svg" alt="" class="app-footer__logo-image">
        </a>
        <p class="app-footer__logo-caption">Аренда спецтехники<br>в Москве и МО</p>
      </div>
      <nav class="app-footer__header-menu">
        <a
          :class="{
            'app-footer__header-menu-item': true,
            'app-footer__header-menu-item--active': false
          }"
          href="#"
          v-if="$mq !== 'mobile'"
        >О компании</a>
        <a
          :class="{
            'app-footer__header-menu-item': true,
            'app-footer__header-menu-item--active': false
          }"
          href="#"
          v-if="$mq !== 'mobile'"
        >Вопрос-ответ</a>
        <a
          :class="{
            'app-footer__header-menu-item': true,
            'app-footer__header-menu-item--active': false
          }"
          href="#"
          v-if="$mq === 'mobile'"
        >Каталог спецтехники</a>
        <a
          :class="{
            'app-footer__header-menu-item': true,
            'app-footer__header-menu-item--active': false
          }"
          href="#"
        >Условия аренды</a>
        <a
          :class="{
            'app-footer__header-menu-item': true,
            'app-footer__header-menu-item--active': false
          }"
          href="#"
        >Контакты</a>
      </nav>
      <nav class="app-footer__main-menu" v-if="$mq !== 'mobile'">

        {% for item in [
          { title: 'Автокраны', isActive: false },
          { title: 'Самосвалы', isActive: false },
          { title: 'Экскаваторы', isActive: false },
          { title: 'Катки', isActive: false },
          { title: 'Манипуляторы', isActive: false },
          { title: 'Бульдозеры', isActive: false },
          { title: 'Тралы', isActive: false },
          { title: 'Погрузчики', isActive: false }
        ] %}

          <a
            :class="{
              'app-footer__main-menu-item': true,
              'app-footer__main-menu-item--active': {{ item.isActive }}
            }"
            href="#"
          >{{ item.title }}</a>

        {% endfor %}

      </nav>
      <address class="app-footer__contacts">
        <a href="tel:" class="app-footer__phone">8 (495) 000-00-00</a>
        <button
          class="app-footer__calback-button"
          type="button"
          @click="
            setSuccessState(false);
            showModal('form-callback');
          "
        >Перезвоните мне</button>
        <a
          href="http://krown.cc"
          class="app-footer__developer"
          target="_blank"
        >Разработка и поддержка</a>
      </address>
    </div>
  </section>
</app-footer>
<!-- /app-footer -->
{% endmacro %}
```

{% sample lang='script' %}

### Script

```JS
import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|-------------------------------------------------------------------------------
| Регистрация состояния компонента
|-------------------------------------------------------------------------------
*/
store.registerModule('AppFooter', {
  namespaced: true,

  state() {
    return {};
  },

  getters: {},

  mutations: {}
});

/*
|-------------------------------------------------------------------------------
| Объявление объекта опций компонента
|-------------------------------------------------------------------------------
*/
const AppFooter = {
  computed: {
    ...mapState('AppFooter', []),
    ...mapState('App', ['modal']),

    ...mapGetters('AppFooter', [])
  },

  methods: {
    ...mapMutations('AppFooter', []),
    ...mapMutations('App', ['showModal']),
    ...mapMutations('FormCallback', ['setSuccessState'])
  }
};

Vue.component('AppFooter', AppFooter);

export default AppFooter;

```

{% sample lang='style' %}

### Style

```SCSS
.app-footer {
  position: relative;
  width: 100%;
  padding: 0 10px;

  @media screen and (--mobile) {
    background-color: #083ea7;
  }
}

.app-footer__content-wrap {
  position: relative;
  display: flex;
  justify-content: space-between;
  width: 100%;
  max-width: 940px;
  margin: 0 auto;
  padding: 80px 0 80px;

  @media screen and (--mobile) {
    display: block;
    padding: 30px 0 48px;
  }
}

.app-footer__info {
  width: 220px;

  @media screen and (--mobile) {
    width: 100%;
    margin-bottom: 20px;
    padding-bottom: 20px;
    border-bottom: 1px solid #1956cc;
  }
}

.app-footer__logo {
  position: relative;
  display: block;
  width: 138px;
  height: 33px;
  margin-bottom: 36px;

  @media screen and (--mobile) {
    margin: 0 auto 14px;
  }
}

.app-footer__logo-image {
  display: block;
  max-width: 100%;
  max-height: 100%;
}

.app-footer__logo-caption {
  color: #fff;
  font-size: 14px;
  font-weight: 300;
  line-height: 19px;

  @media screen and (--mobile) {
    text-align: center;

    & br {
      display: none;
    }
  }
}

.app-footer__header-menu {
  display: flex;
  flex-direction: column;
  width: 220px;

  @media screen and (--mobile) {
    width: 100%;
    margin-bottom: 5px;
  }
}

.app-footer__header-menu-item {
  letter-spacing: .86px;
  text-transform: uppercase;
  color: #fff;
  font-size: 12px;
  font-weight: 500;

  @media screen and (--mobile) {
    padding: 8px 0;
    text-align: center;
  }

  &:hover {
    color: #fc0;
  }

  &:not(:last-child) {
    padding-bottom: 20px;

    @media screen and (--mobile) {
      padding-bottom: 8px;
    }
  }

  &--active {
    color: #fc0;
  }
}

.app-footer__main-menu {
  display: flex;
  flex-direction: column;
  width: 160px;
}

.app-footer__main-menu-item {
  letter-spacing: .86px;
  text-transform: uppercase;
  color: #fff;
  font-size: 12px;
  font-weight: 500;

  &:hover {
    color: #fc0;
  }

  &:not(:last-child) {
    padding-bottom: 20px;
  }

  &--active {
    color: #fc0;
  }
}

.app-footer__contacts {
  display: flex;
  align-items: flex-end;
  flex-direction: column;
  width: 280px;

  @media screen and (--mobile) {
    display: block;
    width: 100%;
  }
}

.app-footer__phone {
  margin-bottom: 10px;
  color: #fff;
  font-size: 30px;
  font-weight: 700;

  @media screen and (--mobile) {
    display: block;
    text-align: center;
  }

  &:hover {
    color: #fc0;
  }
}

.app-footer__calback-button {
  margin-bottom: 50px;
  letter-spacing: 1px;
  text-transform: uppercase;
  color: #fff;
  font-size: 12px;
  font-weight: 500;
  line-height: 14px;

  @media screen and (--mobile) {
    display: block;
    margin: 0 auto;
    margin-bottom: 30px;
    text-align: center;
  }

  &:hover {
    color: #fc0;
  }
}

.app-footer__developer {
  display: block;
  padding-bottom: 36px;
  color: #fff;
  background-image: url('../images/app-footer.developer.svg');
  background-repeat: no-repeat;
  background-position: right bottom;
  font-size: 16px;
  line-height: 24px;

  @media screen and (--mobile) {
    text-align: center;
    background-position: center bottom;
  }

  &:hover {
    color: #fc0;
  }
}

```

{% sample lang='test' %}

### Tests

```JS
import AppFooter from './app-footer';

describe('AppFooter', () => {
  it('компоненту передан объект опций', () => {
    expect(typeof AppFooter).toBe('object');
  });
});
```

{% endmethod %}

{% endblock %}