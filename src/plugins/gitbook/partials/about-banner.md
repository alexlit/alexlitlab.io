{% extends '../_layouts/custom.htm' %}

[//]: HEADER

{% block header %}

# AboutBanner

{% endblock %}

[//]: COMPONENT

{% block component %}about-banner{% endblock %}

[//]: API

{% block api %}

API отсутствует.

{% endblock %}

[//]: ASSETS

{% block assets %}

{% method %}

### Пример использования

```HTML
<about-banner inline-template v-cloak>
  <section class="about-banner">
    <div class="about-banner__content-wrap">
      <figure class="about-banner__item">
        <img src="/assets/images/about-banner.1.jpg" alt="" class="about-banner__image">
        <figcaption class="about-banner__caption">Иначе говоря, политическое манипулирование традиционно определяет кризис легитимности. Многопартийная система, короче говоря, теоретически сохраняет культ личности. Бихевиоризм приводит кризис легитимности, что было отмечено П.Лазарсфельдом. Англо-американский тип политической культуры, короче говоря, доказывает антропологический тоталитарный тип политической культуры. Как уже подчеркивалось, гуманизм приводит коллапс Советского Союза. Политическое учение Аристотеля неоднозначно.</figcaption>
      </figure>
    </div>
  </section>
</about-banner>
```

{% sample lang='template' %}

### Template

```TWIG
{% macro AboutBanner() %}
<!-- about-banner -->
<about-banner inline-template v-cloak>
  <section class="about-banner">
    <div class="about-banner__content-wrap">
      <figure class="about-banner__item">
        <img src="/assets/images/about-banner.1.jpg" alt="" class="about-banner__image">
        <figcaption class="about-banner__caption">Иначе говоря, политическое манипулирование традиционно определяет кризис легитимности. Многопартийная система, короче говоря, теоретически сохраняет культ личности. Бихевиоризм приводит кризис легитимности, что было отмечено П.Лазарсфельдом. Англо-американский тип политической культуры, короче говоря, доказывает антропологический тоталитарный тип политической культуры. Как уже подчеркивалось, гуманизм приводит коллапс Советского Союза. Политическое учение Аристотеля неоднозначно.</figcaption>
      </figure>
    </div>
  </section>
</about-banner>
<!-- /about-banner -->
{% endmacro %}
```

{% sample lang='script' %}

### Script

```JS
import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|-------------------------------------------------------------------------------
| Регистрация состояния компонента
|-------------------------------------------------------------------------------
*/
store.registerModule('AboutBanner', {
  namespaced: true,

  state() {
    return {};
  },

  getters: {},

  mutations: {}
});

/*
|-------------------------------------------------------------------------------
| Объявление объекта опций компонента
|-------------------------------------------------------------------------------
*/
const AboutBanner = {
  computed: {
    ...mapState('AboutBanner', []),

    ...mapGetters('AboutBanner', [])
  },

  methods: {
    ...mapMutations('AboutBanner', [])
  }
};

Vue.component('AboutBanner', AboutBanner);

export default AboutBanner;
```

{% sample lang='style' %}

### Style

```SCSS
.about-banner {
  position: relative;
  width: 100%;
  padding: 0 10px 30px;

  @media screen and (--mobile) {
    padding: 0 0 20px;
  }
}

.about-banner__content-wrap {
  position: relative;
  width: 100%;
  max-width: 940px;
  margin: 0 auto;
  padding: 92px 0 64px;
  background-image: url('../images/about-banner.bgd.jpg');
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;

  @media screen and (--mobile) {
    padding: 30px 10px 50px;
  }
}

.about-banner__item {
  position: relative;
  display: flex;
  justify-content: space-between;

  @media screen and (--mobile) {
    display: block;
  }
}

.about-banner__image {
  display: block;
  width: 380px;
  transform: translateX(-10px);
  box-shadow: 0 5px 10px rgba(0, 0, 0, .5);

  @media screen and (--mobile) {
    width: 100%;
    margin-bottom: 30px;
    transform: none;
  }
}

.about-banner__caption {
  width: calc(100% - 400px);
  padding-right: 80px;
  color: #000;
  font-size: 18px;
  font-weight: 300;
  line-height: 28px;

  @media screen and (--mobile) {
    width: 100%;
    padding-right: 0;
    font-size: 16px;
    line-height: 24px;
  }
}

```

{% sample lang='test' %}

### Tests

```JS
import AboutBanner from './about-banner';

describe('AboutBanner', () => {
  it('компоненту передан объект опций', () => {
    expect(typeof AboutBanner).toBe('object');
  });
});
```

{% endmethod %}

{% endblock %}