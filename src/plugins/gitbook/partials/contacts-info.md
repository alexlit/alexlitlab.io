{% extends '../_layouts/custom.htm' %}

[//]: HEADER

{% block header %}

# ContactsInfo

{% endblock %}

[//]: COMPONENT

{% block component %}contacts-info{% endblock %}

[//]: API

{% block api %}

API отсутствует.

{% endblock %}

[//]: ASSETS

{% block assets %}

{% method %}

### Пример использования

```HTML
<contacts-info inline-template v-cloak>
  <section class="contacts-info">
    <address class="contacts-info__content-wrap">
      <div class="contacts-info__col">
        <p class="contacts-info__col-title">Телефон</p><a href="tel:+79005405050" class="contacts-info__phone">+7 (900) 540-50-50</a> <a href="#" class="contacts-info__download">Скачать Реквизиты</a></div>
      <div class="contacts-info__col">
        <p class="contacts-info__col-title">Адрес</p>
        <p class="contacts-info__paragraph">г. Москва,</p>
        <p class="contacts-info__paragraph">ул. Ленина 56</p>
      </div>
      <div class="contacts-info__col">
        <p class="contacts-info__col-title">Время работы</p>
        <p class="contacts-info__paragraph">09:00 - 18:00</p>
      </div>
      <div class="contacts-info__col">
        <p class="contacts-info__col-title">E-mail</p><a href="mailto:hello@vintel.ru" class="contacts-info__email">hello@vintel.ru</a></div>
    </address>
    <address class="contacts-info__map-wrap">
      <ui-spinner layout="chasing-dots" color="#215ed4" :size="40"></ui-spinner>
      <yandex-map :coords="[55.75399399999374,37.62209300000001]" zoom="14" style="width:100%;height:100%" :behaviors="['default']" :controls="['zoomControl']" map-type="map" @map-was-initialized="" :cluster-options="{1: {clusterDisableClickZoom: true}}" :placemarks="[{coords: [55.75399399999374,37.62209300000001],properties: {},clusterName: '1',callbacks: { click: function() {} }}]"></yandex-map>
    </address>
  </section>
</contacts-info>
```

{% sample lang='template' %}

### Template

```TWIG
{% macro ContactsInfo() %}
<!-- contacts-info -->
<contacts-info inline-template v-cloak>
  <section class="contacts-info">
    <address class="contacts-info__content-wrap">
      <div class="contacts-info__col">
        <p class="contacts-info__col-title">Телефон</p>
        <a href="tel:+79005405050" class="contacts-info__phone">+7 (900) 540-50-50</a>
        <a href="#" class="contacts-info__download">Скачать Реквизиты</a>
      </div>
      <div class="contacts-info__col">
        <p class="contacts-info__col-title">Адрес</p>
        <p class="contacts-info__paragraph">г. Москва, </p>
        <p class="contacts-info__paragraph">ул. Ленина 56 </p>
      </div>
      <div class="contacts-info__col">
        <p class="contacts-info__col-title">Время работы</p>
        <p class="contacts-info__paragraph">09:00 - 18:00</p>
      </div>
      <div class="contacts-info__col">
        <p class="contacts-info__col-title">E-mail</p>
        <a href="mailto:hello@vintel.ru" class="contacts-info__email">hello@vintel.ru</a>
      </div>
    </address>
    <address class="contacts-info__map-wrap">

      <ui-spinner
        layout="chasing-dots"
        color="#215ed4"
        :size="40"
      ></ui-spinner>

      <yandex-map
        :coords="[55.75399399999374,37.62209300000001]"
        zoom="14"
        style="width: 100%; height: 100%;"
        :behaviors="['default']"
        :controls="['zoomControl']"
        map-type="map"
        @map-was-initialized=""
        :cluster-options="{
          1: {
            clusterDisableClickZoom: true
          }
        }"
        :placemarks="[
          {
            coords: [55.75399399999374,37.62209300000001],
            properties: {},
            {# options: {
              iconLayout: 'default#image',
              iconImageHref: '/assets/images/ymaps.pin.svg',
              iconImageSize: [80, 94],
              iconImageOffset: [-80, -47]
            }, #}
            clusterName: '1',
            callbacks: { click: function() {} }
          }
        ]"
      ></yandex-map>

    </address>
  </section>
</contacts-info>
<!-- /contacts-info -->
{% endmacro %}
```

{% sample lang='script' %}

### Script

```JS
import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import { yandexMap, ymapMarker } from 'vue-yandex-maps';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|-------------------------------------------------------------------------------
| Регистрация состояния компонента
|-------------------------------------------------------------------------------
*/
store.registerModule('ContactsInfo', {
  namespaced: true,

  state() {
    return {};
  },

  getters: {},

  mutations: {}
});

/*
|-------------------------------------------------------------------------------
| Объявление объекта опций компонента
|-------------------------------------------------------------------------------
*/
const ContactsInfo = {
  components: { yandexMap, ymapMarker },

  computed: {
    ...mapState('ContactsInfo', []),

    ...mapGetters('ContactsInfo', [])
  },

  methods: {
    ...mapMutations('ContactsInfo', [])
  }
};

Vue.component('ContactsInfo', ContactsInfo);

export default ContactsInfo;

```

{% sample lang='style' %}

### Style

```SCSS
.contacts-info {
  position: relative;
  width: 100%;
  padding: 0 10px;
}

.contacts-info__content-wrap {
  position: relative;
  display: flex;
  justify-content: space-between;
  width: 100%;
  max-width: 940px;
  margin: 0 auto;
  margin-bottom: 55px;

  @media screen and (--mobile) {
    display: block;
    margin-bottom: 0;
  }
}

.contacts-info__col {
  width: calc((100% - 20px * 3) / 4);

  @media screen and (--mobile) {
    width: 100%;
    margin-bottom: 20px;
  }
}

.contacts-info__col-title {
  margin-bottom: 5px;
  letter-spacing: 1px;
  text-transform: uppercase;
  color: #000;
  font-size: 14px;
  font-weight: 700;
  line-height: 22px;
}

.contacts-info__phone {
  display: inline-block;
  margin-bottom: 20px;
  color: #000;
  font-size: 18px;
  font-weight: 300;
  line-height: 28px;

  @media screen and (--mobile) {
    margin-bottom: 10px;
  }
}

.contacts-info__download {
  display: block;
  letter-spacing: 1px;
  text-transform: uppercase;
  color: #215ed4;
  font-size: 14px;
  font-weight: 700;
  line-height: 22px;
}

.contacts-info__paragraph {
  color: #000;
  font-size: 18px;
  font-weight: 300;
  line-height: 28px;
}

.contacts-info__email {
  color: #000;
  font-size: 18px;
  font-weight: 300;
  line-height: 28px;
}

.contacts-info__map-wrap {
  position: relative;
  display: block;
  width: calc(100% + 20px);
  height: 480px;
  margin: 0 -10px;
}

```

{% sample lang='test' %}

### Tests

```JS
import ContactsInfo from './contacts-info';

describe('ContactsInfo', () => {
  it('компоненту передан объект опций', () => {
    expect(typeof ContactsInfo).toBe('object');
  });
});
```

{% endmethod %}

{% endblock %}