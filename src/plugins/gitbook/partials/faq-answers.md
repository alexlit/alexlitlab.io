{% extends '../_layouts/custom.htm' %}

[//]: HEADER

{% block header %}

# FaqAnswers

{% endblock %}

[//]: COMPONENT

{% block component %}faq-answers{% endblock %}

[//]: API

{% block api %}

API отсутствует.

{% endblock %}

[//]: ASSETS

{% block assets %}

{% method %}

### Пример использования

```HTML
<faq-answers initial-active-tab="Каковы условия аренды спецтехники?" inline-template v-cloak>
  <section class="faq-answers">
    <div class="faq-answers__content-wrap">
      <header class="faq-answers__title">Условия работы</header>
      <el-collapse class="faq-answers__el-collapse" v-model="activeTab" accordion>
        <el-collapse-item class="faq-answers__el-collapse-item" title="Каковы условия аренды спецтехники?" name="Каковы условия аренды спецтехники?">
          <p>Компания «Винтел» предоставляет услуги по аренде строительной спецтехники в Москве и МО.
            <br>На сайте подробно описаны возможности ТС и оборудования.
            <br>Менеджер бесплатно консультирует, вникая во все поставленные Вами задачи!
            <br>Так же вы можете обсудить проект непосредственно с архитектором.</p>
        </el-collapse-item>
        <el-collapse-item class="faq-answers__el-collapse-item" title="Какие формы оплаты принимаются?" name="00000">
          <p>Компания «Винтел» предоставляет услуги по аренде строительной спецтехники в Москве и МО.
            <br>На сайте подробно описаны возможности ТС и оборудования.
            <br>Менеджер бесплатно консультирует, вникая во все поставленные Вами задачи!
            <br>Так же вы можете обсудить проект непосредственно с архитектором.</p>
        </el-collapse-item>
        <el-collapse-item class="faq-answers__el-collapse-item" title="Возможно ли привлечение дополнительной техники после начала работ, при необходимости?" name="Возможно ли привлечение дополнительной техники после начала работ, при необходимости?">
          <p>Компания «Винтел» предоставляет услуги по аренде строительной спецтехники в Москве и МО.
            <br>На сайте подробно описаны возможности ТС и оборудования.
            <br>Менеджер бесплатно консультирует, вникая во все поставленные Вами задачи!
            <br>Так же вы можете обсудить проект непосредственно с архитектором.</p>
        </el-collapse-item>
        <el-collapse-item class="faq-answers__el-collapse-item" title="Имеются ли скидки для постоянных клиентов?" name="Имеются ли скидки для постоянных клиентов?">
          <p>Компания «Винтел» предоставляет услуги по аренде строительной спецтехники в Москве и МО.
            <br>На сайте подробно описаны возможности ТС и оборудования.
            <br>Менеджер бесплатно консультирует, вникая во все поставленные Вами задачи!
            <br>Так же вы можете обсудить проект непосредственно с архитектором.</p>
        </el-collapse-item>
        <el-collapse-item class="faq-answers__el-collapse-item" title="Кто несет ответственность за технику во время ее работы?" name="Кто несет ответственность за технику во время ее работы?">
          <p>Компания «Винтел» предоставляет услуги по аренде строительной спецтехники в Москве и МО.
            <br>На сайте подробно описаны возможности ТС и оборудования.
            <br>Менеджер бесплатно консультирует, вникая во все поставленные Вами задачи!
            <br>Так же вы можете обсудить проект непосредственно с архитектором.</p>
        </el-collapse-item>
      </el-collapse>
      <header class="faq-answers__title">Оплата и взаиморасчеты</header>
      <el-collapse class="faq-answers__el-collapse" accordion>
        <el-collapse-item class="faq-answers__el-collapse-item" title="Какие формы оплаты принимаются?" name="Какие формы оплаты принимаются?">
          <p>Компания «Винтел» предоставляет услуги по аренде строительной спецтехники в Москве и МО.
            <br>На сайте подробно описаны возможности ТС и оборудования.
            <br>Менеджер бесплатно консультирует, вникая во все поставленные Вами задачи!
            <br>Так же вы можете обсудить проект непосредственно с архитектором.</p>
        </el-collapse-item>
        <el-collapse-item class="faq-answers__el-collapse-item" title="Имеются ли скидки для постоянных клиентов?" name="Имеются ли скидки для постоянных клиентов?">
          <p>Компания «Винтел» предоставляет услуги по аренде строительной спецтехники в Москве и МО.
            <br>На сайте подробно описаны возможности ТС и оборудования.
            <br>Менеджер бесплатно консультирует, вникая во все поставленные Вами задачи!
            <br>Так же вы можете обсудить проект непосредственно с архитектором.</p>
        </el-collapse-item>
      </el-collapse>
    </div>
  </section>
</faq-answers>
```

{% sample lang='template' %}

### Template

```TWIG
{% macro FaqAnswers() %}
<!-- faq-answers -->
<faq-answers
  initial-active-tab="Каковы условия аренды спецтехники?"
  inline-template
  v-cloak
>
  <section class="faq-answers">
    <div class="faq-answers__content-wrap">

      <header class="faq-answers__title">Условия работы</header>
      <el-collapse
        class="faq-answers__el-collapse"
        v-model="activeTab"
        accordion
      >
        <el-collapse-item
          class="faq-answers__el-collapse-item"
          title="Каковы условия аренды спецтехники?"
          name="Каковы условия аренды спецтехники?"
        >
          <p>
            Компания «Винтел» предоставляет услуги по аренде строительной спецтехники в Москве и МО.
            <br>На сайте подробно описаны возможности ТС и оборудования.
            <br>Менеджер бесплатно консультирует, вникая во все поставленные Вами задачи!
            <br>Так же вы можете обсудить проект непосредственно с архитектором.
          </p>
        </el-collapse-item>
        <el-collapse-item
          class="faq-answers__el-collapse-item"
          title="Какие формы оплаты принимаются?"
          name="00000"
        >
          <p>
            Компания «Винтел» предоставляет услуги по аренде строительной спецтехники в Москве и МО.
            <br>На сайте подробно описаны возможности ТС и оборудования.
            <br>Менеджер бесплатно консультирует, вникая во все поставленные Вами задачи!
            <br>Так же вы можете обсудить проект непосредственно с архитектором.
          </p>
        </el-collapse-item>
        <el-collapse-item
          class="faq-answers__el-collapse-item"
          title="Возможно ли привлечение дополнительной техники после начала работ, при необходимости?"
          name="Возможно ли привлечение дополнительной техники после начала работ, при необходимости?"
        >
          <p>
            Компания «Винтел» предоставляет услуги по аренде строительной спецтехники в Москве и МО.
            <br>На сайте подробно описаны возможности ТС и оборудования.
            <br>Менеджер бесплатно консультирует, вникая во все поставленные Вами задачи!
            <br>Так же вы можете обсудить проект непосредственно с архитектором.
          </p>
        </el-collapse-item>
        <el-collapse-item
          class="faq-answers__el-collapse-item"
          title="Имеются ли скидки для постоянных клиентов?"
          name="Имеются ли скидки для постоянных клиентов?"
        >
          <p>
            Компания «Винтел» предоставляет услуги по аренде строительной спецтехники в Москве и МО.
            <br>На сайте подробно описаны возможности ТС и оборудования.
            <br>Менеджер бесплатно консультирует, вникая во все поставленные Вами задачи!
            <br>Так же вы можете обсудить проект непосредственно с архитектором.
          </p>
        </el-collapse-item>
        <el-collapse-item
          class="faq-answers__el-collapse-item"
          title="Кто несет ответственность за технику во время ее работы?"
          name="Кто несет ответственность за технику во время ее работы?"
        >
          <p>
            Компания «Винтел» предоставляет услуги по аренде строительной спецтехники в Москве и МО.
            <br>На сайте подробно описаны возможности ТС и оборудования.
            <br>Менеджер бесплатно консультирует, вникая во все поставленные Вами задачи!
            <br>Так же вы можете обсудить проект непосредственно с архитектором.
          </p>
        </el-collapse-item>
      </el-collapse>

      <header class="faq-answers__title">Оплата и взаиморасчеты</header>
      <el-collapse
        class="faq-answers__el-collapse"
        accordion
      >
        <el-collapse-item
          class="faq-answers__el-collapse-item"
          title="Какие формы оплаты принимаются?"
          name="Какие формы оплаты принимаются?"
        >
          <p>
            Компания «Винтел» предоставляет услуги по аренде строительной спецтехники в Москве и МО.
            <br>На сайте подробно описаны возможности ТС и оборудования.
            <br>Менеджер бесплатно консультирует, вникая во все поставленные Вами задачи!
            <br>Так же вы можете обсудить проект непосредственно с архитектором.
          </p>
        </el-collapse-item>
        <el-collapse-item
          class="faq-answers__el-collapse-item"
          title="Имеются ли скидки для постоянных клиентов?"
          name="Имеются ли скидки для постоянных клиентов?"
        >
          <p>
            Компания «Винтел» предоставляет услуги по аренде строительной спецтехники в Москве и МО.
            <br>На сайте подробно описаны возможности ТС и оборудования.
            <br>Менеджер бесплатно консультирует, вникая во все поставленные Вами задачи!
            <br>Так же вы можете обсудить проект непосредственно с архитектором.
          </p>
        </el-collapse-item>
      </el-collapse>

    </div>
  </section>
</faq-answers>
<!-- /faq-answers -->
{% endmacro %}
```

{% sample lang='script' %}

### Script

```JS
import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|-------------------------------------------------------------------------------
| Регистрация состояния компонента
|-------------------------------------------------------------------------------
*/
store.registerModule('FaqAnswers', {
  namespaced: true,

  state() {
    return {};
  },

  getters: {},

  mutations: {}
});

/*
|-------------------------------------------------------------------------------
| Объявление объекта опций компонента
|-------------------------------------------------------------------------------
*/
const FaqAnswers = {
  data() {
    return {
      activeTab: null
    };
  },

  props: ['initialActiveTab'],

  computed: {
    ...mapState('FaqAnswers', []),

    ...mapGetters('FaqAnswers', [])
  },

  methods: {
    ...mapMutations('FaqAnswers', [])
  },

  beforeMount() {
    this.activeTab = this.initialActiveTab;
  }
};

Vue.component('FaqAnswers', FaqAnswers);

export default FaqAnswers;

```

{% sample lang='style' %}

### Style

```SCSS
.faq-answers {
  position: relative;
  width: 100%;
  padding: 0 10px;
}

.faq-answers__content-wrap {
  position: relative;
  width: 100%;
  max-width: 940px;
  margin: 0 auto;
  padding-bottom: 50px;

  @media screen and (--mobile) {
    padding-bottom: 20px;
  }
}

.faq-answers__title {
  margin-bottom: 30px;
  letter-spacing: 1px;
  text-transform: uppercase;
  color: #000;
  font-size: 18px;
  font-weight: 500;

  @media screen and (--mobile) {
    margin-bottom: 20px;
  }
}

.faq-answers__el-collapse {
  &.el-collapse {
    margin-bottom: 50px;

    @media screen and (--mobile) {
      margin-bottom: 30px;
    }
  }
}

.faq-answers__el-collapse-item {
  &.el-collapse-item {
    &:not(:last-child) {
      margin-bottom: 10px;
    }

    & .el-collapse-item__header {
      position: relative;
      height: auto;
      padding: 25px 120px 25px 50px;
      transition-duration: .3s !important;
      transition-property: background-color, color;
      color: #000;
      border: none;
      background-color: #eee;
      font-size: 24px;
      font-weight: 500;
      line-height: 30px;

      @media screen and (--mobile) {
        padding: 20px 41px 20px 10px;
        font-size: 18px;
        line-height: 24px;
      }

      &.is-active {
      }

      &:hover {
        color: #fff;
        background-color: #588aeb;

        @media screen and (--mobile) {
          color: #000;
          background-color: #eee;
        }
      }
    }

    & .el-collapse-item__arrow {
      position: absolute;
      top: 0;
      right: 0;
      display: flex;
      align-items: center;
      justify-content: center;
      width: 80px;
      height: 80px;
      margin-right: 0;
      transform: rotate(90deg);

      @media screen and (--mobile) {
        width: 61px;
        height: 61px;
        font-weight: 700;
      }

      &.is-active {
        transform: rotate(-90deg);
      }

      &::before {
        font-size: 21px;
        line-height: 1;
      }
    }

    & .el-collapse-item__content {
      padding: 25px 80px 50px 50px;
      color: #000;
      border-bottom: none;
      background-color: #eee;
      font-size: 18px;
      font-weight: 300;
      line-height: 28px;

      @media screen and (--mobile) {
        padding: 0 10px 20px;
        font-size: 16px;
        line-height: 24px;
      }
    }
  }
}

```

{% sample lang='test' %}

### Tests

```JS
import FaqAnswers from './faq-answers';

describe('FaqAnswers', () => {
  it('компоненту передан объект опций', () => {
    expect(typeof FaqAnswers).toBe('object');
  });
});
```

{% endmethod %}

{% endblock %}