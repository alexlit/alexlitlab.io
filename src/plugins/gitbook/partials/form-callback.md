{% extends '../\_layouts/custom.htm' %}

[//]: HEADER

{% block header %}

# FormCallback

{% endblock %}

[//]: COMPONENT

{% block component %}form-callback{% endblock %}

[//]: API

{% block api %}

API отсутствует.

{% endblock %}

[//]: ASSETS

{% block assets %}

{% method %}

### Пример использования

```HTML
<form-callback action="/assets/forms/form-callback.php" inline-template v-cloak>
  <form class="form-callback" @submit.prevent="validateBeforeSubmit(scope);" :data-vv-scope="scope" :action="action" novalidate>
    <div class="form-callback__content-wrap">
      <div class="form-callback__content">
        <p class="form-callback__suptitle">Обратная связь</p>
        <p class="form-callback__title">Заказать обратный звонок</p>
        <div class="form-callback__conteiner">
          <transition enter-active-class="animated fadeIn" leave-active-class="animated fadeOut" duration="300" mode="out-in">
            <div class="form-callback__text" v-if="!success">
              <p class="form-callback__paragraph">Оставьте ваш номер телефона и мы перезвоним в кратчайшие сроки:</p>
              <div class="form-callback__field-wrap">
                <label for="" class="form-callback__label">Телефон:</label>
                <input :class="{'form-callback__input': true,'shake animated': shake && errors.has(scope + '.PHONE')}" v-model="formData.PHONE" data-inputmask="'mask': '+7 (999) 999-99-99'" placeholder="+7 (___) ___-__-__" name="PHONE" type="tel" @focus="inputmask();" v-validate.disable="{rules: {required: true,regex: /^\+7\s?\([0-9]{3}\)\s?[0-9]{3}-?[0-9]{2}-?[0-9]{2}$/}}" :data-invalid="errors.has(scope + '.PHONE')">
                <transition enter-active-class="animated fadeIn" leave-active-class="animated fadeOut" mode="out-in">
                  <p class="form-callback__policy-error" v-if="!agree && tries > 0">Вы не согласились с условиями обработки данных</p>
                </transition>
                <el-checkbox class="form-callback__el-checkbox" v-model="agree" v-validate.disable="'required'">Я согласен на обработку персональных данных и с условиями <a href="#" target="_blank">политики конфиденциальности</a></el-checkbox>
              </div>
              <button class="form-callback__button" type="send" @submit.prevent="validateBeforeSubmit(scope);">ОТПРАВИТЬ</button>
            </div>
            <figure class="form-callback__success" v-else>
              <message-success inline-template v-cloak>
                <section class="message-success">
                  <div class="message-success__content-wrap">
                    <header class="message-success__title">Ваш заказ оформлен!</header>
                    <p class="message-success__caption">Мы свяжемся с вами в самое ближайшее время для уточнения деталей заказа.</p>
                  </div>
                </section>
              </message-success>
            </figure>
          </transition>
        </div>
      </div>
    </div>
  </form>
</form-callback>
```

{% sample lang='template' %}

### Template

```TWIG
{% from 'components/message-success/message-success.htm' import MessageSuccess %}

{% macro FormCallback() %}
<!-- form-callback -->
<form-callback
  action="/assets/forms/form-callback.php"
  inline-template
  v-cloak
>
  <form
    class="form-callback"
    @submit.prevent="validateBeforeSubmit(scope);"
    :data-vv-scope="scope"
    :action="action"
    novalidate
  >
    <div class="form-callback__content-wrap">

      {# content #}
      <div class="form-callback__content">
        <p class="form-callback__suptitle">Обратная связь</p>
        <p class="form-callback__title">Заказать обратный звонок</p>
        <div class="form-callback__conteiner">
          <transition
            enter-active-class="animated fadeIn"
            leave-active-class="animated fadeOut"
            duration="300"
            mode="out-in"
          >

            {# text #}
            <div class="form-callback__text" v-if="!success">
              <p class="form-callback__paragraph">Оставьте ваш номер телефона и мы перезвоним в кратчайшие сроки:</p>
              <div class="form-callback__field-wrap">
                <label for="" class="form-callback__label">Телефон:</label>
                <input
                  :class="{
                    'form-callback__input': true,
                    'shake animated': shake && errors.has(scope + '.PHONE')
                  }"
                  v-model="formData.PHONE"
                  data-inputmask="'mask': '+7 (999) 999-99-99'"
                  placeholder="+7 (___) ___-__-__"
                  name="PHONE"
                  type="tel"
                  @focus="inputmask();"
                  v-validate.disable="{
                    rules: {
                      required: true,
                      regex: /^\+7\s?\([0-9]{3}\)\s?[0-9]{3}-?[0-9]{2}-?[0-9]{2}$/
                    }
                  }"
                  :data-invalid="errors.has(scope + '.PHONE')"
                >
                <transition
                  enter-active-class="animated fadeIn"
                  leave-active-class="animated fadeOut"
                  mode="out-in"
                >
                  <p
                    class="form-callback__policy-error"
                    v-if="!agree && tries > 0"
                  >Вы не согласились с условиями обработки данных</p>
                </transition>
                <el-checkbox
                  class="form-callback__el-checkbox"
                  v-model="agree"
                  v-validate.disable="'required'"
                >Я согласен на обработку персональных данных и с условиями <a href="#" target="_blank">политики конфиденциальности</a></el-checkbox>
              </div>
              <button
                class="form-callback__button"
                type="send"
                @submit.prevent="validateBeforeSubmit(scope);"
              >ОТПРАВИТЬ</button>
            </div>
            {# /text #}

            {# success #}
            <figure class="form-callback__success" v-else>

              {{ MessageSuccess() }}

            </figure>
            {# /success #}

          </transition>
        </div>
      </div>
      {# /content #}
    </div>
  </form>
</form-callback>

<!-- /form-callback -->
{% endmacro %}
```

{% sample lang='script' %}

### Script

```JS
import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import 'animate.css/animate.css';
import axios from 'axios';
import qs from 'qs';
import Inputmask from 'inputmask/dist/inputmask/inputmask';
import VeeValidate from 'vee-validate';
import store from '../app/app.store';
import './form-callback.php';

Vue.use(VeeValidate, {
  inject: false
});

Vue.config.productionTip = false;

/*
|-------------------------------------------------------------------------------
| Регистрация состояния компонента
|-------------------------------------------------------------------------------
*/
store.registerModule('FormCallback', {
  namespaced: true,

  state() {
    return {
      useUrlEncode: true,
      success: false,
      shake: false,
      tries: 0,
      formData: {}
    };
  },

  getters: {},

  mutations: {
    /**
     * Установка начальных значений формы
     * @param {*} state
     * @param {Object} payload - требуемые значения
     */
    setFormData(state, payload) {
      state.formData = { ...state.formData, ...payload };
    },

    /**
     * Счетчик попыток отправить форму
     * @param {*} state
     */
    incrementTries(state) {
      state.tries += 1;
      state.shake = true;
      setTimeout(() => {
        state.shake = false;
      }, 1000);
    },

    /**
     * Установка состояния "успешная отправка"
     * @param {*} state
     * @param {Boolean} payload
     */
    setSuccessState(state, payload) {
      state.success = payload;
    }
  }
});

/*
|-------------------------------------------------------------------------------
| Объявление объекта опций компонента
|-------------------------------------------------------------------------------
*/
const FormCallback = {
  data() {
    return {
      agree: false,
      scope: Math.random()
        .toString(36)
        .substring(2)
    };
  },

  props: {
    action: {
      type: String,
      default: '/assets/forms/form-callback.php'
    }
  },

  computed: {
    ...mapState('FormCallback', [
      'useUrlEncode',
      'success',
      'shake',
      'tries',
      'formData'
    ]),

    ...mapGetters('FormCallback', [])
  },

  inject: {
    $validator: '$validator'
  },

  methods: {
    ...mapMutations('FormCallback', ['incrementTries', 'setSuccessState']),

    /**
     * Валидация формы и отправка данных
     */
    validateBeforeSubmit(scope) {
      const vm = this;

      vm.$validator
        .validateAll(scope)
        .then(response => {
          if (response && vm.agree) {
            const post = vm.useUrlEncode
              ? qs.stringify(vm.formData)
              : vm.formData;

            axios
              .post(vm.action, post)
              .then(serverResponse => {
                vm.setSuccessState(true);
                console.info(`Отправлены данные:`);
                console.info(post);
                console.info(`Ответ сервера:`);
                console.info(serverResponse);
              })
              .catch(error => {
                console.error(`Ошибка сервера:`);
                console.info(error);
              });
          } else {
            vm.incrementTries();
          }
        })
        .catch(error => {
          console.log(error);
        });
    },

    /**
     * Подключение маски
     */
    inputmask() {
      const vm = this;
      Inputmask({ showMaskOnHover: false }).mask(
        vm.$el.querySelectorAll('[data-inputmask]')
      );
    }
  }
};

Vue.component('FormCallback', FormCallback);

export default FormCallback;

```

{% sample lang='style' %}

### Style

```SCSS
.form-callback {
  position: relative;
  width: 100%;
}

.form-callback__content-wrap {
  position: relative;
  width: 100%;
  max-width: 940px;
  margin: 0 auto;
}

.form-callback__content {
  position: relative;
  width: 100%;
}

.form-callback__suptitle {
  margin-bottom: 10px;
  letter-spacing: 1px;
  text-transform: uppercase;
  color: #000;
  font-size: 14px;
  font-weight: 700;
  line-height: 28px;
}

.form-callback__title {
  margin-bottom: 30px;
  color: #000;
  font-size: 36px;
  font-weight: 700;
  line-height: 42px;

  @media screen and (--mobile) {
    margin-bottom: 20px;
  }
}

.form-callback__conteiner {
  display: flex;
  justify-content: space-between;
  width: 100%;
}

.form-callback__text {
  width: 100%;
}

.form-callback__paragraph {
  margin-bottom: 30px;
  color: #000;
  font-size: 18px;
  font-weight: 400;
  line-height: 22px;
}

.form-callback__field-wrap {
  position: relative;
  width: 100%;
  margin-bottom: 20px;
}

.form-callback__label {
  display: block;
  margin-bottom: 10px;
  letter-spacing: 1px;
  text-transform: uppercase;
  color: #000;
  font-size: 12px;
  font-weight: 500;
  line-height: 18px;
}

.form-callback__input {
  width: 100%;
  max-width: 480px;
  height: 80px;
  margin-bottom: 20px;
  padding: 0 20px;
  color: #000;
  border-bottom: 3px solid #fc0;
  background-color: #f5f5f5;
  font-size: 30px;
  font-weight: 300;
}

.form-callback__policy-error {
  margin-bottom: 20px;
  animation-duration: .4s;
  color: #F44336;
  font-size: 14px;
}

.form-callback__el-checkbox {
  &.el-checkbox {
    max-width: 480px;
  }
}

.form-callback__button {
  padding: 18px 21px;
  text-transform: uppercase;
  color: #000;
  border-radius: 2px;
  background-color: #fc0;
  font-size: 20px;
  font-weight: 700;

  &:hover {
    background-color: #ffe500;
  }
}

.form-callback__success {
  position: relative;
}

```

{% sample lang='test' %}

### Tests

```JS
import FormCallback from './form-callback';

describe('FormCallback', () => {
  it('компоненту передан объект опций', () => {
    expect(typeof FormCallback).toBe('object');
  });
});
```

{% sample lang='serverside' %}

### ServerSide

```php
<?php

require __DIR__ . '/../../plugins/PHPMailer/src/PHPMailer.php';
require __DIR__ . '/../../plugins/PHPMailer/src/Exception.php';

use PHPMailer\PHPMailer\PHPMailer;

// Получаем данные в зависимости от типа
$json = json_decode(file_get_contents('php://input'), TRUE);
$post = $json ? $json : $_POST;

// Инициализируем объект
$mail = new PHPMailer();
$mail->CharSet = 'utf-8';
$mail->setFrom('noreply@vintel.ru','Винтел | Спецтехника');
$mail->addAddress('ljazzmail@gmail.com');
// $mail->addAttachment(__DIR__.'/../../'.$post['IMAGE']);
$mail->isHTML(TRUE);
$mail->Subject = 'Заказ обратного звонка: '.$post['TITLE'];
$mail->Body = '
<html>
  <head>
    <title>'.$mail->Subject.'</title>
  </head>
  <body>
    <p>Телефон:'.$post['PHONE'].'</p>
  </body>
</html>';
$mail->send();

// Запись данных в файл для дебага
$fileData = fopen(basename(__FILE__, '.php') . '.response.md', 'w');

fputs($fileData, print_r("# MESSAGE:\r\n\r\n", TRUE));
fputs($fileData, print_r("```html\r\n", TRUE));
fputs($fileData, print_r($mail->Body, TRUE));
fputs($fileData, print_r("\r\n```\r\n\r\n", TRUE));

fputs($fileData, print_r("## POST:\r\n\r\n", TRUE));
fputs($fileData, print_r("```php\r\n", TRUE));
fputs($fileData, print_r($post, TRUE));
fputs($fileData, print_r("```\r\n\r\n", TRUE));

fputs($fileData, print_r("## FILES:\r\n\r\n", TRUE));
fputs($fileData, print_r("```php\r\n", TRUE));
fputs($fileData, print_r($_FILES, TRUE));
fputs($fileData, print_r("```\r\n\r\n", TRUE));

fputs($fileData, print_r("### REQUEST:\r\n\r\n", TRUE));
fputs($fileData, print_r("```php\r\n", TRUE));
fputs($fileData, print_r($_REQUEST, TRUE));
fputs($fileData, print_r("```\r\n\r\n", TRUE));

fclose($fileData);

// Возвращаем ответ
var_dump($post);

?>

```

{% endmethod %}

{% endblock %}
