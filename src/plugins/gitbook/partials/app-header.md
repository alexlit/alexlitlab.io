{% extends '../_layouts/custom.htm' %}

[//]: HEADER

{% block header %}

# AppHeader

{% endblock %}

[//]: COMPONENT

{% block component %}app-header{% endblock %}

[//]: API

{% block api %}

API отсутствует.

{% endblock %}

[//]: ASSETS

{% block assets %}

{% method %}

### Пример использования

```HTML
<app-header inline-template v-cloak>
  <section class="app-header">
    <div class="app-header__content-wrap">
      <a class="app-header__logo" href="/">
        <img class="app-header__logo-image" :src="$mq !== 'mobile'? '/assets/images/app-header.logo.svg': '/assets/images/app-header.logo@mobile.svg'" alt="">
      </a>
      <div class="app-header__menus-wrap" :data-hidden="$mq === 'mobile' && !showMobileMenu">
        <address class="app-header__contacts" v-if="$mq ==='mobile'"><a href="tel:" class="app-header__phone">8 (495) 000-00-00</a>
          <button class="app-header__calback-button" type="button" @click="setSuccessState(false);showModal('form-callback');">Перезвоните мне</button> <a href="maito:zakaz@bin-tel.ru" class="app-header__email">zakaz@bin-tel.ru</a></address>
        <nav class="app-header__menu">
          <a :class="{'app-header__menu-item': true,'app-header__menu-item--active':false}" href="about.html">
            <span class="app-header__menu-item-label">О компании</span>
          </a>
          <a :class="{'app-header__menu-item': true,'app-header__menu-item--active':false}" href="faq.html">
            <span class="app-header__menu-item-label">Вопрос-ответ</span>
          </a>
          <a :class="{'app-header__menu-item': true,'app-header__menu-item--active':false}" href="content-page.html">
            <span class="app-header__menu-item-label">Условия аренды</span>
          </a>
          <a :class="{'app-header__menu-item': true,'app-header__menu-item--active':false}" href="contacts.html">
            <span class="app-header__menu-item-label">Контакты</span>
          </a>
          <a :class="{'app-header__menu-item': true,'app-header__menu-item--active':false}" href="http://vin-tel.ru" target="_blank">
            <span class="app-header__menu-item-label">Вентиляция</span>
          </a>
        </nav>
        <div class="app-header__app-menu-wrap" v-if="$mq === 'mobile'">
          <app-menu inline-template v-cloak>
            <nav class="app-menu">
              <div class="app-menu__content-wrap"><a :class="{ 'app-menu__item': true, 'app-menu__item--active': false }" href="#">Автокраны</a> <a :class="{ 'app-menu__item': true, 'app-menu__item--active': false }" href="#">Самосвалы</a> <a :class="{ 'app-menu__item': true, 'app-menu__item--active': false }" href="#">Экскаваторы</a> <a :class="{ 'app-menu__item': true, 'app-menu__item--active': false }" href="#">Катки</a> <a :class="{ 'app-menu__item': true, 'app-menu__item--active': false }" href="#">Манипуляторы</a> <a :class="{ 'app-menu__item': true, 'app-menu__item--active': false }" href="#">Бульдозеры</a> <a :class="{ 'app-menu__item': true, 'app-menu__item--active': false }" href="#">Тралы</a> <a :class="{ 'app-menu__item': true, 'app-menu__item--active': false }" href="#">Погрузчики</a> <a class="app-menu__readmore" href="#" v-if="$mq !== 'mobile'">Вся техника</a> <a :class="{ 'app-menu__item': true, 'app-menu__item--active': false }" href="#" v-if="$mq === 'mobile'">Борт. полуприцепы</a> <a :class="{ 'app-menu__item': true, 'app-menu__item--active': false }" href="#" v-if="$mq === 'mobile'">ФРЕЗА</a> <a :class="{ 'app-menu__item': true, 'app-menu__item--active': false }" href="#" v-if="$mq === 'mobile'">Бетононасос</a> <a :class="{ 'app-menu__item': true, 'app-menu__item--active': false }" href="#" v-if="$mq === 'mobile'">Бетономешалки</a> <a :class="{ 'app-menu__item': true, 'app-menu__item--active': false }" href="#" v-if="$mq === 'mobile'">Асфальтоукладчики</a></div>
            </nav>
          </app-menu>
        </div><a href="http://krown.cc" class="app-header__developer" target="_blank" v-if="$mq === 'mobile'">Разработка и поддержка</a></div>
      <address class="app-header__contacts" v-if="$mq !=='mobile'"><a href="tel:" class="app-header__phone">8 (495) 000-00-00</a>
        <button class="app-header__calback-button" type="button" @click="setSuccessState(false);showModal('form-callback');">Перезвоните мне</button> <a href="maito:zakaz@bin-tel.ru" class="app-header__email">zakaz@bin-tel.ru</a></address>
      <button :class="{'app-header__hamburger': true,'app-header__hamburger--close': showMobileMenu,}" type="button" v-if="$mq === 'mobile'" @click="showMobileMenu = !showMobileMenu"></button>
    </div>
  </section>
</app-header>
```

{% sample lang='template' %}

### Template

```TWIG
{% from 'components/app-menu/app-menu.htm' import AppMenu %}

{% macro appHeaderContacts( vIf = "$mq !=='mobile'" ) %}
<address class="app-header__contacts" v-if="{{ vIf | safe }}">
  <a href="tel:" class="app-header__phone">8 (495) 000-00-00</a>
  <button
    class="app-header__calback-button"
    type="button"
    @click="
      setSuccessState(false);
      showModal('form-callback');
    "
  >Перезвоните мне</button>
  <a href="maito:zakaz@bin-tel.ru" class="app-header__email">zakaz@bin-tel.ru</a>
</address>
{% endmacro %}

{% macro AppHeader(
  activeItem = null
) %}
<!-- app-header -->
<app-header inline-template v-cloak>
  <section class="app-header">
    <div class="app-header__content-wrap">
      <a
        class="app-header__logo"
        href="/"
      >
        <img
        class="app-header__logo-image"
        :src="
          $mq !== 'mobile'
            ? '/assets/images/app-header.logo.svg'
            : '/assets/images/app-header.logo@mobile.svg'
        "
        alt=""
      >
      </a>
      <div
        class="app-header__menus-wrap"
        :data-hidden="$mq === 'mobile' && !showMobileMenu"
      >

        {{ appHeaderContacts( vIf = "$mq ==='mobile'" )}}

        <nav class="app-header__menu">

          {% for item in [
            { title: 'О компании', href: "about.html"},
            { title: 'Вопрос-ответ', href: "faq.html"},
            { title: 'Условия аренды', href: "content-page.html"},
            { title: 'Контакты', href: "contacts.html"},
            { title: 'Вентиляция', href: "http://vin-tel.ru", target: "_blank"}
          ]  %}

            <a
              :class="{
                'app-header__menu-item': true,
                'app-header__menu-item--active': {% if item.title == activeItem %} true {% else %} false {% endif %}
              }"
              href="{{ item.href }}"
              {% if item.target %}target="{{ item.target }}"{% endif %}
            >
              <span class="app-header__menu-item-label">{{ item.title }}</span>
            </a>

          {% endfor %}

        </nav>
        <div class="app-header__app-menu-wrap" v-if="$mq === 'mobile'">

          {{ AppMenu() }}

        </div>
        <a
          href="http://krown.cc"
          class="app-header__developer"
          target="_blank"
          v-if="$mq === 'mobile'"
        >Разработка и поддержка</a>
      </div>

      {{ appHeaderContacts( vIf = "$mq !=='mobile'" )}}

      <button
        :class="{
          'app-header__hamburger': true,
          'app-header__hamburger--close': showMobileMenu,
        }"
        type="button"
        v-if="$mq === 'mobile'"
        @click="showMobileMenu = !showMobileMenu"
      ></button>
    </div>
  </section>
</app-header>
<!-- /app-header -->
{% endmacro %}

```

{% sample lang='script' %}

### Script

```JS
import Vue from 'vue/dist/vue';
import { mapState, mapGetters, mapMutations } from 'vuex';
import store from '../app/app.store';

Vue.config.productionTip = false;

/*
|-------------------------------------------------------------------------------
| Регистрация состояния компонента
|-------------------------------------------------------------------------------
*/
store.registerModule('AppHeader', {
  namespaced: true,

  state() {
    return {};
  },

  getters: {},

  mutations: {}
});

/*
|-------------------------------------------------------------------------------
| Объявление объекта опций компонента
|-------------------------------------------------------------------------------
*/
const AppHeader = {
  data() {
    return {
      showMobileMenu: false
    };
  },

  watch: {
    showMobileMenu(newValue) {
      this.$root.noScroll = newValue;
    },
    $mq(newValue) {
      if (newValue !== 'mobile') {
        this.$root.noScroll = false;
      }
    }
  },

  computed: {
    ...mapState('AppHeader', []),
    ...mapState('App', ['modal']),

    ...mapGetters('AppHeader', [])
  },

  methods: {
    ...mapMutations('AppHeader', []),
    ...mapMutations('App', ['showModal']),
    ...mapMutations('FormCallback', ['setSuccessState'])
  }
};

Vue.component('AppHeader', AppHeader);

export default AppHeader;

```

{% sample lang='style' %}

### Style

```SCSS
.app-header {
  position: relative;
  z-index: 100;
  width: 100%;
  padding: 0 10px;

  @media screen and (--mobile) {
    background-color: #245cbb;
  }
}

.app-header__content-wrap {
  position: relative;
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  max-width: 940px;
  margin: 0 auto;
  padding: 20px 0;
  border-bottom: 1px solid color(#eee alpha(.2));

  @media screen and (--mobile) {
    padding: 0;
    border-bottom: none;
    background-color: #245cbb;
    box-shadow: -10px 0 0 #245cbb, 10px 0 0 #245cbb;
  }
}

.app-header__logo {
  display: flex;
  align-items: center;
  width: 166px;
  height: 65px;
  margin-right: 30px;

  @media screen and (--mobile) {
    width: 138px;
    height: 76px;
    margin-right: 0;
  }
}

.app-header__logo-image {
  display: block;
  max-width: 100%;
  max-height: 100%;
}

.app-header__menus-wrap {
  position: relative;
  transition-duration: .4s;
  transform: translateY(0);

  @media screen and (--mobile) {
    position: absolute;
    z-index: -1;
    top: 76px;
    left: -10px;
    overflow: auto;
    width: calc(100% + 20px);
    height: calc(100vh - 76px);
    padding-bottom: 100px;
    background-color: #245cbb;
    -webkit-overflow-scrolling: touch;
  }

  &[data-hidden] {
    transform: translateY(-100%);
  }
}

.app-header__menu {
  position: relative;
  display: flex;

  @media screen and (--mobile) {
    display: block;
    padding-bottom: 13px;

    &::after {
      position: absolute;
      bottom: 0;
      left: 10px;
      display: block;
      width: calc(100% - 20px);
      height: 1px;
      content: '';
      background-color: #3375e0;
    }
  }
}

.app-header__menu-item {
  display: block;
  display: flex;
  align-items: center;
  height: 65px;
  letter-spacing: .86px;
  text-transform: uppercase;
  color: #fff;
  font-size: 12px;
  font-weight: 500;

  @media screen and (--mobile) {
    height: 40px;
    padding: 0 10px;
  }

  &:hover {
    color: #fc0;
  }

  &:not(:last-child) {
    padding-right: 25px;

    @media screen and (--mobile) {
      padding-right: 10px;
    }
  }

  &--active {
    pointer-events: none;
    color: #fc0;
  }
}

.app-header__menu-item-label {
  position: relative;
  line-height: 14px;

  &::after {
    position: absolute;
    bottom: -6px;
    left: 0;
    display: block;
    width: 20px;
    height: 2px;
    content: '';
    transition-duration: .4s;
    background-color: #eb5a34;

    @nest .app-header__menu-item--active .app-header__menu-item-label& {
      width: 100%;
    }

    @nest .app-header__menu-item:last-child .app-header__menu-item-label& {
      opacity: 0;
    }
  }
}

.app-header__app-menu-wrap {
  position: relative;
  width: 100%;
  margin-bottom: 51px;
  padding-top: 10px;

  & .app-menu {
    padding: 0;

    & .app-menu__content-wrap {
      display: block;
    }

    & .app-menu__item {
      padding: 0 10px;
      line-height: 34px;
    }
  }
}

.app-header__contacts {
  position: relative;
  display: flex;
  align-items: flex-end;
  flex-direction: column;
  margin-left: auto;
  transform: translateY(10px);

  @media screen and (--mobile) {
    align-items: flex-start;
    margin-bottom: 23px;
    padding-left: 10px;
    transform: none;
  }
}

.app-header__phone {
  margin-bottom: 5px;
  color: #fff;
  font-size: 18px;
  font-weight: 700;
  line-height: 21px;

  @media screen and (--mobile) {
    margin-bottom: 10px;
    font-size: 24px;
    line-height: 30px;
  }

  &:hover {
    color: #fc0;
  }
}

.app-header__calback-button {
  margin-bottom: 5px;
  letter-spacing: 1px;
  text-transform: uppercase;
  color: #fff;
  font-size: 12px;
  font-weight: 500;
  line-height: 14px;

  @media screen and (--mobile) {
    margin-bottom: 10px;
    color: #fc0;
  }

  &:hover {
    color: #fc0;
  }
}

.app-header__email {
  display: block;
  display: inline-block;
  letter-spacing: 1px;
  text-transform: uppercase;
  color: #fff;
  font-size: 12px;
  font-weight: 500;
  line-height: 14px;

  &:hover {
    color: #fc0;
  }
}

.app-header__hamburger {
  position: absolute;
  top: 0;
  right: -10px;
  width: 56px;
  height: 76px;
  transition-duration: 0s;
  background-image: url('../images/app-header.hamburger.svg');
  background-repeat: no-repeat;
  background-position: center;
  background-size: 24px auto;

  &--close {
    background-image: url('../images/app-header.close.svg');
  }
}

.app-header__developer {
  display: block;
  padding-bottom: 36px;
  padding-left: 10px;
  color: #fff;
  background-image: url('../images/app-footer.developer.svg');
  background-repeat: no-repeat;
  background-position: 10px bottom;
  font-size: 16px;
  line-height: 24px;

  &:hover {
    color: #fc0;
  }
}

```

{% sample lang='test' %}

### Tests

```JS
import AppHeader from './app-header';

describe('AppHeader', () => {
  it('компоненту передан объект опций', () => {
    expect(typeof AppHeader).toBe('object');
  });
});
```

{% endmethod %}

{% endblock %}