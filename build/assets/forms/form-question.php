<?php

    // Получаем данные в зависимости от типа

    $json = json_decode(file_get_contents('php://input'), true);
    $post = $json ? $json : $_POST ;

    // Формируем письмо

    $to      = 'alex.lit@outlook.com';
    $subject = 'Вопрос';
    $message =
'<html>
    <head>
        <title>'.$subject.'</title>
    </head>
    <body>
        <p>Email: '.$post['EMAIL'].'</p>
        <p>Вопрос: '.$post['MESSAGE'].'</p>
    </body>
</html>';
    $headers  = "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=utf-8\r\n";
    $headers .= "From: noreply@alexlit.gitlab.io\r\n";
    mail($to, $subject, $message, $headers);

    // Запись данных в файл для дебага

    $fileData = fopen(basename(__FILE__, '.php').'.response.md', 'w');

    fputs($fileData, print_r("# MESSAGE:\r\n\r\n", true));
    fputs($fileData, print_r("```html\r\n", true));
    fputs($fileData, print_r($message, true));
    fputs($fileData, print_r("\r\n```\r\n\r\n", true));

    fputs($fileData, print_r("## POST:\r\n\r\n", true));
    fputs($fileData, print_r("```php\r\n", true));
    fputs($fileData, print_r($post, true));
    fputs($fileData, print_r("```\r\n\r\n", true));

    fputs($fileData, print_r("## FILES:\r\n\r\n", true));
    fputs($fileData, print_r("```php\r\n", true));
    fputs($fileData, print_r($_FILES, true));
    fputs($fileData, print_r("```\r\n\r\n", true));

    fputs($fileData, print_r("### REQUEST:\r\n\r\n", true));
    fputs($fileData, print_r("```php\r\n", true));
    fputs($fileData, print_r($_REQUEST, true));
    fputs($fileData, print_r("```\r\n\r\n", true));

    fclose($fileData);

    // Возвращаем ответ

    echo var_dump($post);

?>
