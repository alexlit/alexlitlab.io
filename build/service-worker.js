// we'll version our cache
const cacheName = 'v1::static';

window.self.addEventListener('install', event => {
  // once the SW is installed, go ahead and fetch the resources
  // to make this work offline
  event.waitUntil(
    caches
      .open(cacheName)
      .then(cache => cache.addAll(['/']).then(() => window.self.skipWaiting()))
  );
});

// when the browser fetches a url, either response with
// the cached object or go ahead and fetch the actual url
window.self.addEventListener('fetch', event => {
  event.respondWith(
    // ensure we check the *right* cache to match against
    caches
      .open(cacheName)
      .then(cache =>
        cache
          .match(event.request)
          .then(response => response || fetch(event.request))
      )
  );
});
