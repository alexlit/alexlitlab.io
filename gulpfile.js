/*
|-------------------------------------------------------------------------------
| PLUGINS
|-------------------------------------------------------------------------------
| Подключение плагинов для таск раннера
*/
const base64 = require('gulp-base64');
const browserSync = require('browser-sync').create();
const concat = require('gulp-concat');
const empty = require('gulp-empty');
const fileinclude = require('gulp-file-include');
const flatten = require('gulp-flatten');
const ftp = require('vinyl-ftp');
const getSlug = require('speakingurl');
const gulp = require('gulp');
const gutil = require('gulp-util');
const htmlbeautify = require('gulp-html-beautify');
const htmlmin = require('gulp-htmlmin');
const imagemin = require('gulp-imagemin');
const inject = require('gulp-inject-string');
const njkRender = require('gulp-nunjucks-render');
const plumber = require('gulp-plumber');
const postcss = require('gulp-postcss');
const rename = require('gulp-rename');
const rimraf = require('rimraf');
const sitemap = require('gulp-sitemap');
const sourcemaps = require('gulp-sourcemaps');
const typograf = require('gulp-typograf');
const webpack = require('webpack');
const webpackStream = require('webpack-stream');

/*
|-------------------------------------------------------------------------------
| PROJECT
|-------------------------------------------------------------------------------
| Конфиг плагинов и пути до исходников проекта
*/
const PROJECT = {
  resources: {
    css: ['./src/components/app/*.css', './src/components/**/*.css'],
    files: ['./src/components/**/files/*.*'],
    fonts: ['./src/components/**/fonts/**/*.*'],
    htm: ['./src/pages/**/*.htm'],
    icons: ['./src/components/**/icons/*.*'],
    images: './src/components/**/images/*.*',
    root: ['./src/*.*', './src/.htaccess'],
    plugins: ['./src/plugins/**', '!./src/plugins/gitbook/**'],
    sounds: ['./src/components/**/sounds/*.*'],
    videos: ['./src/components/**/videos/*.*'],
    styleguide: {
      htm: ['./src/components/**/*.spec.htm'],
      md: ['./src/components/**/*.spec.md']
    }
  },

  plugins: {
    postcss: [
      require('cq-prolyfill/postcss-plugin')(),
      require('postcss-for'),
      require('postcss-each'),
      require('postcss-nested'),
      require('postcss-object-fit-images'),
      require('postcss-input-range')(),
      require('postcss-cssnext')(),
      require('css-mqpacker')
    ],

    base64: {
      baseDir: './build/assets/styles/',
      maxImageSize: 8 * 1024 * 10,
      debug: true,
      exclude: [/fonts/]
    },

    imagemin: [
      imagemin.gifsicle({ interlaced: true }),
      imagemin.jpegtran({ progressive: true }),
      imagemin.optipng({ optimizationLevel: 5 }),
      imagemin.svgo({
        plugins: [{ removeViewBox: true }, { cleanupIDs: true }]
      })
    ],

    njkRender: {
      path: './src/'
    },

    htmlmin: {
      removeComments: process.env.NODE_ENV !== 'dev',
      collapseWhitespace: true,
      preserveLineBreaks: process.env.NODE_ENV === 'dev',
      customAttrCollapse: /(v-|@|:)/,
      minifyCSS: true,
      minifyJS: true
    },

    htmlBeautify: {
      indent_size: 2,
      indent_char: ' ',
      eol: '\n',
      indent_level: 0,
      indent_with_tabs: false,
      preserve_newlines: true,
      max_preserve_newlines: 0,
      jslint_happy: false,
      space_after_anon_function: false,
      brace_style: 'collapse',
      keep_array_indentation: false,
      keep_function_indentation: false,
      space_before_conditional: true,
      break_chained_methods: false,
      eval_code: false,
      unescape_strings: false,
      wrap_line_length: 0,
      wrap_attributes_indent_size: 2,
      end_with_newline: false,
      unformatted: 'a'
    },

    typograf: {
      locale: ['ru', 'en-US'],
      htmlEntity: { type: 'default' },
      processingSeparateParts: false,
      safeTags: [
        ['<\\?php', '\\?>'],
        ['\\[\\[', '\\]\\]'],
        ['\\{\\{', '\\}\\}'],
        ['<no-typography>', '</no-typography>']
      ]
    }
  }
};

if (process.env.NODE_ENV === 'build') {
  PROJECT.plugins.postcss.push(
    require('cssnano')({
      discardComments: {
        removeAll: true
      },
      safe: true,
      autoprefixer: false,
      zindex: false
    })
  );
}

/*
|-------------------------------------------------------------------------------
| RemoveBuild
|-------------------------------------------------------------------------------
| Удаление директории сборки
*/
gulp.task(
  'removeBuild',
  gulp.series(done =>
    rimraf('./build', () => {
      console.info('Удалена директория "./build"');
      done();
    })
  )
);

/*
|-------------------------------------------------------------------------------
| HTM
|-------------------------------------------------------------------------------
| Сборка разметки
*/
gulp.task(
  'htm',
  gulp.series(
    /**
     * Сборка страниц
     */
    () => {
      console.info('Сборка разметки...');
      return gulp
        .src(PROJECT.resources.htm)
        .pipe(process.env.NODE_ENV === 'dev' ? plumber() : empty())
        .pipe(njkRender(PROJECT.plugins.njkRender))
        .pipe(htmlmin(PROJECT.plugins.htmlmin))
        .pipe(
          process.env.NODE_ENV === 'dev'
            ? htmlbeautify(PROJECT.plugins.htmlBeautify)
            : empty()
        )
        .pipe(
          rename(path => {
            path.basename = getSlug(path.basename, {
              mark: true
            });
          })
        )
        .pipe(typograf(PROJECT.plugins.typograf))
        .pipe(gulp.dest('./build/'));
    },
    /**
     * Генерация sitemap
     */
    done => {
      console.info('Генерация sitemap...');
      gulp
        .src(['./build/**/*.html', '!./build/styleguide/**/*.html'], {
          read: false
        })
        .pipe(
          sitemap({
            siteUrl: 'http://alexlit.gitlab.io'
          })
        )
        .pipe(gulp.dest('./build/'));
      done();
    }
  )
);

/*
|------------------------------------------------------------------------------
| IMAGES
|------------------------------------------------------------------------------
| Обработка изображений
*/

/**
 * Обработка контентных изображений компонента
 * @param {Object} parameters аргументы функции
 * @param {String} parameters.partial имя компонента
 * @param {Boolean} parameters.minify включена ли минификация при production сборке
 */
function processingContentImagesFromComponent(
  parameters = {
    partial: 'partial-name',
    minify: false
  }
) {
  console.info(
    `Обработка контентных изображений компонента ${parameters.partial}...`
  );
  return gulp
    .src(
      [
        `./src/components/${parameters.partial}/images/*/*.*`,
        `./src/components/${parameters.partial}/images/*/*/*.*`
      ],
      { since: gulp.lastRun(processingContentImagesFromComponent) }
    )
    .pipe(
      process.env.NODE_ENV === 'build' && parameters.minify
        ? imagemin(PROJECT.plugins.imagemin)
        : empty()
    )
    .pipe(gulp.dest(`./build/assets/images/${parameters.partial}/`));
}

/**
 * Обработка изображений шаблона
 */
function processingTemplateImages() {
  console.info('Обработка изображений шаблона...');
  return gulp
    .src(PROJECT.resources.images, {
      since: gulp.lastRun(processingTemplateImages)
    })
    .pipe(
      process.env.NODE_ENV === 'dev'
        ? empty()
        : imagemin(PROJECT.plugins.imagemin)
    )
    .pipe(flatten())
    .pipe(gulp.dest('./build/assets/images/'));
}

gulp.task(
  'images',
  gulp.parallel(
    () => processingTemplateImages(),
    () =>
      processingContentImagesFromComponent({
        partial: 'portfolio-item',
        minify: false
      }),
    () =>
      processingContentImagesFromComponent({
        partial: 'technologies-slideshow',
        minify: true
      }),
    () =>
      processingContentImagesFromComponent({
        partial: 'received-sertificates',
        minify: true
      })
  )
);

/*
|-------------------------------------------------------------------------------
| CSS
|-------------------------------------------------------------------------------
| Работа со стилями
*/
gulp.task(
  'css',
  gulp.series(done => {
    console.info('Обработка стилей...');
    gulp
      .src(PROJECT.resources.css)
      .pipe(
        process.env.NODE_ENV === 'dev'
          ? plumber(function(error) {
            console.log(error);
            this.emit('end');
          })
          : empty()
      )
      .pipe(process.env.NODE_ENV === 'dev' ? sourcemaps.init() : empty())
      .pipe(concat('app.css'))
      .pipe(postcss(PROJECT.plugins.postcss))
      .pipe(
        process.env.NODE_ENV === 'dev'
          ? empty()
          : base64(PROJECT.plugins.base64)
      )
      .pipe(process.env.NODE_ENV === 'dev' ? sourcemaps.write('./') : empty())
      .pipe(gulp.dest('./build/assets/styles/'));
    done();
  })
);

/*
|-------------------------------------------------------------------------------
| JS
|-------------------------------------------------------------------------------
| Работа с js модулями и вендорными зависимостями
*/
gulp.task(
  'js',
  gulp.series(done => {
    console.info('Делегирование сборки js модулей на Webpack...');
    gulp
      .src('./')
      .pipe(webpackStream(require('./webpack.config.js'), webpack))
      .pipe(gulp.dest('build/assets/scripts/'));
    done();
  })
);

/*
|-------------------------------------------------------------------------------
| COMMON
|-------------------------------------------------------------------------------
| Статические файлы, такие как видео, аудио, robots.txt и т.д.
*/
gulp.task(
  'common',
  gulp.series(done => {
    console.info('Перенос файлов, не требующих обработки...');
    gulp
      .src(PROJECT.resources.icons)
      .pipe(flatten())
      .pipe(gulp.dest('./build/assets/icons/'));
    gulp
      .src(PROJECT.resources.fonts)
      .pipe(flatten())
      .pipe(gulp.dest('./build/assets/fonts/'));
    gulp
      .src(PROJECT.resources.videos)
      .pipe(flatten())
      .pipe(gulp.dest('./build/assets/videos/'));
    gulp
      .src(PROJECT.resources.files)
      .pipe(flatten())
      .pipe(gulp.dest('./build/assets/files/'));
    gulp
      .src(PROJECT.resources.sounds)
      .pipe(flatten())
      .pipe(gulp.dest('./build/assets/sounds/'));
    gulp.src(PROJECT.resources.root).pipe(gulp.dest('./build/'));
    gulp.src(PROJECT.resources.plugins).pipe(gulp.dest('./build/plugins/'));
    done();
  })
);

/*
|-------------------------------------------------------------------------------
| STYLEGUIDE
|-------------------------------------------------------------------------------
| Генерация стайлгайда
*/
gulp.task(
  'styleguide',
  gulp.series(
    done => {
      console.info('Генерация стайлгайда...');
      done();
    },
    async done => {
      await rimraf('./src/plugins/gitbook/components', () =>
        console.info('Удалена директория "./src/plugins/gitbook/components"')
      );
      await gulp.src('./README.md').pipe(gulp.dest('./src/plugins/gitbook/'));
      done();
    },
    /**
     * Генарация шаблонов компонентов для примеров использования
     */
    () =>
      gulp
        .src(PROJECT.resources.styleguide.htm)
        .pipe(plumber())
        .pipe(flatten())
        .pipe(njkRender(PROJECT.plugins.njkRender))
        .pipe(htmlmin(PROJECT.plugins.htmlmin))
        .pipe(htmlbeautify(PROJECT.plugins.htmlBeautify))
        .pipe(
          rename(path => {
            const file = path;
            file.basename = file.basename.replace(/\.spec/, '');
          })
        )
        .pipe(gulp.dest('./src/plugins/gitbook/components/')),
    /**
     * Генарация описания компонента
     */
    () =>
      gulp
        .src(PROJECT.resources.styleguide.md)
        .pipe(flatten())
        .pipe(fileinclude({ basepath: '@root' }))
        .pipe(
          rename(path => {
            const file = path;
            file.basename = file.basename.replace(/\.spec/, '');
          })
        )
        .pipe(gulp.dest('./src/plugins/gitbook/components/'))
  )
);
gulp.task(
  'styleguide:previews',
  gulp.series(() => {
    console.info('Генерация разметки компонентов для стайлгайда...');
    return gulp
      .src(PROJECT.resources.styleguide.htm)
      .pipe(plumber())
      .pipe(flatten())
      .pipe(njkRender(PROJECT.plugins.njkRender))
      .pipe(htmlmin(PROJECT.plugins.htmlmin))
      .pipe(htmlbeautify(PROJECT.plugins.htmlBeautify))
      .pipe(
        rename(path => {
          const file = path;
          file.basename = file.basename.replace(/\.spec/, '');
        })
      )
      .pipe(
        inject.prepend(`
          <!DOCTYPE html>
          <link rel="stylesheet" href="/assets/styles/vendor.css">
          <link rel="stylesheet" href="/assets/styles/app.css">
          <div class="app" id="app" style="background: none; padding: .1px;">
        `)
      )
      .pipe(
        inject.append(`
          </div>
          <script src="/assets/scripts/vendor.js"></script>
          <script src="/assets/scripts/app.js"></script>
        `)
      )
      .pipe(gulp.dest('./build/styleguide/previews/'));
  })
);

/*
|-------------------------------------------------------------------------------
| WATCH
|-------------------------------------------------------------------------------
| Отслеживание изменений в исходниках
*/
gulp.task(
  'watch',
  gulp.series('htm', 'css', 'js', 'images', 'common', done => {
    console.info('Запуск слежения за изменениями файлов...');
    gulp.watch(
      './src/**/*.htm',
      gulp.series('htm', subtaskDone => {
        setTimeout(() => {
          browserSync.reload();
        }, 300);
        subtaskDone();
      })
    );
    gulp.watch(
      PROJECT.resources.css,
      gulp.series('css', subtaskDone => {
        setTimeout(() => {
          browserSync.reload(['./build/assets/styles/app.css']);
        }, 500);
        subtaskDone();
      })
    );
    gulp.watch(
      './build/**/*.js',
      gulp.series(subtaskDone => {
        browserSync.reload();
        subtaskDone();
      })
    );
    gulp.watch(
      PROJECT.resources.images,
      gulp.series('images', subtaskDone => {
        browserSync.reload();
        subtaskDone();
      })
    );
    gulp.watch(
      [
        PROJECT.resources.files,
        PROJECT.resources.fonts,
        PROJECT.resources.icons,
        PROJECT.resources.root,
        PROJECT.resources.sounds,
        PROJECT.resources.videos
      ],
      gulp.series('common', subtaskDone => {
        browserSync.reload();
        subtaskDone();
      })
    );
    done();
  })
);

/*
|-------------------------------------------------------------------------------
| BROWSER-SYNC
|-------------------------------------------------------------------------------
| Перезагрузка страницы в браузере при внесении изменений
*/
gulp.task(
  'browser-sync',
  gulp.parallel(done => {
    console.info('Запуск сервера синхронизации...');
    browserSync.init({
      server: './build/',
      // proxy: `alexlit.local`,
      open: false
    });
    done();
  })
);

/*
|-------------------------------------------------------------------------------
| DEPLOY
|-------------------------------------------------------------------------------
| Деплой на удаленный сервер
*/
gulp.task(
  'deploy',
  gulp.series(done => {
    console.info('Деплой на удаленный сервер...');
    const distribution = ['./build/**', '!./build/styleguide/**'];
    const connection = ftp.create({
      host: process.env.FTP_HOST,
      user: process.env.FTP_USER,
      password: process.env.FTP_PASSWORD,
      parallel: 10,
      log: gutil.log
    });

    gulp
      .src(distribution, { base: './build/', buffer: false })
      .pipe(plumber())
      .pipe(connection.newerOrDifferentSize(process.env.FTP_PATH))
      .pipe(connection.dest(process.env.FTP_PATH));
    done();
  })
);

/*
|-------------------------------------------------------------------------------
| TASKS
|-------------------------------------------------------------------------------
| Наборы тасков, исполняемых по-умолчанию
*/
gulp.task(
  'run',
  process.env.NODE_ENV === 'dev'
    ? gulp.series('removeBuild', gulp.parallel('watch', 'browser-sync'))
    : gulp.series('removeBuild', 'htm', 'js', 'images', 'css', 'common')
);
